// note: uses stack allocated arrays, may need `ulimit -s unlimited`

#include <complex.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <fftw3.h>

#include <sndfile.h>

// FIXME these defines must match markov-analysis.c

#define FFTSIZE 256
#define FFTBINS (FFTSIZE/2 + 1)
#define OVERLAP 16
#define OHOP (FFTSIZE / OVERLAP)
#define SOMSIZE 16

// FIXME should match analyzed source audio
#define SR 44100

int main(int argc, char **argv)
{
  if (argc != 3)
  {
    fprintf
      ( stderr
      , "usage:\n"
        "  %s in.dat out.wav\n"
      , argv[0]
      );
    return 1;
  }

  fprintf(stderr, "reading %s\n", argv[1]);
  FILE *dfile = fopen(argv[1], "rb");
  double imap[SOMSIZE][SOMSIZE][FFTBINS];
  int lasti = 0;
  int lastj = 0;
  int mins = 1.0 / 0.0;
  for (int i = 0; i < SOMSIZE; ++i)
  for (int j = 0; j < SOMSIZE; ++j)
  {
    double s = 0;
    for (int k = 0; k < FFTBINS; ++k)
    {
      double r = 0;
      fscanf(dfile, "%lf ", &r);
      imap[i][j][k] = r;
      s += r * r;
    }
    if (s > mins)
    {
      mins = s;
      lasti = i;
      lastj = j;
    }
  }
  double chain[SOMSIZE][SOMSIZE][SOMSIZE][SOMSIZE];
  for (int i = 0; i < SOMSIZE; ++i)
  for (int j = 0; j < SOMSIZE; ++j)
  for (int ii = 0; ii < SOMSIZE; ++ii)
  for (int jj = 0; jj < SOMSIZE; ++jj)
  {
    double r = 0;
    fscanf(dfile, "%lf ", &r);
    chain[i][j][ii][jj] = r;
  }
  fclose(dfile);

  const int CHANNELS = 2;
  fprintf(stderr, "writing %s\n", argv[2]);
  SF_INFO outfo =
    { 0
    , SR
    , CHANNELS
    , SF_FORMAT_WAV | SF_FORMAT_FLOAT
    , 0
    , 0
    };
  SNDFILE *ofile = sf_open(argv[2], SFM_WRITE, &outfo);
  if (! ofile) abort();

  float nbuffer[FFTSIZE][CHANNELS];
  memset(nbuffer, 0, sizeof(nbuffer));

  float obuffer[FFTSIZE][CHANNELS];
  memset(obuffer, 0, sizeof(obuffer));

  float owindow[FFTSIZE];
  memset(owindow, 0, sizeof(owindow));
  for (int i = 0; i < FFTSIZE; ++i)
  {
    owindow[i] = 0.5 - 0.5 * cos(i * 6.283185307179586 / FFTSIZE);
  }

  float abuffer[OHOP][CHANNELS];
  memset(abuffer, 0, sizeof(abuffer));

  double _Complex *fft_in = fftw_malloc(FFTSIZE * sizeof(*fft_in));
  double _Complex *fft_out = fftw_malloc(FFTSIZE * sizeof(*fft_out));
  fftw_plan forward = fftw_plan_dft_1d(FFTSIZE, fft_in, fft_out, FFTW_FORWARD, FFTW_PATIENT | FFTW_DESTROY_INPUT);
  fftw_plan backward = fftw_plan_dft_1d(FFTSIZE, fft_out, fft_in, FFTW_BACKWARD, FFTW_PATIENT | FFTW_DESTROY_INPUT);

  // FIXME 10mins should be enough for testing?
  for (int duration = 0; duration < 10 * 60 * SR; duration += OHOP)
  {
    // generate noise
    for (int i = 0; i < FFTSIZE - OHOP; ++i)
    {
      for (int channel = 0; channel < CHANNELS; ++channel)
      {
        nbuffer[i][channel] = nbuffer[i + OHOP][channel];
      }
    }
    for (int i = FFTSIZE - OHOP; i < FFTSIZE; ++i)
    {
      for (int channel = 0; channel < CHANNELS; ++channel)
      {
        nbuffer[i][channel] = rand() / (double) RAND_MAX - 0.5;
      }
    }

    // Markov chain
    double E[FFTBINS];
    int nexti = lasti;
    int nextj = lastj;
    double t = rand() / (double) RAND_MAX;
    for (int i = 0; i < SOMSIZE; ++i)
    {
      for (int j = 0; j < SOMSIZE; ++j)
      {
        t -= chain[lasti][lastj][i][j];
        if (t <= 0)
        {
          nexti = i;
          nextj = j;
          break;
        }
      }
      if (t <= 0)
        break;
    }
    for (int k = 0; k < FFTBINS; ++k)
    {
      E[k] = imap[nexti][nextj][k];
    }
    lasti = nexti;
    lastj = nextj;

    // process
    for (int channel = 0; channel < CHANNELS; ++channel)
    {
      // window input
      for (int i = 0; i < FFTSIZE; ++i)
      {
        fft_in[i] = nbuffer[i][channel] * owindow[i];
      }

      // FFT
      fftw_execute(forward);

      // amplify magnitude
      for (int i = 0; i < FFTSIZE; ++i)
      {
        if (i < FFTBINS)
        {
          fft_out[i] *= E[i];
        }
        else
        {
          fft_out[i] *= E[FFTSIZE - i];
        }
      }

      // IFFT
      fftw_execute(backward);

      // overlap-add
      for (int i = 0; i < FFTSIZE; ++i)
      {
        obuffer[i][channel] = i + OHOP < FFTSIZE ? obuffer[i + OHOP][channel] : 0;
      }
      for (int i = 0; i < FFTSIZE; ++i)
      {
        obuffer[i][channel] += creal(fft_in[i]) * owindow[i];
      }
    }

    // output
    sf_writef_float(ofile, &obuffer[0][0], OHOP);
  } // for duration

  sf_close(ofile);
  return 0;
}
