// note: uses stack allocated arrays, may need `ulimit -s unlimited`

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sndfile.h>

// FIXME these defines must match octave-analysis.c

#define OCTAVES 11
#define OCTAVESIZE (1 << (OCTAVES - 1))

#define OVERLAP 4

#define OHOP (OCTAVESIZE / OVERLAP)

// FIXME should match analyzed source audio
#define SR 44100

int main(int argc, char **argv)
{
  if (argc != 3)
  {
    fprintf
      ( stderr
      , "usage:\n"
        "  %s in.csv out.wav\n"
      , argv[0]
      );
    return 1;
  }

  fprintf(stderr, "reading %s\n", argv[1]);
  FILE *ifile = fopen(argv[1], "rb");

  fprintf(stderr, "writing %s\n", argv[2]);
  const int CHANNELS = 2;
  SF_INFO outfo =
    { 0
    , SR
    , CHANNELS
    , SF_FORMAT_WAV | SF_FORMAT_FLOAT
    , 0
    , 0
    };
  SNDFILE *ofile = sf_open(argv[2], SFM_WRITE, &outfo);
  if (! ofile) abort();

  float nbuffer[OCTAVESIZE][CHANNELS];
  memset(nbuffer, 0, sizeof(nbuffer));

  float obuffer[OCTAVESIZE][CHANNELS];
  memset(obuffer, 0, sizeof(obuffer));

  float owindow[OCTAVESIZE];
  memset(owindow, 0, sizeof(owindow));
  for (int i = 0; i < OCTAVESIZE; ++i)
  {
    owindow[i] = 0.5 - 0.5 * cos(i * 6.283185307179586 / OCTAVESIZE);
  }

  int aindex = 0;
  float abuffer[OHOP][CHANNELS];
  memset(abuffer, 0, sizeof(abuffer));

  // FIXME 10mins should be enough for testing?
  for (int duration = 0; duration < 10 * 60 * SR; ++duration)
  {
    // generate noise
    for (int channel = 0; channel < CHANNELS; ++channel)
    {
      abuffer[aindex][channel] = rand() / (double) RAND_MAX - 0.5;
    }
    aindex++;
    if (aindex == OHOP)
    {
      aindex = 0;
      for (int i = 0; i < OCTAVESIZE - OHOP; ++i)
      {
        for (int channel = 0; channel < CHANNELS; ++channel)
        {
          nbuffer[i][channel] = nbuffer[i + OHOP][channel];
        }
      }
      for (int i = OCTAVESIZE - OHOP; i < OCTAVESIZE; ++i)
      {
        for (int channel = 0; channel < CHANNELS; ++channel)
        {
          nbuffer[i][channel] = abuffer[i - (OCTAVESIZE - OHOP)][channel];
        }
      }

      // window
      float haar[2][OCTAVESIZE][CHANNELS];
      memset(haar, 0, sizeof(haar));
      int src = 0;
      int dst = 1;
      for (int i = 0; i < OCTAVESIZE; ++i)
      {
        for (int channel = 0; channel < CHANNELS; ++channel)
        {
          haar[src][i][channel] = nbuffer[i][channel] * owindow[i];
        }
      }

      // compute Haar wavelet transform
      for (int length = OCTAVESIZE >> 1; length > 0; length >>= 1)
      {
        for (int i = 0; i < length; ++i)
        {
          for (int channel = 0; channel < CHANNELS; ++channel)
          {
            float a = haar[src][2 * i + 0][channel];
            float b = haar[src][2 * i + 1][channel];
            float s = (a + b) / 2;
            float d = (a - b) / 2;
            haar[dst][         i][channel] = s;
            haar[dst][length + i][channel] = d;
          }
        }
        for (int i = 0; i < OCTAVESIZE; ++i)
        {
          for (int channel = 0; channel < CHANNELS; ++channel)
          {
            haar[src][i][channel] = haar[dst][i][channel];
          }
        }
      }

      // compute energy per octave
      double E[OCTAVES];
      memset(E, 0, sizeof(E));
      for (int channel = 0; channel < CHANNELS; ++channel)
      {
        E[0] += fabsf(haar[src][0][channel]); // DC
      }
      for ( int octave = 1, length = 1
          ; length <= OCTAVESIZE >> 1
          ; octave += 1, length <<= 1
          )
      {
        double rms = 0;
        for (int channel = 0; channel < CHANNELS; ++channel)
        {
          for (int i = 0; i < length; ++i)
          {
            double a = haar[src][length + i][channel];
            rms += a * a;
          }
        }
        rms /= length * CHANNELS;
        rms = sqrt(fmax(rms, 0));
        E[octave] = rms;
      }

      // read energies
      double Ein[OCTAVES];
      for (int octave = 0; octave < OCTAVES; ++octave)
      {
        Ein[octave] = 0;
      }
      int ok = 1;
      for (int octave = 0; octave < OCTAVES; ++octave)
      {
        if (octave + 1 < OCTAVES)
        {
          ok &= 1 == fscanf(ifile, "%lf ", &Ein[octave]);
        }
        else
        {
          ok &= 1 == fscanf(ifile, "%lf\n", &Ein[octave]);
        }
      }
      if (! ok)
      {
        break;
      }

      // amplify octaves
      {
        double gain = Ein[0] / E[0];
        if (isnan(gain) || isinf(gain)) gain = 0;
        for (int channel = 0; channel < CHANNELS; ++channel)
        {
          haar[src][0][channel] *= Ein[0] / E[0]; // DC
        }
      }
      for ( int octave = 1, length = 1
          ; length <= OCTAVESIZE >> 1
          ; octave += 1, length <<= 1
          )
      {
        double gain = Ein[octave] / E[octave];
        if (isnan(gain) || isinf(gain)) gain = 0;
        for (int channel = 0; channel < CHANNELS; ++channel)
        {
          for (int i = 0; i < length; ++i)
          {
            haar[src][length + i][channel] *= gain;
          }
        }
      }

      // compute inverse Haar wavelet transform
      for (int length = 1; length <= OCTAVESIZE >> 1; length <<= 1)
      {
        for (int i = 0; i < OCTAVESIZE; ++i)
        {
          for (int channel = 0; channel < CHANNELS; ++channel)
          {
            haar[dst][i][channel] = haar[src][i][channel];
          }
        }
        for (int i = 0; i < length; ++i)
        {
          for (int channel = 0; channel < CHANNELS; ++channel)
          {
            float s = haar[dst][         i][channel];
            float d = haar[dst][length + i][channel];
            float a = s + d;
            float b = s - d;
            haar[src][2 * i + 0][channel] = a;
            haar[src][2 * i + 1][channel] = b;
          }
        }
      }
  
      // window
      for (int i = 0; i < OCTAVESIZE; ++i)
      {
        for (int channel = 0; channel < CHANNELS; ++channel)
        {
          haar[src][i][channel] *= owindow[i];
        }
      }
  
      // overlap-add
      for (int i = 0; i < OCTAVESIZE; ++i)
      {
        for (int channel = 0; channel < CHANNELS; ++channel)
        {
          obuffer[i][channel]
            = i + OHOP < OCTAVESIZE
            ? obuffer[i + OHOP][channel]
            : 0;
        }
      }
      for (int i = 0; i < OCTAVESIZE; ++i)
      {
        for (int channel = 0; channel < CHANNELS; ++channel)
        {
          obuffer[i][channel] += haar[src][i][channel];
        }
      }
  
      // output
      sf_writef_float(ofile, &obuffer[0][0], OHOP);

    } // if aindex == OHOP
  } // for duration

  fclose(ifile);
  sf_close(ofile);
  return 0;
}
