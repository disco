#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sndfile.h>

uint64_t next_power_of_two(uint64_t v)
{
  v--;
  v |= v >> 1;
  v |= v >> 2;
  v |= v >> 4;
  v |= v >> 8;
  v |= v >> 16;
  v |= v >> 32;
  v++;
  return v;
}

int main(int argc, char **argv)
{
  if (argc != 6)
  {
    fprintf
      ( stderr
      , "usage:\n"
        "  %s in.wav out.wav ratio spread delay\n"
      , argv[0]
      );
    return 1;
  }
  float ratio = atof(argv[3]);
  float spread = atof(argv[4]);
  float delay = atof(argv[5]);

  SF_INFO info;
  memset(&info, 0, sizeof(info));
  fprintf(stderr, "reading %s\n", argv[1]);
  SNDFILE *ifile = sf_open(argv[1], SFM_READ, &info);
  if (! ifile) abort();
  int64_t isize = next_power_of_two(info.frames);
  float *ibuffer = calloc(1, sizeof(*ibuffer) * isize * info.channels);
  if (! ibuffer) abort();
  if (info.frames != sf_readf_float(ifile, ibuffer, info.frames)) abort();
  sf_close(ifile);

  int64_t oframes = info.frames * ratio;
  float *ihbuffer = calloc(1, sizeof(*ihbuffer) * isize * 2);
  if (! ihbuffer) abort();
  int64_t osize = next_power_of_two(oframes);
  float *obuffer = calloc(1, sizeof(*obuffer) * osize * info.channels);
  if (! obuffer) abort();
  float *ohbuffer = calloc(1, sizeof(*ohbuffer) * osize * 2);
  if (! ohbuffer) abort();

  int64_t src = 0;
  int64_t dst = 1;
  for (int64_t channel = 0; channel < info.channels; ++channel)
  {
    fprintf(stderr, "channel %d\n", (int) channel + 1);

    // unpack channel
    for (int64_t i = 0; i < isize; ++i)
    {
      ihbuffer[isize * src + i] = ibuffer[info.channels * i + channel];
    }

    // compute Haar wavelet transform
    for (int64_t length = isize >> 1; length > 0; length >>= 1)
    {
      for (int64_t i = 0; i < length; ++i)
      {
        float a = ihbuffer[isize * src + 2 * i + 0];
        float b = ihbuffer[isize * src + 2 * i + 1];
        float s = (a + b) / 2;
        float d = (a - b) / 2;
        ihbuffer[isize * dst +          i] = s;
        ihbuffer[isize * dst + length + i] = d;
      }
      for (int64_t i = 0; i < isize; ++i)
      {
        ihbuffer[isize * src + i] = ihbuffer[isize * dst + i];
      }
    }

    // time stretch
    for (int64_t olength = osize >> 1, ilength = isize >> 1; olength > 0 && ilength > 0; olength >>= 1, ilength >>= 1)
    {
      for (int64_t o = 0; o < olength; ++o)
      {
        int64_t i = o / ratio + spread * rand() / RAND_MAX + delay;
        if (0 <= i && i < ilength)
        {
          ohbuffer[osize * src + olength + o] = ihbuffer[isize * dst + ilength + i];
        }
      }
    }

    // compute inverse Haar wavelet transform
    for (int64_t length = 1; length <= osize >> 1; length <<= 1)
    {
      for (int64_t i = 0; i < osize; ++i)
      {
        ohbuffer[osize * dst + i] = ohbuffer[osize * src + i];
      }
      for (int64_t i = 0; i < length; ++i)
      {
        float s = ohbuffer[osize * dst +          i];
        float d = ohbuffer[osize * dst + length + i];
        float a = s + d;
        float b = s - d;
        ohbuffer[osize * src + 2 * i + 0] = a;
        ohbuffer[osize * src + 2 * i + 1] = b;
      }
    }

    // pack channel
    for (int64_t i = 0; i < osize; ++i)
    {
      obuffer[info.channels * i + channel] = ohbuffer[osize * src + i];
    }

  }

  fprintf(stderr, "writing %s\n", argv[2]);
  SF_INFO outfo =
    { 0
    , info.samplerate
    , info.channels
    , SF_FORMAT_WAV | SF_FORMAT_FLOAT
    , 0
    , 0
    };
  SNDFILE *ofile = sf_open(argv[2], SFM_WRITE, &outfo);
  if (! ofile) abort();
  if (oframes != sf_writef_float(ofile, obuffer, oframes)) abort();
  sf_close(ofile);

  free(ohbuffer);
  free(obuffer);
  free(ihbuffer);
  free(ibuffer);
  return 0;
}
