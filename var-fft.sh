#!/bin/bash
set -e
echo "Analyzing filter..."
rm -i input.wav || true
rm -i output.txt || true
ln -sf "$1" input.wav
./fft-analysis.sh
rm input.wav
rm -i input.csv || true
mv output.txt input.csv
echo "Analyzing left..."
ecasound -q -i "$2" -f:f32,1, -o input.wav
./fft-analysis.sh
rm input.wav
rm -i modulate-l.csv || true
mv output.txt modulate-l.csv
echo "Analyzing right..."
ecasound -q -i "$2" -f:f32,1, -o input.wav -chcopy:2,1
./fft-analysis.sh
rm input.wav
rm -i modulate-r.csv || true
mv output.txt modulate-r.csv
echo "Building VAR..."
rm -i params.csv || true
rm -i sigmau.csv || true
python3 ./var-analysis.py
rm input.csv
echo "Simulating left..."
rm -i output-l.txt || true
./var-simulate 129 8 params.csv sigmau.csv < modulate-l.csv > output-l.txt
echo "Simulating right..."
rm -i output-r.txt || true
./var-simulate 129 8 params.csv sigmau.csv < modulate-r.csv > output-r.txt
rm -f params.csv
rm -f sigmau.csv
rm -f modulate-l.csv
rm -f modulate-r.csv
echo "Synthesizing left..."
rm -f output.wav
rm -f output.txt
mv output-l.txt output.txt
./fft-synthesis.sh
#rm -f output-l.wav
ffmpeg -i output.wav -map_channel 0.0.0 -c:a pcm_f32le -y output-l.wav
rm -f output.wav
echo "Synthesizing right..."
rm -f output.wav
rm -f output.txt
mv output-r.txt output.txt
./fft-synthesis.sh
#rm -f output-r.wav
ffmpeg -i output.wav -map_channel 0.0.0 -c:a pcm_f32le -y output-r.wav
rm -f output.wav
rm -f output.txt
echo "Merging stereo..."
ffmpeg -i output-l.wav -i output-r.wav -filter_complex "[0:a][1:a]amerge" -c:a pcm_f32le -y "$1-$2-u.wav"
#rm -f output-l.wav
#rm -f output-r.wav
echo "Normalizing..."
./normalize "$1-$2-u.wav" "$1-$2-n.wav"
