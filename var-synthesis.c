// note: uses stack allocated arrays, may need `ulimit -s unlimited`

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <gsl/gsl_linalg.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

#include <sndfile.h>

// FIXME these defines must match octave-analysis.c

#define OCTAVES 11
#define OCTAVESIZE (1 << (OCTAVES - 1))

#define OVERLAP 4

#define OHOP (OCTAVESIZE / OVERLAP)

// FIXME should match analyzed source audio
#define SR 44100

#define HISTORY 8

#define CHANNELS 2

// state
static double cA[OCTAVES * HISTORY + 1][OCTAVES];
static double Ecov[OCTAVES][OCTAVES];
static double sigma[OCTAVES];
static SNDFILE *ofile = 0;
static double Y[HISTORY][OCTAVES];
static float nbuffer[OCTAVESIZE][CHANNELS];
static float obuffer[OCTAVESIZE][CHANNELS];
static float owindow[OCTAVESIZE];
static int aindex = 0;
static float abuffer[OHOP][CHANNELS];
static gsl_rng *PRNG = 0;
static gsl_matrix_view cov;

static void audio_process1(double t)
{
    // generate noise
    for (int channel = 0; channel < CHANNELS; ++channel)
    {
      abuffer[aindex][channel] = rand() / (double) RAND_MAX - 0.5;
    }
    aindex++;
    if (aindex == OHOP)
    {
      aindex = 0;
      for (int i = 0; i < OCTAVESIZE - OHOP; ++i)
      {
        for (int channel = 0; channel < CHANNELS; ++channel)
        {
          nbuffer[i][channel] = nbuffer[i + OHOP][channel];
        }
      }
      for (int i = OCTAVESIZE - OHOP; i < OCTAVESIZE; ++i)
      {
        for (int channel = 0; channel < CHANNELS; ++channel)
        {
          nbuffer[i][channel] = abuffer[i - (OCTAVESIZE - OHOP)][channel];
        }
      }

      // window
      float haar[2][OCTAVESIZE][CHANNELS];
      memset(haar, 0, sizeof(haar));
      int src = 0;
      int dst = 1;
      for (int i = 0; i < OCTAVESIZE; ++i)
      {
        for (int channel = 0; channel < CHANNELS; ++channel)
        {
          haar[src][i][channel] = nbuffer[i][channel] * owindow[i];
        }
      }

      // compute Haar wavelet transform
      for (int length = OCTAVESIZE >> 1; length > 0; length >>= 1)
      {
        for (int i = 0; i < length; ++i)
        {
          for (int channel = 0; channel < CHANNELS; ++channel)
          {
            float a = haar[src][2 * i + 0][channel];
            float b = haar[src][2 * i + 1][channel];
            float s = (a + b) / 2;
            float d = (a - b) / 2;
            haar[dst][         i][channel] = s;
            haar[dst][length + i][channel] = d;
          }
        }
        for (int i = 0; i < OCTAVESIZE; ++i)
        {
          for (int channel = 0; channel < CHANNELS; ++channel)
          {
            haar[src][i][channel] = haar[dst][i][channel];
          }
        }
      }

      // compute energy per octave
      double E[OCTAVES];
      memset(E, 0, sizeof(E));
      for (int channel = 0; channel < CHANNELS; ++channel)
      {
        E[0] += fabsf(haar[src][0][channel]); // DC
      }
      for ( int octave = 1, length = 1
          ; length <= OCTAVESIZE >> 1
          ; octave += 1, length <<= 1
          )
      {
        double rms = 0;
        for (int channel = 0; channel < CHANNELS; ++channel)
        {
          for (int i = 0; i < length; ++i)
          {
            double a = haar[src][length + i][channel];
            rms += a * a;
          }
        }
        rms /= length * CHANNELS;
        rms = sqrt(fmax(rms, 0));
        E[octave] = rms;
      }

      // generate correlated noise source
      double noise[OCTAVES];
      // generate independent Gaussian vectors
      for (int k = 0; k < OCTAVES; ++k)
      {
        noise[k] = gsl_ran_gaussian(PRNG, pow(sigma[k], 1 - cos(M_PI * (t - 0.5))));
      }
      gsl_vector_view x = gsl_vector_view_array(noise, OCTAVES);
      // correlate them to the correct covariance matrix
      gsl_blas_dtrmv(CblasLower, CblasNoTrans, CblasNonUnit, &cov.matrix, &x.vector);

      // step vector autoregression
      double Yn[OCTAVES];
      for (int octave = 0; octave < OCTAVES; ++octave)
      {
        int k = 0;
        Yn[octave] = cA[k++][octave] + noise[octave];
        for (int p = 0; p < HISTORY; ++p)
        {
          for (int o = 0; o < OCTAVES; ++o)
          {
            Yn[octave] += cA[k++][octave] * Y[p][o];
          }
        }
      }
      for (int octave = 0; octave < OCTAVES; ++octave)
      {
        for (int p = HISTORY - 1; p > 0; --p)
        {
          Y[p][octave] = Y[p - 1][octave];
        }
        Y[0][octave] = Yn[octave];
      }

#if 0
      // normalize
      double s = 0;
      for (int i = 0; i < HISTORY; ++i)
      {
        for (int j = 0; j < OCTAVES; ++j)
        {
          s += Y[i][j] * Y[i][j];
        }
      }
      s /= HISTORY;
      s /= OCTAVES;
      s = sqrt(s);
      s = 1/s;
      if (isnan(s) || isinf(s)) s = 0;
      for (int i = 0; i < HISTORY; ++i)
      {
        for (int j = 0; j < OCTAVES; ++j)
        {
          Y[i][j] *= s;
        }
      }
#endif

      // amplify octaves
      {
        double gain = Yn[0] / E[0];
        if (isnan(gain) || isinf(gain)) gain = 0;
        for (int channel = 0; channel < CHANNELS; ++channel)
        {
          haar[src][0][channel] *= gain; // DC
        }
      }
      for ( int octave = 1, length = 1
          ; length <= OCTAVESIZE >> 1
          ; octave += 1, length <<= 1
          )
      {
        double gain = Yn[octave] / E[octave];
        if (isnan(gain) || isinf(gain)) gain = 0;
        for (int channel = 0; channel < CHANNELS; ++channel)
        {
          for (int i = 0; i < length; ++i)
          {
            haar[src][length + i][channel] *= gain;
          }
        }
      }

      // compute inverse Haar wavelet transform
      for (int length = 1; length <= OCTAVESIZE >> 1; length <<= 1)
      {
        for (int i = 0; i < OCTAVESIZE; ++i)
        {
          for (int channel = 0; channel < CHANNELS; ++channel)
          {
            haar[dst][i][channel] = haar[src][i][channel];
          }
        }
        for (int i = 0; i < length; ++i)
        {
          for (int channel = 0; channel < CHANNELS; ++channel)
          {
            float s = haar[dst][         i][channel];
            float d = haar[dst][length + i][channel];
            float a = s + d;
            float b = s - d;
            haar[src][2 * i + 0][channel] = a;
            haar[src][2 * i + 1][channel] = b;
          }
        }
      }
  
      // window
      for (int i = 0; i < OCTAVESIZE; ++i)
      {
        for (int channel = 0; channel < CHANNELS; ++channel)
        {
          haar[src][i][channel] *= owindow[i];
        }
      }
  
      // overlap-add
      for (int i = 0; i < OCTAVESIZE; ++i)
      {
        for (int channel = 0; channel < CHANNELS; ++channel)
        {
          obuffer[i][channel]
            = i + OHOP < OCTAVESIZE
            ? obuffer[i + OHOP][channel]
            : 0;
        }
      }
      for (int i = 0; i < OCTAVESIZE; ++i)
      {
        for (int channel = 0; channel < CHANNELS; ++channel)
        {
          obuffer[i][channel] += haar[src][i][channel];
        }
      }
  
      // output
      sf_writef_float(ofile, &obuffer[0][0], OHOP);

    } // if aindex == OHOP
}

int main(int argc, char **argv)
{
  if (argc != 4)
  {
    fprintf
      ( stderr
      , "usage:\n"
        "  %s params.csv sigmau.csv out.wav\n"
      , argv[0]
      );
    return 1;
  }

  memset(Y, 0, sizeof(Y));

  fprintf(stderr, "reading %s\n", argv[1]);
  FILE *ifile = fopen(argv[1], "rb");
  for (int i = 0; i < OCTAVES * HISTORY + 1; ++i)
  {
    for (int j = 0; j < OCTAVES; ++j)
    {
      fscanf(ifile, "%lf ", &cA[i][j]);
    }
  }
  fclose(ifile);

  fprintf(stderr, "reading %s\n", argv[2]);
  ifile = fopen(argv[2], "rb");
  for (int i = 0; i < OCTAVES; ++i)
  {
    for (int j = 0; j < OCTAVES; ++j)
    {
      fscanf(ifile, "%lf ", &Ecov[i][j]);
    }
  }
  fclose(ifile);
  for (int i = 0; i < OCTAVES; ++i)
  {
    sigma[i] = sqrt(Ecov[i][i]);
  }
  cov = gsl_matrix_view_array(&Ecov[0][0], OCTAVES, OCTAVES);
  gsl_linalg_cholesky_decomp1(&cov.matrix);
  gsl_rng_env_setup();
  PRNG = gsl_rng_alloc(gsl_rng_default);

  fprintf(stderr, "writing %s\n", argv[3]);
  SF_INFO outfo =
    { 0
    , SR
    , CHANNELS
    , SF_FORMAT_WAV | SF_FORMAT_FLOAT
    , 0
    , 0
    };
  ofile = sf_open(argv[3], SFM_WRITE, &outfo);
  if (! ofile) abort();

  memset(nbuffer, 0, sizeof(nbuffer));

  memset(obuffer, 0, sizeof(obuffer));

  memset(owindow, 0, sizeof(owindow));
  for (int i = 0; i < OCTAVESIZE; ++i)
  {
    owindow[i] = 0.5 - 0.5 * cos(i * 6.283185307179586 / OCTAVESIZE);
  }

  memset(abuffer, 0, sizeof(abuffer));

  // FIXME 10mins should be enough for testing?
  for (int duration = 0; duration < 1 * 60 * SR; ++duration)
  {
    audio_process1((duration + 0.5) / (1 * 60 * SR));
  }

  sf_close(ofile);
  return 0;
}
