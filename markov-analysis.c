// note: uses stack allocated arrays, may need `ulimit -s unlimited`

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sndfile.h>

#include <gsl/gsl_linalg.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

#define OCTAVES 11
#define OCTAVESIZE (1 << (OCTAVES - 1))

#define OVERLAP 16

#define OHOP (OCTAVESIZE / OVERLAP)

#define SOMSIZE 16

int main(int argc, char **argv)
{
  if (argc != 3)
  {
    fprintf
      ( stderr
      , "usage:\n"
        "  %s in.wav out.dat\n"
      , argv[0]
      );
    return 1;
  }
  SF_INFO info;
  memset(&info, 0, sizeof(info));
  fprintf(stderr, "reading %s\n", argv[1]);
  SNDFILE *ifile = sf_open(argv[1], SFM_READ, &info);
  if (! ifile) abort();
  const int CHANNELS = info.channels;

  float window[OCTAVESIZE];
  memset(window, 0, sizeof(window));
  for (int i = 0; i < OCTAVESIZE; ++i)
  {
    window[i] = 0.5 - 0.5 * cos(i * 6.283185307179586 / OCTAVESIZE);
  }

  int Ecount = 0;
  double Emean[OCTAVES];
  memset(Emean, 0, sizeof(Emean));
  double Ecov[OCTAVES][OCTAVES];
  memset(Ecov, 0, sizeof(Ecov));

  double imap[SOMSIZE][SOMSIZE][OCTAVES];
  memset(imap, 0, sizeof(imap));

  gsl_rng_env_setup();
  gsl_rng *PRNG = gsl_rng_alloc(gsl_rng_default);
  int step = 0;
  int lasti = 0, lastj = 0;
  double chain[SOMSIZE][SOMSIZE][SOMSIZE][SOMSIZE];
  memset(chain, 0, sizeof(chain));
  double total[SOMSIZE][SOMSIZE];
  memset(total, 0, sizeof(total));

  for (int pass = 0; pass < 4; ++pass)
  {
    fprintf(stderr, "pass %d/4\n", pass + 1);
    sf_seek(ifile, 0, SEEK_SET);
    float ibuffer[OCTAVESIZE][CHANNELS];
    memset(ibuffer, 0, sizeof(ibuffer));

    if (pass == 2)
    {
      // Cholesky decomposition of covariance matrix
      for (int i = 0; i < OCTAVES; ++i)
      {
        for (int j = 0; j < OCTAVES; ++j)
        {
          Ecov[i][j] /= Ecount;
          fprintf(stderr, "%g ", Ecov[i][j]);
        }
        fprintf(stderr, "\n");
      }
      gsl_matrix_view cov = gsl_matrix_view_array(&Ecov[0][0], OCTAVES, OCTAVES);
      gsl_linalg_cholesky_decomp1(&cov.matrix);
      // randomize SOM
      for (int i = 0; i < SOMSIZE; ++i)
      {
        for (int j = 0; j < SOMSIZE; ++j)
        {
          // generate independent Gaussian vectors
          double d[OCTAVES];
          for (int k = 0; k < OCTAVES; ++k)
          {
            d[k] = gsl_ran_gaussian(PRNG, 1);
          }
          gsl_vector_view x = gsl_vector_view_array(d, OCTAVES);
          // correlate them to the correct covariance matrix
          gsl_blas_dtrmv(CblasLower, CblasNoTrans, CblasNonUnit, &cov.matrix, &x.vector);
          for (int k = 0; k < OCTAVES; ++k)
          {
            imap[i][j][k] = Emean[k] / Ecount + d[k];
          }
        }
      }
      step = 0;
    }

    int reading = OVERLAP;
    while (reading > 0)
    {

      // read input
      for (int i = 0; i < OCTAVESIZE - OHOP; ++i)
      {
        for (int channel = 0; channel < CHANNELS; ++channel)
        {
          ibuffer[i][channel] = ibuffer[i + OHOP][channel];
        }
      }
      for (int i = OCTAVESIZE - OHOP; i < OCTAVESIZE; ++i)
      {
        for (int channel = 0; channel < CHANNELS; ++channel)
        {
          ibuffer[i][channel] = 0;
        }
      }
      if (reading == OVERLAP)
      {
        int r = sf_readf_float(ifile, &ibuffer[OCTAVESIZE - OHOP][0], OHOP);
        if (r != OHOP)
        {
          reading--;
        }
      }
      else
      {
        reading--;
      }
  
      // window
      float haar[2][OCTAVESIZE][CHANNELS];
      memset(haar, 0, sizeof(haar));
      int src = 0;
      int dst = 1;
      for (int i = 0; i < OCTAVESIZE; ++i)
      {
        for (int channel = 0; channel < CHANNELS; ++channel)
        {
          haar[src][i][channel] = ibuffer[i][channel] * window[i];
        }
      }

      // compute Haar wavelet transform
      for (int length = OCTAVESIZE >> 1; length > 0; length >>= 1)
      {
        for (int i = 0; i < length; ++i)
        {
          for (int channel = 0; channel < CHANNELS; ++channel)
          {
            float a = haar[src][2 * i + 0][channel];
            float b = haar[src][2 * i + 1][channel];
            float s = (a + b) / 2;
            float d = (a - b) / 2;
            haar[dst][         i][channel] = s;
            haar[dst][length + i][channel] = d;
          }
        }
        for (int i = 0; i < OCTAVESIZE; ++i)
        {
          for (int channel = 0; channel < CHANNELS; ++channel)
          {
            haar[src][i][channel] = haar[dst][i][channel];
          }
        }
      }

      // compute energy per octave
      double E[OCTAVES];
      memset(E, 0, sizeof(E));
      for (int channel = 0; channel < CHANNELS; ++channel)
      {
        E[0] += fabsf(haar[src][0][channel]); // DC
      }
      for ( int octave = 1, length = 1
          ; length <= OCTAVESIZE >> 1
          ; octave += 1, length <<= 1
          )
      {
        double rms = 0;
        for (int channel = 0; channel < CHANNELS; ++channel)
        {
          for (int i = 0; i < length; ++i)
          {
            double a = haar[src][length + i][channel];
            rms += a * a;
          }
        }
        rms /= length * CHANNELS;
        rms = sqrt(fmax(rms, 0));
        E[octave] = rms;
      }

      if (pass == 0)
      {
        // compute mean
        for (int octave = 0; octave < OCTAVES; ++octave)
        {
          Emean[octave] += E[octave];
        }
        Ecount += 1;
      }
      else if (pass == 1)
      {
        // compute covariance
        for (int octave1 = 0; octave1 < OCTAVES; ++octave1)
        {
          for (int octave2 = 0; octave2 < OCTAVES; ++octave2)
          {
            Ecov[octave1][octave2]
              += (E[octave1] - Emean[octave1] / Ecount)
               * (E[octave2] - Emean[octave2] / Ecount);
          }
        }
      }
      else if (pass == 2)
      {
        // compute SOM
        
        // find best match
        int mini = 0;
        int minj = 0;
        double mins = 1.0 / 0.0;
        for (int i = 0; i < SOMSIZE; ++i)
        {
          for (int j = 0; j < SOMSIZE; ++j)
          {
            double s = 0;
            for (int k = 0; k < OCTAVES; ++k)
            {
              double d = E[k] - imap[i][j][k];
              s += d * d;
            }
            if (s < mins)
            {
              mins = s;
              mini = i;
              minj = j;
            }
          }
        }
        
        // update weights
        for (int i = 0; i < SOMSIZE; ++i)
        {
          for (int j = 0; j < SOMSIZE; ++j)
          {
            double di = (i - mini) * 1.0 / SOMSIZE;
            double dj = (j - minj) * 1.0 / SOMSIZE;
            // FIXME magic numbers to control learning rate
            double kernel = 0.1 * exp(- (di * di + dj * dj) * (step + 1.0) / 100000);
            for (int k = 0; k < OCTAVES; ++k)
            {                
              imap[i][j][k] += kernel * (E[k] - imap[i][j][k]);
            }
          }
        }
        step++;
      }
      else if (pass == 3)
      {
        // compute Markov chain
        
        // find best match
        int mini = 0;
        int minj = 0;
        double mins = 1.0 / 0.0;
        for (int i = 0; i < SOMSIZE; ++i)
        {
          for (int j = 0; j < SOMSIZE; ++j)
          {
            double s = 0;
            for (int k = 0; k < OCTAVES; ++k)
            {
              double d = E[k] - imap[i][j][k];
              s += d * d;
            }
            if (s < mins)
            {
              mins = s;
              mini = i;
              minj = j;
            }
          }
        }
        chain[lasti][lastj][mini][minj] += 1;
        total[lasti][lastj] += 1;
        lasti = mini;
        lastj = minj;
      }
    } // while reading
  } // for pass
  sf_close(ifile);

  // output
  fprintf(stderr, "writing %s\n", argv[2]);
  FILE *ofile = fopen(argv[2], "wb");
  for (int i = 0; i < SOMSIZE; ++i)
  for (int j = 0; j < SOMSIZE; ++j)
  for (int k = 0; k < OCTAVES; ++k)
    fprintf(ofile, "%.18e\n", imap[i][j][k]);
  for (int i = 0; i < SOMSIZE; ++i)
  for (int j = 0; j < SOMSIZE; ++j)
  for (int ii = 0; ii < SOMSIZE; ++ii)
  for (int jj = 0; jj < SOMSIZE; ++jj)
    fprintf(ofile, "%.18e\n", chain[i][j][ii][jj] / total[i][j]);
  fclose(ofile);
  fprintf(stderr, "total frames %d\n", Ecount);

  return 0;
}
