#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sndfile.h>

int main(int argc, char **argv)
{
  if (argc != 3)
  {
    fprintf
      ( stderr
      , "usage:\n"
        "  %s in.wav out.wav\n"
      , argv[0]
      );
    return 1;
  }
  SF_INFO info;
  memset(&info, 0, sizeof(info));
  fprintf(stderr, "reading %s\n", argv[1]);
  SNDFILE *ifile = sf_open(argv[1], SFM_READ, &info);
  if (! ifile)
  {
    return 1;
  }
  float *data = malloc(info.frames * info.channels * sizeof(*data));
  sf_readf_float(ifile, data, info.frames);
  sf_close(ifile);
  double peak = 0.0;
  for (size_t i = 0; i < (size_t) info.frames * info.channels; ++i)
  {
    peak = fmax(peak, data[i]);
  }
  fprintf(stderr, "peak %g\n", peak);
  float gain = 1 / peak;
  for (size_t i = 0; i < (size_t) info.frames * info.channels; ++i)
  {
    data[i] *= gain;
  }
  fprintf(stderr, "writing %s\n", argv[2]);
  SF_INFO outfo = { 0, info.samplerate, info.channels, SF_FORMAT_WAV | SF_FORMAT_FLOAT, 0, 0 };
  SNDFILE *ofile = sf_open(argv[2], SFM_WRITE, &outfo);
  sf_writef_float(ofile, data, info.frames);
  sf_close(ofile);
  return 0;
}
