#include <m_pd.h>

#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

static t_class *moise_tilde_class;

struct moise_tilde
{
  t_object obj;
  gsl_rng *rng;
};

static int moise_tilde_get_seed(void)
{
  static int val = 307;
  val = val * 435898247 + 382842987; // Pd's noise~ algorithm
  return val;
}

static t_int *moise_tilde_perform(t_int *w)
{
  t_sample *out = (t_sample *) (w[1]);
  gsl_rng *rng = (gsl_rng *) (w[2]);
  int n = (int) (w[3]);
  while (n--)
  {
    *out++ = gsl_rng_uniform(rng) * 2.0 - 1.0;
  }
  return w + 4;
}

static void moise_tilde_dsp(struct moise_tilde *x, t_signal **sp)
{
  dsp_add(moise_tilde_perform, 3, sp[0]->s_vec, x->rng, sp[0]->s_n);
}

static void moise_tilde_seed(struct moise_tilde *x, t_float f)
{
  gsl_rng_set(x->rng, f);
}

static void *moise_tilde_new(void)
{
  struct moise_tilde *x = (struct moise_tilde *) pd_new(moise_tilde_class);
  outlet_new(&x->obj, gensym("signal"));
  x->rng = gsl_rng_alloc(gsl_rng_mt19937);
  gsl_rng_set(x->rng, moise_tilde_get_seed());
  return x;
}

static void moise_tilde_free(struct moise_tilde *x)
{
  gsl_rng_free(x->rng);
}

extern void moise_tilde_setup(void)
{
  moise_tilde_class = class_new(gensym("moise~"), moise_tilde_new, (t_method)moise_tilde_free, sizeof(struct moise_tilde), 0, 0);
  class_addmethod(moise_tilde_class, (t_method)moise_tilde_dsp, gensym("dsp"), A_CANT, 0);
  class_addmethod(moise_tilde_class, (t_method)moise_tilde_seed, gensym("seed"), A_FLOAT, 0);
}
