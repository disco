# disco

Audio fingerprint discrimination and resynthesis via Haar wavelets.

- <https://mathr.co.uk/disco>
- <https://code.mathr.co.uk/disco>
- <https://mathr.co.uk/disco/designer22> (2022 version, low latency)
- <https://mathr.co.uk/disco/designer19> (2019 version, high latency)

See also:

- <https://mathr.co.uk/deep-disco> (classic Amiga version)

## dependencies

gcc, make, libsndfile, libgsl

for disco/designer native version: libsdl2, opengl

for disco/designer web version: emscripten

## build

    make                  # native versions
    make -C designer      # native version
    make -C designer web  # web version

## examples

Timbre stamp white noise with energy per octave of a control input:

    ./timbre-stamp input.wav output.wav

Compute energy per octave (audio) per octave (rhythm) fingerprint:

    ./rhythm-analysis input.wav output.dat

Resynthesize from fingerprint by stamping on white noise:

    ./rhythm-synthesis input.dat output.wav

Compute energy per octave (audio) Markov chain via self-organizing map:

    ./markov-analysis input.wav output.dat

Resynthesize from Markov chain by stamping on white noise:

    ./markov-synthesis input.dat output.wav

Normalize an audio file to peak value 1.0:

    ./normalize input.wav output.wav

## designer

Run disco/designer interactive demo native version:

    ./designer/disco-designer

Run disco/designer interactive demo web version (wasm needs http(s)
server, regular file URLs are insufficient):

    ( cd designer && python -m SimpleHTTPServer 8080 ) &
    sensible-browser http://localhost:8080/index.html

## bugs

Uses large stack-allocated arrays: if it crashes, try running this first:

    ulimit -s unlimited

## legal

Copyright (C) 2019,2022 Claude Heiland-Allen <mailto:claude@mathr.co.uk>

Copyleft: This is a free work, you can copy, distribute, and
modify it under the terms of the Free Art License
<http://artlibre.org/licence/lal/en/>
