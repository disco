// note: uses stack allocated arrays, may need `ulimit -s unlimited`

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sndfile.h>

#define OCTAVES 11
#define OCTAVESIZE (1 << (OCTAVES - 1))

#define OVERLAP 4

#define OHOP (OCTAVESIZE / OVERLAP)


struct datum
{
  short *audio;
  float energy[OCTAVES];
  float total;
};

struct database
{
  short *audio;
  size_t length;
  struct datum *data;
  size_t count;
};

int main(int argc, char **argv)
{
  if (argc != 3)
  {
    fprintf
      ( stderr
      , "usage:\n"
        "  %s in.wav out.wav\n"
      , argv[0]
      );
    return 1;
  }
  SF_INFO info;
  memset(&info, 0, sizeof(info));
  fprintf(stderr, "reading %s\n", argv[1]);
  SNDFILE *ifile = sf_open(argv[1], SFM_READ, &info);
  if (! ifile) abort();
  if (! (info.channels == 1)) abort();
  struct database db;
  db.audio = malloc(sizeof(*db.audio) * info.frames * info.channels);
  if (! (db.audio)) abort();
  db.count = (info.frames - (OVERLAP - 1) * OHOP) / OHOP;
  db.data = malloc(sizeof(*db.data) * db.count);
  if (! (db.data)) abort();
  sf_readf_short(ifile, db.audio, info.frames);
  sf_close(ifile);

  float window[OCTAVESIZE];
  memset(window, 0, sizeof(window));
  for (int i = 0; i < OCTAVESIZE; ++i)
  {
    window[i] = 0.5 - 0.5 * cos(i * 6.283185307179586 / OCTAVESIZE);
  }

  // build db
  for (size_t j = 0; j < db.count; ++j)
  {
    fprintf(stderr, "\rbuilding database %d%%", (int) (j * 100.0 / db.count));
    db.data[j].audio = db.audio + OHOP * j;

    float haar[2][OCTAVESIZE];
    const int src = 0;
    const int dst = 1;
    for (int i = 0; i < OCTAVESIZE; ++i)
    {
      haar[src][i] = (db.data[j].audio[i] / 32768.f) * window[i];
    }
  
    // compute Haar wavelet transform
    for (int length = OCTAVESIZE >> 1; length > 0; length >>= 1)
    {
      for (int i = 0; i < length; ++i)
      {
        float a = haar[src][2 * i + 0];
        float b = haar[src][2 * i + 1];
        float s = (a + b) / 2;
        float d = (a - b) / 2;
        haar[dst][         i] = s;
        haar[dst][length + i] = d;
      }
      for (int i = 0; i < OCTAVESIZE; ++i)
      {
        haar[src][i] = haar[dst][i];
      }
    }
  
    // compute energy per octave
    db.data[j].energy[0] = fabsf(haar[src][0]); // DC
    for ( int octave = 1, length = 1
        ; length <= OCTAVESIZE >> 1
        ; octave += 1, length <<= 1
        )
    {
      double rms = 0;
      for (int i = 0; i < length; ++i)
      {
        double a = haar[src][length + i];
        rms += a * a;
      }
      rms /= length * info.channels;
      rms = sqrt(fmax(rms, 0));
      db.data[j].energy[octave] = rms;
    }
    double total = 0;
    for (int i = 1; i < OCTAVES; ++i)
    {
      total += db.data[j].energy[i] * db.data[j].energy[i];
    }
    db.data[j].total = sqrt(total);
  }
  fprintf(stderr, "\n");

  // deep dream
  fprintf(stderr, "writing %s\n", argv[2]);
  SF_INFO outfo =
    { 0
    , info.samplerate
    , info.channels
    , SF_FORMAT_WAV | SF_FORMAT_FLOAT
    , 0
    , 0
    };
  SNDFILE *ofile = sf_open(argv[2], SFM_WRITE, &outfo);
  if (! ofile) abort();
  size_t length = info.samplerate * 1.5;
  float buffer[length];
  memset(buffer, 0, sizeof(buffer));
  size_t ixr = 0, ixw = 0;
  for (size_t j = 0; j < length; ++j)
  {
    fprintf(stderr, "\rdreaming %d%%", (int) (j * 100.0 / length));
    size_t r = ixr;

    float haar[2][OCTAVESIZE];
    const int src = 0;
    const int dst = 1;
    for (int i = 0; i < OCTAVESIZE; ++i)
    {
      haar[src][i] = buffer[r++] * window[i];
      if (r >= length)
      {
        r = 0;
      }
    }
  
    // compute Haar wavelet transform
    for (int length = OCTAVESIZE >> 1; length > 0; length >>= 1)
    {
      for (int i = 0; i < length; ++i)
      {
        float a = haar[src][2 * i + 0];
        float b = haar[src][2 * i + 1];
        float s = (a + b) / 2;
        float d = (a - b) / 2;
        haar[dst][         i] = s;
        haar[dst][length + i] = d;
      }
      for (int i = 0; i < OCTAVESIZE; ++i)
      {
        haar[src][i] = haar[dst][i];
      }
    }
  
    // compute energy per octave
    float E[OCTAVES];
    E[0] = fabsf(haar[src][0]); // DC
    for ( int octave = 1, length = 1
        ; length <= OCTAVESIZE >> 1
        ; octave += 1, length <<= 1
        )
    {
      double rms = 0;
      for (int i = 0; i < length; ++i)
      {
        double a = haar[src][length + i];
        rms += a * a;
      }
      rms /= length * info.channels;
      rms = sqrt(fmax(rms, 0));
      E[octave] = rms;
    }
    double total = 0;
    for (int i = 1; i < OCTAVES; ++i)
    {
      total += E[i] * E[i];
    }
    total = sqrt(total);

    // amplify matches
    double ntotal = 0;
    double nbuffer[OCTAVESIZE];
    memset(nbuffer, 0, sizeof(nbuffer));
    for (size_t k = 0; k < db.count; ++k)
    {
      double match = 0;
      for (int i = 1; i < OCTAVES; ++i)
      {
        match += db.data[k].energy[i] * E[i];
      }
      float gain = match / (db.data[k].total * total);
      if (isnan(gain) || isinf(gain)) continue;
      gain *= gain;
      gain *= gain;
      gain *= gain;
      gain *= gain;
      gain *= gain;
      gain *= gain;
      gain *= total;
      for (int i = 0; i < OCTAVESIZE; ++i)
      {
        nbuffer[i] += gain * (db.data[k].audio[i] / 32768.f);
      }
      ntotal += gain;
    }

    // feed back to audio
    const float noise = 0.01;
    double gain = 1 / (ntotal);
    if (isnan(gain) || isinf(gain)) gain = 0;
    float obuffer[OHOP];
    size_t w = ixw;
    for (int i = 0; i < OCTAVESIZE; ++i)
    {
      buffer[w] = tanhf
        ( buffer[w]
        + window[i] * nbuffer[i] * gain
        + noise * (rand() / (double) RAND_MAX - 0.5)
        );
      if (i < OHOP)
      {
        obuffer[i] = buffer[w];
      }
      w++;
      if (w >= length)
      {
        w = 0;
      }
    }
    sf_writef_float(ofile, obuffer, OHOP);
    ixr += OHOP;
    if (ixr >= length)
      ixr -= length;
    ixw += OHOP;
    if (ixw >= length)
      ixw -= length;
  }
  fprintf(stderr, "\n");
  sf_close(ofile);

  return 0;
}
