import math
import pandas as pd
import numpy as np
import scipy as scipy
from scipy.linalg import logm, expm, polar
from statsmodels.tsa.api import VAR

K = 129
H = 2

def load(filename):
  print ("Loading {}...".format(filename))
  input_data = pd.read_csv(filename, delim_whitespace=True, header=None, index_col=False, parse_dates=False)
  r = input_data.values
  c = 1j * np.hstack([np.zeros((r.shape[0],1)), r[:,K:], np.zeros((r.shape[0],1))]) + r[:,0:K]
  d = pd.DataFrame(data=c, index=[i for i in range(c.shape[0])], columns=['f'+str(i) for i in range(c.shape[1])])
  d.index = pd.to_datetime(d.index, unit='ms')
  print ("Doing VAR...")
  model = VAR(d)
  result = model.fit(H, trend='c')
  a = result.params.values[0,:]
  A = [ result.params.values[(1+i*K):(1+(i+1)*K),:] for i in range(H) ]
  return a, A

a, A = load("input1.csv")
b, B = load("input2.csv")
c, C = load("input3.csv")

print ("Preprocessing interpolation...")
def eig1(M):
  v = np.random.rand(M.shape[0])
  for _ in range(100):
    v /= np.linalg.norm(v)
    old_v = v
    v = np.dot(M, v)
  return np.abs(np.average(v / old_v))
bma = b - a
cmb = c - b
amc = a - c
I = np.identity(K)
def rad(m):
  return np.abs(scipy.sparse.linalg.eigs(m, k=1)[0])
ea = np.amax([ rad(A[i].T) for i in range(H) ])
eb = np.amax([ rad(B[i].T) for i in range(H) ])
ec = np.amax([ rad(C[i].T) for i in range(H) ])
for i in range(H):
  A[i] *= 0.95 / ea
  B[i] *= 0.95 / eb
  C[i] *= 0.95 / ec
ea = np.amax([ rad(A[i].T) for i in range(H) ])
eb = np.amax([ rad(B[i].T) for i in range(H) ])
ec = np.amax([ rad(C[i].T) for i in range(H) ])
A1B = [ np.linalg.inv(A[i].T) @ B[i].T for i in range(H) ]
B1C = [ np.linalg.inv(B[i].T) @ C[i].T for i in range(H) ]
C1A = [ np.linalg.inv(C[i].T) @ A[i].T for i in range(H) ]
RAB = [ logm(A1B[i]) for i in range(H) ]
RBC = [ logm(B1C[i]) for i in range(H) ]
RCA = [ logm(C1A[i]) for i in range(H) ]
print (ea, eb, ec)

print ("Loading input 4...")
input_data = pd.read_csv("input4.csv", delim_whitespace=True, header=None, index_col=False, parse_dates=False)
r = input_data.values
X = 1j * np.hstack([np.zeros((r.shape[0],1)), r[:,K:], np.zeros((r.shape[0],1))]) + r[:,0:K]
N = X.shape[0];

print ("Interpolating...")
output = []
Y = [ np.zeros(K) for i in range(H) ]
for k in range(N):
  s = (float(k) + 0.5) / float(N / 3.0)
  direction = int(s)
  s -= direction
  if direction is 0:
    M = [ A[i].T @ expm(s * RAB[i]) for i in range(H) ]
    ab = a + s * bma
    eab = ea ** (1 - s) * eb ** s
  elif direction is 1:
    M = [ B[i].T @ expm(s * RBC[i]) for i in range(H) ]
    ab = b + s * cmb
    eab = eb ** (1 - s) * ec ** s
  else:
    M = [ C[i].T @ expm(s * RCA[i]) for i in range(H) ]
    ab = c + s * amc
    eab = ec ** (1 - s) * ea ** s
  e = np.amax([ rad(M[i]) for i in range(H) ])
  M = [ M[i] * (eab / e) for i in range(H) ]
  f = [ rad(M[i]) for i in range(H) ]
  g = 1
  Yn = np.ravel(X[k,:]) + a + s * bma
  for j in range(H):
    Yn += M[j] @ Y[j]
  Y.insert(0, Yn)
  g = 1
  Y = [ y * g for y in Y[:H] ]
  g = 1/math.sqrt(sum([ np.real(np.dot(y, y.conj().T)) for y in Y ]) / (H * K))
  output.append(Yn)
  print (N, k, eab, e, f, g)

output = np.vstack(output)
output = np.hstack([np.real(output), np.imag(output)[:,1:(K-1)]])
np.savetxt("output.txt", output, delimiter=" ")
print ("Done!")
