#!/bin/bash
pd -nrt -noaudio -noprefs -batch -path . -open "$(dirname "$(readlink -e "${0}")")/fft-synthesis.pd"
