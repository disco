#ifndef DISCO_random_h
#define DISCO_random_h

#include <stdint.h>

#include "types.h"

#ifdef DISCO_RANDOM_MUSL
typedef uint64_t RANDOM;
#endif

#ifdef DISCO_RANDOM_BAD
typedef uint32_t RANDOM;
#endif

#ifdef DISCO_RANDOM_LFSR
typedef uint32_t RANDOM;
#endif

#ifdef DISCO_RANDOM_XORSHIFT32
typedef uint32_t RANDOM;
#endif

ULONG get(RANDOM seed);
RANDOM rnd(RANDOM seed);

#ifdef DISCO_DEBUG
extern ULONG u8rands;
#endif
extern RANDOM prng;
typedef union { ULONG l; UBYTE b[4]; } U4;
extern U4 prng_data;
extern UBYTE prng_count;
UBYTE u8rand(void);

#endif
