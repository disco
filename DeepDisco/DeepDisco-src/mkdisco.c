#include <stdio.h>

#include "ctzbtab.h"

int main(int argc, char **argv)
{
  int i, j, k, ix = 0, z, w;
  printf(
"void DISCO_bytes(struct DISCO0 *d, BYTE *out, LONG length\n"
"#ifdef DISCO_DEBUG\n"
", ULONG *histogram\n"
", ULONG *overflow\n"
", ULONG pass\n"
"#endif\n"
")\n"
"{\n"
"#if 1\n"
"\n"
"  ULONG *outl = (ULONG *) out;\n"
"  SAMPLE accum = d->accum;\n"
"  union { ULONG l; BYTE b[4]; } o;\n"
  );
  for (i = 0; i < 9; ++i)
  {
    printf(
"  SAMPLE octave%d = d->octave[%d];\n", i, i);
  }
  for (i = 0; i < 9; ++i)
  {
    printf(
"  WORD *gain%d = d->gain[%d];\n", i, i);
  }
  for (i = 0; i < 9; ++i)
  {
    printf(
"  UBYTE ox%d = d->ox[%d];\n", i, i);
  }
  for (i = 0; i < 9; ++i)
  {
    printf(
"  UBYTE uncorrelated%d = d->uncorrelated[%d];\n", i, i);
  }
  for (i = 0; i < 9; ++i)
  {
    for (j = 0; j <= i; ++j)
    {
      printf(
"  WORD *correlate%d%d = d->correlate[%d][%d];\n"
      , i, j, i, j);
    }
  }
  printf(
"  length >>= 8;\n"
"  while (length--)\n"
"  {\n"
  );
  for (j = 0; j < 64; ++j)
  {
    printf(
"    prng_data.l = get(prng = rnd(prng));\n"
    );
    for (k = 0; k < 4; ++k)
    {
      printf("/* %d */\n", ix);
      z = ctzbtab[ix++];
      printf(
"    accum -= octave%d;\n" /* z */
"#ifdef DISCO_DEBUG\n"
"    if (pass == 0)\n"
"    if (accum < -32768L || 32767L < accum)\n"
"    {\n"
"      *overflow += 1;\n"
"    }\n"
"#endif\n"
"    octave%d = gain%d[prng_data.b[%d]];\n" /* z, z, k */
"    accum += octave%d;\n" /* z */
     , z, z, z, k, z);
     printf(
"#ifdef DISCO_DEBUG\n"
"    if (pass == 0)\n"
"    if (accum < -32768L || 32767L < accum)\n"
"    {\n"
"      *overflow += 1;\n"
"    }\n"
"    if (pass == 0)\n"
"    histogram[(UWORD) ((WORD) accum)] += 1;\n"
"#endif\n"
"    if (++(ox%d) == COUNT)\n" /* z */
"    {\n"
"      SAMPLE s;\n"
"      ox%d = 0;\n" /* z */
"      prng_count = 3;\n"
"      uncorrelated%d = (UBYTE) (pull1(&d->noise[%d]\n" /* z, z */
"#ifdef DISCO_DEBUG\n"
", histogram\n"
", overflow\n"
", pass\n"
"#endif\n"
"      ) >> 8);\n"
      , z, z, z, z);
      for (w = 0; w <= z; ++w)
      {
        if (w == 0)
        {
          printf(
"      s = correlate%d%d[uncorrelated%d];\n" /* z, w, w */
          , z, w, w);
        }
        else
        {
          printf(
"      s += correlate%d%d[uncorrelated%d];\n" /* z, w, w */
          , z, w, w);
        }
        printf(
"#ifdef DISCO_DEBUG\n"
"      if (pass == 0)\n"
"      if (s < -32768L || 32767L < s)\n"
"      {\n"
"        *overflow += 1;\n"
"      }\n"
"      if (pass == 0)\n"
"      histogram[(UWORD) ((WORD) s)] += 1;\n"
"#endif\n"
        );
      }
      printf(
"      gain%d = &gaintab0[(UBYTE) (s >> 8)][0];\n" /* z */
"    }\n"
"    o.b[%d] = accum >> 8;\n" /* k */
      , z, k);
    } /* k */
    printf(
"    *outl++ = o.l;\n"
    );
  } /* j */
printf(
"  }\n"
"  d->accum = accum;\n"
);
  for (i = 0; i < 9; ++i)
  {
    printf(
"  d->octave[%d] = octave%d;\n", i, i);
  }
  for (i = 0; i < 9; ++i)
  {
    printf(
"  d->gain[%d] = gain%d;\n", i, i);
  }
  for (i = 0; i < 9; ++i)
  {
    printf(
"  d->ox[%d] = ox%d;\n", i, i);
  }
  for (i = 0; i < 9; ++i)
  {
    printf(
"  d->uncorrelated[%d] = uncorrelated%d;\n", i, i);
  }
  printf(
"\n"
"#else\n"
"\n"
"  while (length--)\n"
"  {\n"
"    SAMPLE o = pull0(d\n"
"#ifdef DISCO_DEBUG\n"
", histogram\n"
", overflow\n"
", pass\n"
"#endif\n"
"    );\n"
"#ifdef DISCO_DEBUG\n"
"    if (pass == -1)\n"
"    histogram[(UWORD) ((WORD) o)] += 1;\n"
"#endif\n"
"    *out++ = o >> 8;\n"
"  }\n"
"\n"
"#endif\n"
"}\n"
"\n"
  );
  return 0;
  (void) argc;
  (void) argv;
}
