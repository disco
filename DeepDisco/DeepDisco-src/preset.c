#include "preset.h"

#ifdef AMIGA

#include <exec/memory.h>
#include <dos/dos.h>
#include <proto/exec.h>
#include <proto/dos.h>

struct PRESET93_v1 *PRESET93_new(void)
{
  struct PRESET93_v1 *preset;
  if ((preset = AllocMem(sizeof(struct PRESET93_v1), MEMF_PUBLIC | MEMF_CLEAR)))
  {
    preset->magic = 0xDee9D15cUL;
    preset->bytes = sizeof(struct PRESET93_v1);
    preset->version = 1;
    preset->octaves = 9;
    preset->levels = 3;
    preset->channels = 1;
  }
  return preset;  
}

void PRESET93_delete(struct PRESET93_v1 *preset)
{
  FreeMem(preset, sizeof(struct PRESET93_v1));
}

ULONG PRESET93_load(struct PRESET93_v1 *preset, const char *filename)
{
  BPTR ifile;
  ULONG retval;
  if ((ifile = Open(filename, MODE_OLDFILE)))
  {
    if (sizeof(struct PRESET93_v1) == Read(ifile, preset, sizeof(struct PRESET93_v1)))
    {
      if (preset->magic == 0xDee9D15cUL && preset->bytes == sizeof(struct PRESET93_v1) && preset->version == 1 && preset->octaves == 9 && preset->levels == 3 && preset->channels == 1)
      {
        retval = 0;
      }
      else
      {
        retval = 3;
      }
    }
    else
    {
      retval = 2;
    }
    Close(ifile);
  }
  else
  {
    retval = 1;
  }
  return retval;
}

ULONG PRESET93_save(struct PRESET93_v1 *preset, const char *filename)
{
  BPTR ofile;
  if ((ofile = Open(filename, MODE_NEWFILE)))
  {
    if (sizeof(struct PRESET93_v1) == Write(ofile, preset, sizeof(struct PRESET93_v1)))
    {
      Close(ofile);
      return 0;
    }
    else
    {
      Close(ofile);
      return 2;
    }
  }
  else
  {
    return 1;
  }
}

#else

#include <stdio.h>
#include <stdlib.h>

ULONG native_to_big_endian(ULONG n)
{
  union { ULONG l; UBYTE b[4]; } endian;
  endian.b[0] = (n >> 24) & 0xFF;
  endian.b[1] = (n >> 16) & 0xFF;
  endian.b[2] = (n >>  8) & 0xFF;
  endian.b[3] = (n >>  0) & 0xFF;
  return endian.l;
}

ULONG big_endian_to_native(ULONG n)
{
  union { ULONG l; UBYTE b[4]; } endian;
  endian.l = n;
  return
    ((ULONG)(endian.b[0])) << 24 |
    ((ULONG)(endian.b[1])) << 16 |
    ((ULONG)(endian.b[2])) <<  8 |
    ((ULONG)(endian.b[3])) <<  0 ;
}


struct PRESET93_v1 *PRESET93_new(void)
{
  struct PRESET93_v1 *preset;
  if ((preset = calloc(1, sizeof(struct PRESET93_v1))))
  {
    preset->magic = 0xDee9D15cUL;
    preset->bytes = sizeof(struct PRESET93_v1);
    preset->version = 1;
    preset->octaves = 9;
    preset->levels = 3;
    preset->channels = 1;
  }
  return preset;  
}

void PRESET93_delete(struct PRESET93_v1 *preset)
{
  free(preset);
}

ULONG PRESET93_load(struct PRESET93_v1 *preset, const char *filename)
{
  FILE *ifile;
  ULONG retval;
  if ((ifile = fopen(filename, "rb")))
  {
    if (1 == fread(preset, sizeof(struct PRESET93_v1), 1, ifile))
    {
      preset->magic   = big_endian_to_native(preset->magic);
      preset->bytes   = big_endian_to_native(preset->bytes);
      preset->gain[0] = big_endian_to_native(preset->gain[0]);
      preset->gain[1] = big_endian_to_native(preset->gain[1]);
      preset->gain[2] = big_endian_to_native(preset->gain[2]);
      if (preset->magic == 0xDee9D15cUL && preset->bytes == sizeof(struct PRESET93_v1) && preset->version == 1 && preset->octaves == 9 && preset->levels == 3 && preset->channels == 1)
      {
        retval = 0;
      }
      else
      {
        retval = 3;
      }
    }
    else
    {
      retval = 2;
    }
    fclose(ifile);
  }
  else
  {
    retval = 1;
  }
  return retval;
}

ULONG PRESET93_save(struct PRESET93_v1 *preset, const char *filename)
{
  FILE *ofile;
  ULONG retval = 1;
  if ((ofile = fopen(filename, "wb")))
  {
    preset->magic   = native_to_big_endian(preset->magic);
    preset->bytes   = native_to_big_endian(preset->bytes);
    preset->gain[0] = native_to_big_endian(preset->gain[0]);
    preset->gain[1] = native_to_big_endian(preset->gain[1]);
    preset->gain[2] = native_to_big_endian(preset->gain[2]);
    if (1 == fwrite(preset, sizeof(struct PRESET93_v1), 1, ofile))
    {
      retval = 0;
    }
    else
    {
      retval = 2;
    }
    preset->magic   = big_endian_to_native(preset->magic);
    preset->bytes   = big_endian_to_native(preset->bytes);
    preset->gain[0] = big_endian_to_native(preset->gain[0]);
    preset->gain[1] = big_endian_to_native(preset->gain[1]);
    preset->gain[2] = big_endian_to_native(preset->gain[2]);
    fclose(ofile);
  }
  else
  {
    retval = 1;
  }
  return retval;
}

#endif
