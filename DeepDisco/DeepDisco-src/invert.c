#include <math.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include <gsl/gsl_linalg.h>

#include <sndfile.h>

#include "preset.h"
#include "ctzbtab.h"

#define COUNT 64

uint8_t rmstodb(double rms)
{
  double db = 255 * log(rms) / (0.5 * 0.23025850929940458 * 36) + 255;
  if (db < 0) db = 0;
  if (db > 255) db = 255;
  return floor(db + 0.5);
}

double db(double rms)
{
  return 20 * log10(fabs(rms));
}

struct stats
{
  double s0, s1, s2;
};

double mean(struct stats *s)
{
  return s->s1 / s->s0;
}

double stddev(struct stats *s)
{
  if (s->s0 > 0)
  {
    double t = s->s0 * s->s2 - s->s1 * s->s1;
    return t > 0 ? sqrt(t) / s->s0 : 0;
  }
  else
  {
    return 0;
  }
}

void push(struct stats *s, double in)
{
  s->s0 += 1;
  s->s1 += in;
  s->s2 += in * in;
}

struct cov
{
  uint64_t count;
  double mean[9];
  double covariance[9][9];
};

void cov_push(struct cov *self, double observation[9])
{
  double delta_at_nMin1[9], weighted_delta_at_n[9], D[9][9];
  int i, j;
  self->count += 1;
  for (i = 0; i < 9; ++i)
  {
    delta_at_nMin1[i] = observation[i] - self->mean[i];
  }
  for (i = 0; i < 9; ++i)
  {
    self->mean[i] += delta_at_nMin1[i] / self->count;
  }
  for (i = 0; i < 9; ++i)
  {
    weighted_delta_at_n[i] = (observation[i] - self->mean[i]) / self->count;
  }
  for (i = 0; i < 9; ++i)
  {
    for (j = 0; j < 9; ++j)
    {
/*
        D_at_n = np.broadcast_to(weighted_delta_at_n, self._shape).T
        D = (delta_at_nMin1 * self._identity).dot(D_at_n.T)
*/
      D[i][j] = delta_at_nMin1[i] * weighted_delta_at_n[j];
    }
  }
  for (i = 0; i < 9; ++i)
  {
    for (j = 0; j < 9; ++j)
    {
      self->covariance[i][j] = self->covariance[i][j] * (self->count - 1) / self->count + D[i][j];
    }
  }
}

struct state2
{
  struct stats data;
  double octave[9];
  struct stats meta[9];
  double accum;
  uint8_t ix;
};

void push2(struct state2 *s, double in, int pass)
{
  if (pass > 1)
  {
    double delta;
    int z = ctzbtab[s->ix++];
    s->accum *= (255.0/256.0);
    s->accum += in;
    delta = (s->accum - s->octave[z]) / (1 << z);
    s->octave[z] = s->accum;
    push(&s->data, in);
    push(&s->meta[z], delta);
  }
}

struct state1
{
  struct stats data;
  double octave[9];
  struct stats ostats[9];
  struct state2 meta[9];
  double observation[9];
  struct cov covariance;
  gsl_matrix_view covmatrix;
  double accum;
  uint8_t ix;
};

void push1(struct state1 *s, double in, int pass)
{
  if (pass > 0)
  {
    double delta;
    int z = ctzbtab[s->ix++];
    s->accum *= (255.0/256.0);
    s->accum += in;
    delta = (s->accum - s->octave[z]) / (1 << z);
    s->octave[z] = s->accum;
    push(&s->data, in);
    push(&s->ostats[z], delta);
    if (s->ostats[z].s0 >= COUNT)
    {
      s->observation[z] = stddev(&s->ostats[z]);
      if (pass == 1)
      {
        push2(&s->meta[z], s->observation[z], pass);
        cov_push(&s->covariance, s->observation);
      }
      else
      {
        /* latent = L^{-1} observation */
        int w;
        double latent[9];
        gsl_vector_view v = gsl_vector_view_array(&latent[0], 9);
        for (w = 0; w < 9; ++w)
        {
          latent[w] = s->observation[w];
        }
        gsl_blas_dtrsv(CblasLower, CblasNoTrans, CblasNonUnit, &s->covmatrix.matrix, &v.vector);
        push2(&s->meta[z], latent[z], pass);
      }
      s->ostats[z].s0 = 0;
      s->ostats[z].s1 = 0;
      s->ostats[z].s2 = 0;
    }
  }
}

struct state0
{
  struct stats data;
  double octave[9];
  struct stats ostats[9];
  struct state1 meta[9];
  double observation[9];
  struct cov covariance;
  gsl_matrix_view covmatrix;
  double accum;
  uint8_t ix;
};

void push0(struct state0 *s, double in, int pass)
{
  double delta;
  int z = ctzbtab[s->ix++], w;
  s->accum *= (255.0/256.0);
  s->accum += in;
  delta = (s->accum - s->octave[z]) / (1 << z);
  s->octave[z] = s->accum;
  push(&s->data, in);
  push(&s->ostats[z], delta);
  if (s->ostats[z].s0 >= COUNT)
  {
    s->observation[z] = stddev(&s->ostats[z]);
    if (pass == 0)
    {
      push1(&s->meta[z], s->observation[z], pass);
      cov_push(&s->covariance, s->observation);
    }
    else
    {
      /* latent = L^{-1} observation */
      double latent[9];
      gsl_vector_view v = gsl_vector_view_array(&latent[0], 9);
      for (w = 0; w < 9; ++w)
      {
        latent[w] = s->observation[w];
      }
      gsl_blas_dtrsv(CblasLower, CblasNoTrans, CblasNonUnit, &s->covmatrix.matrix, &v.vector);
      push1(&s->meta[z], latent[z], pass);
    }
    s->ostats[z].s0 = 0;
    s->ostats[z].s1 = 0;
    s->ostats[z].s2 = 0;
  }
}

struct state0 state;

int main(int argc, char **argv)
{
  double ma, d;
  int64_t a, i, j, k, pass;
  struct PRESET93_v1 *preset;
  if (! (argc > 2))
  {
    fprintf(stderr, "ERROR: usage: %s out.preset in1.wav in2.wav ...\n", argv[0]);
    return 1;
  }
  memset(&state, 0, sizeof(state));
  /*
  pass = 0 : compute covariance matrix at level 1
  pass = 1 : compute covariance matrices of level 1 latent variables at level 2
  pass = 3 : compute statistics of level 2 latent variables at level 3
  */
  preset = PRESET93_new();
  for (pass = 0; pass < 3; ++pass)
  {
    fprintf(stderr, "NOTICE: pass %d/3\n", (int) pass + 1);
    for (a = 2; a < argc; ++a)
    {
      SF_INFO info;
      SNDFILE *ifile;
      memset(&info, 0, sizeof(info));
      fprintf(stderr, "NOTICE: reading '%s'\n", argv[a]);
      ifile = sf_open(argv[a], SFM_READ, &info);
      if (ifile)
      {
        size_t bytes = sizeof(float) * info.channels * info.frames;
        if (bytes > 0)
        {
          float *input = malloc(bytes);
          if (bytes)
          {
            if (info.frames == sf_readf_float(ifile, input, info.frames))
            {
              sf_count_t n;
              for (n = 0; n < info.frames; ++n)
              {
                push0(&state, input[info.channels * n + 0], pass);
              }
            }
            else
            {
              fprintf(stderr, "WARNING: could not read '%s'\n", argv[a]);
            }
            free(input);
          }
          else
          {
            fprintf(stderr, "WARNING: could not allocate %ld bytes\n", bytes);
          }
        }
        else
        {
          fprintf(stderr, "WARNING: failed sizing '%s'\n", argv[a]);
        }
        sf_close(ifile);
      }
      else
      {
        fprintf(stderr, "WARNING: failed opening '%s'\n", argv[a]);
      }
    }
    if (pass == 0)
    {
      state.covmatrix = gsl_matrix_view_array(&state.covariance.covariance[0][0], 9, 9);
      gsl_linalg_cholesky_decomp(&state.covmatrix.matrix);
      ma = 0;
      for (i = 0; i < 9; ++i)
      {
        double sum = 0;
        for (j = 0; j < 9; ++j)
        {
          sum += state.covariance.covariance[i][j];
        }
        ma = sum > ma ? sum : ma;
      }
      for (i = 0; i < 9; ++i)
      {
        for (j = 0; j < 9; ++j)
        {
          preset->correlate0[i][j] = rmstodb(state.covariance.covariance[i][j] / ma);
        }
      }
    }
    else if (pass == 1)
    {
      gsl_set_error_handler_off();
      for (i = 0; i < 9; ++i)
      {
        state.meta[i].covmatrix = gsl_matrix_view_array(&state.meta[i].covariance.covariance[0][0], 9, 9);
        if (GSL_EDOM == gsl_linalg_cholesky_decomp(&state.meta[i].covmatrix.matrix))
        {
          fprintf(stderr, "WARNING: not positive definite at index %d\n", (int) i);
          if (i > 0)
          {
            for (j = 0; j < 9; ++j)
            {
              for (k = 0; k < 9; ++k)
              {
                preset->correlate1[i][j][k] = preset->correlate1[i-1][j][k];
              }
            }
          }
          else
          {
            abort();
          }
        }
        ma = 0;
        for (j = 0; j < 9; ++j)
        {
          double sum = 0;
          for (k = 0; k < 9; ++k)
          {
            sum += state.meta[i].covariance.covariance[j][k];
          }
          ma = sum > ma ? sum : ma;
        }
        for (j = 0; j < 9; ++j)
        {
          for (k = 0; k < 9; ++k)
          {
            preset->correlate1[i][j][k] = rmstodb(state.meta[i].covariance.covariance[j][k] / ma);
          }
        }
      }
    }
  }
  fprintf(stderr, "NOTICE: writing '%s'\n", argv[1]);
  ma = 0;
  for (i = 0; i < 9; ++i)
  for (j = 0; j < 9; ++j)
  for (k = 0; k < 9; ++k)
  {
    d = stddev(&state.meta[i].meta[j].meta[k]);
    ma = d > ma ? d : ma;
  }
  for (i = 0; i < 9; ++i)
  for (j = 0; j < 9; ++j)
  for (k = 0; k < 9; ++k)
    preset->db[i][j][k] = rmstodb(stddev(&state.meta[i].meta[j].meta[k]) / ma);
  PRESET93_save(preset, argv[1]);
  PRESET93_delete(preset);
  return 0;
}
