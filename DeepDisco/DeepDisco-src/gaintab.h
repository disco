#ifndef DISCO_gaintab_h
#define DISCO_gaintab_h

#include "preset.h"

extern WORD **gaintab0;
extern WORD **gaintab1;
extern WORD **gaintab2;

LONG gaintab_new(const struct PRESET93_v1 *preset);
void gaintab_free(LONG bytes);

#endif
