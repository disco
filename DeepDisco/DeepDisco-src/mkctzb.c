#include <stdio.h>

int ctz(uint8_t x)
{
  int n = 0;
  if (x == 0) return 8;
  while ((x & 1) == 0)
  {
    x >>= 1;
    n++;
  }
  return n;
}

int main(int argc, char **argv)
{
  int c;
  printf(
"#ifndef DISCO_ctzbtab_h\n"
"#define DISCO_ctzbtab_h\n"
"\n"
"#include \"types.h\"\n"
"\n"
"const UBYTE ctzbtab[256] = "
  );
  for (c = 0; c < 256; ++c)
  {
    printf("%c%d", c == 0 ? '{' : ',', ctzb(c));
  }
  printf("};\n"
"\n"
"#endif\n"
  );
  return 0;
}
