#include <string.h>
#include <time.h>

#include <exec/types.h>
#include <exec/memory.h>
#include <dos/dos.h>
#include <graphics/gfxbase.h>
#include <workbench/startup.h>
#include <devices/audio.h>
#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/alib.h>

#ifdef LATTICE
int CXBRK(void) { return(0); }     /* Disable SAS CTRL/C handling */
int chkabort(void) { return(0); }  /* really */
#endif

#define SR 22050

struct STATE
{
  UBYTE ctzb[256];
  WORD rs[9];
  WORD t;
  ULONG seed;
  UBYTE i;
};

struct STATE state;

UBYTE ctzb(UBYTE i)
{
  if (i)
  {
    UBYTE r = 0;
    while (! (i & 1))
    {
      i >>= 1;
      r += 1;
    }
    return r;
  }
  else
  {
    return 8;
  }
}

void dsp_init(struct STATE *s, ULONG seed)
{
  WORD b;
  for (b = 0; b < 256; ++b)
  {
    s->ctzb[b] = ctzb(b);
  }
  for (b = 0; b < 9; ++b)
  {
    s->rs[b] = 0;
  }
  s->t = 0;
  s->seed = seed;
  s->i = 0;
}

void dsp(struct STATE *s, BYTE *out, LONG length)
{
  WORD t = s->t;
  ULONG seed = s->seed;
  UBYTE i = s->i;
  UBYTE z;
  union
  {
    ULONG l;
    BYTE b[4];
  } pun;
  while (length -= 4)
  {
    seed ^= seed << 13;
    seed ^= seed >> 17;
    seed ^= seed << 5;
    pun.l = seed;
    {
      z = s->ctzb[i++];
      t -= s->rs[z];
      s->rs[z] = ((WORD)(pun.b[0]));
      t += s->rs[z];
      *out++ = t >> 4;
    }
    {
      z = s->ctzb[i++];
      t -= s->rs[z];
      s->rs[z] = ((WORD)(pun.b[1]));
      t += s->rs[z];
      *out++ = t >> 4;
    }
    {
      z = s->ctzb[i++];
      t -= s->rs[z];
      s->rs[z] = ((WORD)(pun.b[2]));
      t += s->rs[z];
      *out++ = t >> 4;
    }
    {
      z = s->ctzb[i++];
      t -= s->rs[z];
      s->rs[z] = ((WORD)(pun.b[3]));
      t += s->rs[z];
      *out++ = t >> 4;
    }
  }
  s->t = t;
  s->seed = seed;
  s->i = i;
}

struct GfxBase *GfxBase;

struct AUDIO
{
  struct IOAudio *io[2];
  struct MsgPort *port[2];
  struct Message *msg;
  ULONG device;
  ULONG clock;
  ULONG speed;
  UBYTE *data;
  ULONG size;
  UBYTE chan[4];
  UBYTE *chans[4];
  struct Task *task;
  BYTE oldpri;
};

void AUDIO_delete(struct AUDIO *a)
{
  if (! a->device) CloseDevice((struct IORequest *) a->io[0]);
  if (a->port[0]) DeletePort(a->port[0]);
  if (a->port[1]) DeletePort(a->port[1]);
  if (a->io[0]) FreeMem(a->io[0], sizeof(struct IOAudio));
  if (a->io[1]) FreeMem(a->io[1], sizeof(struct IOAudio));
  if (a->task) SetTaskPri(a->task, a->oldpri);
  if (a->data) FreeMem(a->data, a->size);
  FreeMem(a, sizeof(struct AUDIO));
}

struct AUDIO *AUDIO_new(ULONG isPAL, ULONG samplerate, ULONG blocksize)
{
  BYTE c = 0;
  struct AUDIO *a = (struct AUDIO *) AllocMem(sizeof(struct AUDIO), MEMF_PUBLIC | MEMF_CLEAR);
  if (! a)
  {
    return a;
  }
  a->device = 1;
  a->task = FindTask(NULL);
  a->oldpri = SetTaskPri(a->task, 21);
  a->clock = isPAL ? 3546895L : 3579545L;
  a->speed = a->clock / samplerate;
  a->size = blocksize * 2;
  a->data = (UBYTE *) AllocMem(a->size, MEMF_CHIP | MEMF_CLEAR);
  a->io[0] = (struct IOAudio *) AllocMem(sizeof(struct IOAudio), MEMF_PUBLIC | MEMF_CLEAR);
  a->io[1] = (struct IOAudio *) AllocMem(sizeof(struct IOAudio), MEMF_PUBLIC | MEMF_CLEAR);
  a->port[0] = CreatePort(0, 0);
  a->port[1] = CreatePort(0, 0);
  a->chan[0] = 1;
  a->chan[1] = 2;
  a->chan[2] = 4;
  a->chan[3] = 8;
  a->chans[0] = &a->chan[0];
  a->chans[1] = &a->chan[1];
  a->chans[2] = &a->chan[2];
  a->chans[3] = &a->chan[3];
  if (a->data && a->io[0] && a->port[0] && a->io[1] && a->port[1])
  {
    while (a->device && c < 4)
    {
      a->io[0]->ioa_Request.io_Message.mn_ReplyPort = a->port[0];
      a->io[0]->ioa_Request.io_Message.mn_Node.ln_Pri = 127;
      a->io[0]->ioa_AllocKey = 0;
      a->io[0]->ioa_Data = a->chans[c++];
      a->io[0]->ioa_Length = 1;
      a->device = OpenDevice(AUDIONAME, 0L, (struct IORequest *) a->io[0], 0L);
    }
    a->io[0]->ioa_Request.io_Command = CMD_WRITE;
    a->io[0]->ioa_Request.io_Flags = ADIOF_PERVOL;
    a->io[0]->ioa_Volume = 64;
    a->io[0]->ioa_Period = a->speed;
    a->io[0]->ioa_Cycles = 1;
    a->io[0]->ioa_Length = blocksize;
    *(a->io[1]) = *(a->io[0]);
    a->io[1]->ioa_Request.io_Message.mn_ReplyPort = a->port[1];
    a->io[0]->ioa_Data = a->data;
    a->io[1]->ioa_Data = a->data + blocksize;
  }
  if (a->device)
  {
    AUDIO_delete(a);
    a = 0;
  }
  return a;
}

int main1(BPTR out, ULONG ctrlc)
{
  UBYTE w;
  ULONG isPAL;
  ULONG samplerate = SR;
  const UBYTE haptic = 10;
  const ULONG blocksize = ((samplerate / haptic) / 4) * 4; /* block size MUST be multiple of 4 */
  struct AUDIO *audio;
  LONG bytes;
  int retval = 20;
  dsp_init(&state, time(0));
  if ((GfxBase = (struct GfxBase *) OpenLibrary("graphics.library", 0L)))
  {
    isPAL = !!(GfxBase->DisplayFlags & PAL);
    if (isPAL)
    {
      Write(out, "PAL\n", 4);
    }
    else
    {
      Write(out, "NTSC\n", 5);
    }
    if ((audio = AUDIO_new(isPAL, samplerate, blocksize)))
    {
      dsp(&state, audio->io[0]->ioa_Data, blocksize);
      dsp(&state, audio->io[1]->ioa_Data, blocksize);
      audio->io[0]->ioa_Length = blocksize;
      BeginIO((struct IORequest *) audio->io[0]);
      w = 0;
      while (1)
      {
        audio->io[! w]->ioa_Length = blocksize;
        BeginIO((struct IORequest *) audio->io[! w]);
        if (ctrlc & Wait(ctrlc | (1 << audio->port[w]->mp_SigBit))) { break; }
        while (! GetMsg(audio->port[w])) { }
        dsp(&state, audio->io[w]->ioa_Data, blocksize);
        w = ! w;
      }
      retval = 0;
      AUDIO_delete(audio);
    }
    else
    {
      Write(out, "audio error\n", 12);
    }
    CloseLibrary((struct Library *)GfxBase);
  }
  else
  {
    Write(out, "graphics error\n", 15);
  }
  return retval;
}

int main(int argc, char **argv)
{
  int retval = 20;
  if (argc == 0)
  {
    struct WBStartup *wb;
    if ((wb = (struct WBStartup *) argv))
    {
      BPTR out;
      if ((out = Open("CON:", MODE_NEWFILE)))
      {
        retval = main1(out, SIGBREAKF_CTRL_C);
        Close(out);
      }
      else
      {
        /* what do? */
      }
    }
    else
    {
      BPTR out;
      if ((out = Open("CON:", MODE_NEWFILE)))
      {
        Write(out, "workbench error\n", 16);
        Close(out);
      }
    }
  }
  else
  {
    if (argc == 1)
    {
      retval = main1(Output(), SIGBREAKF_CTRL_C);
    }
    else if (argc == 2 && 0 == strcmp("NOQUIT", argv[1]))
    {
      retval = main1(Output(), 0);
    }
    else
    {
      Write(Output(), "usage error\n", 12);
    }
  }
  return retval;
}

__entry const char version[] = "\0$VER: PinkNoise 1.1 (16.9.2021)";
