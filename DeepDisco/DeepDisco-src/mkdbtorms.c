#include <math.h>
#include <stdio.h>

int main(int argc, char **argv)
{
  for (int db = 0; db < 256; ++db)
  {
    printf("%d,", (int) (255 * (exp(0.5 * 0.23025850929940458 * (db - 255.0) / 255.0 * 36)) + 0.5));
  }
  printf("\n");
  return 0;
}
