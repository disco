#include <stdio.h>
#include <time.h>

#include <sndfile.h>

#include "disco.h"

struct DISCO0 disco;

int main(int argc, char **argv)
{
  struct PRESET93_v1 *preset;
  SF_INFO info = { 0, 88 << 8, 1, SF_FORMAT_WAV | SF_FORMAT_FLOAT, 0, 0 };
  SNDFILE *ofile;
  if (! (argc > 2))
  {
    fprintf(stderr, "ERROR: usage: %s preset.deepdisco out.wav\n", argv[0]);
    return 1;
  }
  preset = PRESET93_new();
  if (! preset)
  {
    fprintf(stderr, "ERROR: alloc failed\n");
    return 1;
  }
  if (0 != PRESET93_load(preset, argv[1]))
  {
    fprintf(stderr, "ERROR: could not load '%s'\n", argv[1]);
    PRESET93_delete(preset);
    return 1;
  }
  DISCO_init(&disco, preset, time(0));
  if ((ofile = sf_open(argv[2], SFM_WRITE, &info)))
  {
    int j;
    for (j = 0; j < 1 << 12; ++j)
    {
      BYTE bytes[1 << 12];
      float floats[1 << 12];
      int i;
      DISCO_bytes(&disco, bytes, 1 << 12);
      for (i = 0; i < 1 << 12; ++i)
      {
        floats[i] = bytes[i] / 128.0;
      }
      sf_writef_float(ofile, floats, 1 << 12);
    }
    sf_close(ofile);
  }
  else
  {
    return 1;
  }
  return 0;
}
