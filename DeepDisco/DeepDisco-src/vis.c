#include <stdio.h>

#include "preset.h"

int main(int argc, char **argv)
{
  const int d = 16 * 2 * 3 * 3 * 3;
  int i, j, k, kx, ky;
  struct PRESET93_v1 *preset;
  if (! (argc > 1))
  {
    fprintf(stderr, "ERROR: usage: %s in.deepdisco > out.svg\n", argv[0]);
    return 1;
  }
  preset = PRESET93_new();
  if (! preset)
  {
    fprintf(stderr, "ERROR: alloc failed\n");
    return 1;
  }
  if (0 != PRESET93_load(preset, argv[1]))
  {
    fprintf(stderr, "ERROR: could not load '%s'\n", argv[1]);
    PRESET93_delete(preset);
    return 1;
  }
  printf(
"<?xml version='1.0' encoding='UTF-8' standalone='no'?>\n"
"<!DOCTYPE svg PUBLIC '-//W3C//DTD SVG 1.1//EN' 'http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd'>\n"
"<svg width='%d' height='%d' viewBox='0 0 %d %d' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink'>\n"
"<rect fill='#fff' stroke='none' x='0' y='0' width='%d' height='%d' />\n"
"<g fill='#08f' stroke='#000' stroke-width='1'>\n"
  , d, 3 * d, d, 3 * d, d, 3 * d
  );
  for (i = 0; i < 9; ++i)
  {
    for (j = 0; j < 9; ++j)
    {
      k = 0;
      for (kx = 0; kx < 3; ++kx)
      for (ky = 0; ky < 3; ++ky)
      {
        int x = ((i) + kx * 9 + 0.5) * 16 * 2;
        int y = ((j) + ky * 9 + 0.5) * 16 * 2;
        double r = preset->db[i][j][k] / 16.0;
        printf("<circle cx='%d' cy='%d' r='%f' />\n", x, y, r);
        ++k;
      }
    }
  }
  for (i = 0; i < 9; ++i)
  {
    for (j = 0; j < 9; ++j)
    {
      int x = (((i)) + 0.5) * 3 * 16 * 2;
      int y = d + (((j)) + 0.5) * 3 * 16 * 2;
      double r = preset->correlate0[i][j] * 3 / 16.0;
      printf("<circle cx='%d' cy='%d' r='%f' />\n", x, y, r);
    }
  }
  k = 0;
  for (kx = 0; kx < 3; ++kx)
  for (ky = 0; ky < 3; ++ky)
  {
    for (i = 0; i < 9; ++i)
    {
      for (j = 0; j < 9; ++j)
      {
        int x = ((i) + kx * 9 + 0.5) * 16 * 2;
        int y = 2 * d + ((j) + ky * 9 + 0.5) * 16 * 2;
        double r = preset->correlate1[i][j][k] / 16.0;
        printf("<circle cx='%d' cy='%d' r='%f' />\n", x, y, r);
      }
    }
    ++k;
  }
  printf(
"</g>\n"
"</svg>\n"
  );
  PRESET93_delete(preset);
  return 0;
}
