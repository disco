#include "disco.h"
#include "gaintab.h"
#include "ctzbtab.h"
#include "random.h"

SAMPLE pull2(struct DISCO2 *d
#ifdef DISCO_DEBUG
, ULONG *histogram
, ULONG *overflow
, ULONG pass
#endif
)
{
  UBYTE z = ctzbtab[d->ix++];
  d->accum -= d->octave[z];
#ifdef DISCO_DEBUG
  if (pass == 2)
  if (d->accum < -32768L || 32767L < d->accum)
  {
    *overflow += 1;
  }
#endif
  d->octave[z] = d->gain[z][u8rand()];
  d->accum += d->octave[z];
#ifdef DISCO_DEBUG
  if (pass == 2)
  if (d->accum < -32768L || 32767L < d->accum)
  {
    *overflow += 1;
  }
  if (pass == 2)
  histogram[(UWORD) ((WORD) d->accum)] += 1;
#endif
  return d->accum;
}

SAMPLE pull1(struct DISCO1 *d
#ifdef DISCO_DEBUG
, ULONG *histogram
, ULONG *overflow
, ULONG pass
#endif
)
{
  UBYTE z = ctzbtab[d->ix++];
  d->accum -= d->octave[z];
#ifdef DISCO_DEBUG
  if (pass == 1)
  if (d->accum < -32768L || 32767L < d->accum)
  {
    *overflow += 1;
  }
#endif
  d->octave[z] = d->gain[z][u8rand()];
  d->accum += d->octave[z];
#ifdef DISCO_DEBUG
  if (pass == 1)
  if (d->accum < -32768L || 32767L < d->accum)
  {
    *overflow += 1;
  }
  if (pass == 1)
  histogram[(UWORD) ((WORD) d->accum)] += 1;
#endif
  if (++(d->ox[z]) == COUNT)
  {
    UBYTE w;
    SAMPLE s = 0;
    d->ox[z] = 0;
    d->uncorrelated[z] = (UBYTE) (pull2(&d->noise[z]
#ifdef DISCO_DEBUG
, histogram
, overflow
, pass
#endif
    ) >> 8);
    for (w = 0; w <= z; ++w)
    {
      s += d->correlate[z][w][d->uncorrelated[w]];
#ifdef DISCO_DEBUG
  if (pass == 1)
      if (s < -32768L || 32767L < s)
      {
        *overflow += 1;
      }
  if (pass == 1)
      histogram[(UWORD) ((WORD) s)] += 1;
#endif
    }
    d->gain[z] = &gaintab1[(UBYTE) (s >> 8)][0];
  }
  return d->accum;
}

SAMPLE pull0(struct DISCO0 *d
#ifdef DISCO_DEBUG
, ULONG *histogram
, ULONG *overflow
, ULONG pass
#endif
)
{
  UBYTE z = ctzbtab[d->ix++];
  d->accum -= d->octave[z];
#ifdef DISCO_DEBUG
  if (pass == 0)
  if (d->accum < -32768L || 32767L < d->accum)
  {
    *overflow += 1;
  }
#endif
  d->octave[z] = d->gain[z][u8rand()];
  d->accum += d->octave[z];
#ifdef DISCO_DEBUG
  if (pass == 0)
  if (d->accum < -32768L || 32767L < d->accum)
  {
    *overflow += 1;
  }
  if (pass == 0)
  histogram[(UWORD) ((WORD) d->accum)] += 1;
#endif
  if (++(d->ox[z]) == COUNT)
  {
    UBYTE w;
    SAMPLE s = 0;
    d->ox[z] = 0;
    d->uncorrelated[z] = (UBYTE) (pull1(&d->noise[z]
#ifdef DISCO_DEBUG
, histogram
, overflow
, pass
#endif
    ) >> 8);
    for (w = 0; w <= z; ++w)
    {
      s += d->correlate[z][w][d->uncorrelated[w]];
#ifdef DISCO_DEBUG
  if (pass == 0)
      if (s < -32768L || 32767L < s)
      {
        *overflow += 1;
      }
  if (pass == 0)
      histogram[(UWORD) ((WORD) s)] += 1;
#endif
    }
    d->gain[z] = &gaintab0[(UBYTE) (s >> 8)][0];
  }
  return d->accum;
}

LONG DISCO_init(struct DISCO0 *d, struct PRESET93_v1 *preset, ULONG seed)
{
  LONG bytes;
  UBYTE i, j, k;
  if ((bytes = gaintab_new(preset)))
  {
    for (i = 0; i < 9; ++i)
    {
      d->gain[i] = &gaintab0[0][0];
      for (j = 0; j < 9; ++j)
      {
        d->correlate[i][j] = &gaintab1[preset->correlate0[i][j]][0];
        d->noise[i].gain[j] = &gaintab1[0][0];
        for (k = 0; k < 9; ++k)
        {
          d->noise[i].correlate[j][k] = &gaintab1[preset->correlate1[i][j][k]][0];
          d->noise[i].noise[j].gain[k] = &gaintab2[preset->db[i][j][k]][0];
        }
      }
    }
  }
  prng ^= seed;
  return bytes;
}

void DISCO_deinit(LONG bytes)
{
  gaintab_free(bytes);
}

#if 0

void DISCO_bytes(struct DISCO0 *d, BYTE *out, LONG length
#ifdef DISCO_DEBUG
, ULONG *histogram
, ULONG *overflow
, ULONG pass
#endif
)
{
  while (length--)
  {
    SAMPLE o = pull0(d
#ifdef DISCO_DEBUG
, histogram
, overflow
, pass
#endif
    );
#ifdef DISCO_DEBUG
if (pass == -1)
    histogram[(UWORD) ((WORD) o)] += 1;
#endif
    *out++ = o >> 8;
  }
}

#else

#include "disco_unroll.c"

#endif
