#include <stdio.h>
#include <time.h>
#include <unistd.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_audio.h>

#include "disco.h"

struct DISCO0 disco;

int MIN(int a, int b)
{
  return a < b ? a : b;
}

static void audiocb(void *userdata, Uint8 *stream, int len) {
  float *out = (float *) stream;
  int nframes = len / sizeof(float) / 2;
  int k = 0, i, c;
  while (nframes > 0)
  {
    BYTE data[256];
    DISCO_bytes(&disco, data, MIN((nframes + 1) / 2, 256));
    for (i = 0; i < MIN(nframes, 512); ++i) {
      for (c = 0; c < 2; ++c) {
        out[k++] = data[i/2] / 128.0;
      }
    }
    nframes -= 512;
  }
  (void) userdata;
}

int main(int argc, char **argv)
{
  SDL_AudioSpec want, have;
  SDL_AudioDeviceID dev;
  struct PRESET93_v1 *preset;
  if (! (argc > 1))
  {
    fprintf(stderr, "ERROR: usage: %s preset.deepdisco\n", argv[0]);
    return 1;
  }
  preset = PRESET93_new();
  if (! preset)
  {
    fprintf(stderr, "ERROR: alloc failed\n");
    return 1;
  }
  if (0 != PRESET93_load(preset, argv[1]))
  {
    fprintf(stderr, "ERROR: could not load '%s'\n", argv[1]);
    PRESET93_delete(preset);
    return 1;
  }
  DISCO_init(&disco, preset, time(0));
  SDL_Init(SDL_INIT_AUDIO);
  want.freq = 44100;
  want.format = AUDIO_F32;
  want.channels = 2;
  want.samples = 4096;
  want.callback = audiocb;
  dev = SDL_OpenAudioDevice(NULL, 0, &want, &have, SDL_AUDIO_ALLOW_ANY_CHANGE);
  if (have.format != AUDIO_F32 || have.channels != 2)
  {
    fprintf(stderr, "want: %d %d %d %d\n", want.freq, want.format, want.channels, want.samples);
    fprintf(stderr, "have: %d %d %d %d\n", have.freq, have.format, have.channels, have.samples);
    fprintf(stderr, "error: bad audio parameters\n");
    return 1;
  }
  SDL_PauseAudioDevice(dev, 0);
  while (1)
  {
    sleep(1);
  }
  return 0;
}
