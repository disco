#include <stdio.h>
#include <string.h>

#ifndef DISCO_DEBUG
#error define DISCO_DEBUG
#endif

#include "gaintab.h"
#include "disco.h"
#include "random.h"
#include "preset.h"

struct DISCO0 disco;
ULONG histogram[1 << 16];
ULONG overflow = 9;
BYTE buffer[1 << 16];

int main(int argc, char **argv)
{
  LONG lo[3] = { 0, 0, 0 };
  LONG hi[3] = { 1 << 24, 1 << 24, 1 << 24 };
  LONG md[3] = { 7281L, 7281L, 338940L };
  BYTE w;
  LONG i;
  struct PRESET93_v1 *preset;
  if (! (argc > 2))
  {
    fprintf(stderr, "ERROR: usage: %s in.deepdisco out.deepdisco > histogram.dat\n", argv[0]);
    return 1;
  }
  if (! (preset = PRESET93_new()))
  {
    fprintf(stderr, "ERROR: could not allocate preset\n");
    return 1;
  }
  if (0 != PRESET93_load(preset, argv[1]))
  {
    fprintf(stderr, "ERROR: could not load preset '%s'\n", argv[1]);
    PRESET93_delete(preset);
    return 1;
  }
  for (w = 2; w >= 0; --w)
  {
    fprintf(stderr, "NOTICE: pass %d/3\n", (int) (3 - w));
    while (lo[w] + 1 < hi[w])
    {
      LONG mi, ma;
      int64_t lhs, rhs;
      md[w] = (lo[w] + hi[w]) >> 1;
      fprintf(stderr, "INFO: %d %d %d", md[0], md[1], md[2]);
      lhs = (int64_t) (((md[w] * 255 + 0x7F) >> 1) + 0x7F);
      rhs = ((((int64_t) md[w]) * 255 + 0x7F) >> 1) + 0x7F;
      if (lhs != rhs || ! (lhs >> 24 == 0 || lhs >> 24 == -1))
      {
        hi[w] = md[w];
        fprintf(stderr, " BAD %ld %ld\n", lhs, rhs);
        continue;
      }
      memset(histogram, 0, sizeof(histogram));
      memset(&disco, 0, sizeof(disco));
      u8rands = 0;
      overflow = 0;
      preset->gain[0] = md[0];
      preset->gain[1] = md[1];
      preset->gain[2] = md[2];
      DISCO_init(&disco, preset, -1);
      for (i = 0; i < 1 << 12; ++i)
      {
        DISCO_bytes(&disco, buffer, 1 << 16, histogram, &overflow, w);
        if (overflow)
        {
          break;
        }
      }
      fprintf(stderr, " (%u u8rands)", u8rands);
      mi = -32678L;
      ma = 32767L;
      for (i = -32768L; i <= 32767L; ++i)
      {
        UWORD k = (UWORD) ((WORD) i);
        if (histogram[k])
        {
          mi = i;
          break;
        }
      }
      for (i = 32767L; i >= -32768L; --i)
      {
        UWORD k = (UWORD) ((WORD) i);
        if (histogram[k])
        {
          ma = i;
          break;
        }
      }
      fprintf(stderr, " %d %d", (int) mi, (int) ma);
      if (overflow)
      {
        hi[w] = md[w];
        fprintf(stderr, " BAD %u\n", overflow);
      }
      else
      {
        lo[w] = md[w];
        fprintf(stderr, " GOOD %ld\n", lhs);
      }
    }
    md[w] = lo[w];
  }
  preset->gain[0] = md[0];
  preset->gain[1] = md[1];
  preset->gain[2] = md[2];
  if (0 == PRESET93_save(preset, argv[2]))
  {
    fprintf(stderr, "NOTICE: saved preset '%s'\n", argv[2]);
  }
  else
  {
    fprintf(stderr, "ERROR: could not save preset '%s'\n", argv[2]);
    return 1;
  }
  memset(histogram, 0, sizeof(histogram));
  memset(&disco, 0, sizeof(disco));
  u8rands = 0;
  overflow = 0;
  DISCO_init(&disco, preset, -1);
  for (i = 0; i < 1 << 12; ++i)
  {
    DISCO_bytes(&disco, buffer, 1 << 16, histogram, &overflow, -1);
  }
  for (i = 0; i < 16; ++i)
  {
    LONG total = 0;
    LONG mi = -(1L << i);
    LONG ma =  (1L << i) - 1;
    LONG j;
    for (j = mi; j <= ma; ++j)
    {
      UWORD k = (UWORD) ((WORD) j);
      total += histogram[k];
    }
    printf("%d %d\n", (int) (i + 1), (int) total);
  }
  return 0;
}

