---
title: Deep Disco
author: Claude Heiland-Allen
keywords: Amiga, noise, realtime
abstract: Real noises are neither white nor pink.
date: 2021-09-16
geometry: margin=2cm
...

# Deep Disco

<a href='amiga-500.jpg' title='Amiga 500'><img src='amiga-500-thumb.jpg' alt='Amiga 500' /></a>

<audio controls>
<source src="DeepDisco.mp3" type="audio/mpeg">
<source src="DeepDisco.ogg" type="audio/ogg; codecs=vorbis">
</audio>

<https://mathr.co.uk/deep-disco>

## Concept

White noise has equal energy per frequency.  It is easy to generate
with a computer: make a stream of uncorrelated pseudo-random numbers,
much like throwing dice repeatedly.

Pink noise has equal energy per octave.  It can be generated on a
computer by adding together many white noise streams at different
speeds: one for each octave of the output.

Real noises are neither white not pink.  *Deep Disco* modifies the
Voss-Clarke-Gardner-McCartney pink noise algorithm
to set the energy of each of 9 octaves individually.  The audio
output's "unpink" noise is itself controlled by 9 other unpink noise
generators at subsonic frequencies, each of which is controlled by
another 9.
Correlations between the octaves are also controlled, altogether over
1000 parameters that are chosen by the artist.

## Implementation

The Amiga is a family of personal computers introduced by Commodore in
1985.  Based on the Motorola 68000 microprocessor, the Amiga includes
custom hardware to accelerate graphics and sound.  The best-selling
Amiga 500 (1987) sold four to six million units in the late 1980s and
early 1990s.  The sound chip, named Paula, supports four 8-bit PCM
sound channels.

Deep Disco runs on original Amiga hardware.  It was developed in the
C programming language with the VBCC compiler on Debian Linux (using
the FS-UAE emulator for testing).  To transfer the program to the old
hardware, custom networking software was written in C (Linux side) and
Blitz Basic 2 (Amiga side) that uses the MIDI SysEx protocol for data
exchange.

## Download

Amiga DiskMasher archive: [DeepDisco.dms](DeepDisco.dms) ([sig](DeepDisco.dms.sig))

For Amiga, see <https://aminet.net/package/util/arc/dms111>.
For Linux, you can use `xdms` to convert DMS to ADF and `unadf` to
extract files from ADF.

## Usage

Build the programs for Linux:

    make

Analyze audio to make a preset:

    ./invert preset.deepdisco a.wav b.wav c.wav ...

Optimize the preset gains:

    ./debug preset.deepdisco preset.deepdisco > preset.dat

Visualize the preset:

    ./vis preset.deepdisco > preset.svg

Generate sound with SDL2:

    ./deepdisco-sdl2 preset.deepdisco

Generate sound with JACK:

    ./deepdisco-jack preset.deepdisco

Build the program on Linux for AmigaOS using VBCC cross-compiler:

    make amiga

Generate sound on Amiga:

    DeepDisco68000 preset.deepdisco

## See Also

Part of the artist's *disco* project for audio fingerprint
discrimination and resynthesis: <https://mathr.co.uk/disco>

## Legal

Copyright (C) 2021 Claude Heiland-Allen <mailto:claude@mathr.co.uk>

Copyleft: This is a free work, you can copy, distribute, and
modify it under the terms of the Free Art License
<http://artlibre.org/licence/lal/en/>

## References

- Richard F. Voss and John Clarke, *1/f noise in music: Music from 1/f noise*, The Journal of the Acoustical Society of America 63 258 (1978) <https://doi.org/10.1121/1.381721>
- Martin Gardner, *Mathematical Games: White and brown music, fractal curves and 1/f fluctuations*, Scientific American 238 4 pp.16-33 (April 1978) <http://www.jstor.org/stable/24955701>
- Martin Gardner, *White, Brown, and Fractal Music* in *Fractal Music, Hypercards, and More: Mathematical Recreations from Scientific American Magazine*, W. H. Freeman (1991), ISBN 0716721880, 9780716721888
- James McCartney, *noise generation* in *music-dsp mailing list* (2nd September 1999) <https://music.columbia.edu/pipermail/music-dsp/1999-September/035431.html>

---
<https://mathr.co.uk>
