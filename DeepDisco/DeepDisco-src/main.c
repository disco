#include <string.h>
#include <time.h>

#include <exec/types.h>
#include <exec/memory.h>
#include <dos/dos.h>
#include <graphics/gfxbase.h>
#include <workbench/startup.h>
#include <devices/audio.h>
#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/alib.h>

#ifdef DISCO_ADAPTIVE
#include <devices/timer.h>
#include <proto/timer.h>
#endif

#undef DISCO_DEBUG
#include "disco.h"

#ifdef LATTICE
/* Disable SAS CTRL/C handling */
int CXBRK(void) { return(0); }
int chkabort(void) { return(0); }
#endif

#ifdef DISCO_ADAPTIVE
struct Device *TimerBase;
#endif
struct GfxBase *GfxBase;

enum
{
  FILTER_QUERY = 0,
  FILTER_SET = 1,
} FILTER_ACTION;

int AUDIO_filter(enum FILTER_ACTION action, int value)
{
  const UBYTE bit = 0x02;
  const UBYTE mask = ~bit;
  volatile UBYTE *CIA_A_PRA = (UBYTE *) 0xBFE001UL;
  UBYTE old, new;
  switch (action)
  {
    case FILTER_QUERY:
      old = *CIA_A_PRA;
      break;
    case FILTER_SET:
      if (! value)
      {
        old = *CIA_A_PRA;
        new = old;
        new |= bit;
        *CIA_A_PRA = new;
      }
      else
      {
        old = *CIA_A_PRA;
        new = old;
        new &= mask;
        *CIA_A_PRA = new;
      }
  }
  return !!(old * bit);
}

struct AUDIO
{
  struct IOAudio *io[2];
  struct MsgPort *port[2];
  struct Message *msg;
  ULONG device;
  ULONG clock;
  ULONG speed;
  UBYTE *data;
  ULONG size;
  UBYTE chan[4];
  UBYTE *chans[4];
  struct Task *task;
  BYTE oldpri;
};

void AUDIO_delete(struct AUDIO *a)
{
  if (! a->device) CloseDevice((struct IORequest *) a->io[0]);
  if (a->port[0]) DeletePort(a->port[0]);
  if (a->port[1]) DeletePort(a->port[1]);
  if (a->io[0]) FreeMem(a->io[0], sizeof(struct IOAudio));
  if (a->io[1]) FreeMem(a->io[1], sizeof(struct IOAudio));
  if (a->task) SetTaskPri(a->task, a->oldpri);
  if (a->data) FreeMem(a->data, a->size);
  FreeMem(a, sizeof(struct AUDIO));
}

struct AUDIO *AUDIO_new(ULONG isPAL, ULONG samplerate, ULONG blocksize)
{
  BYTE c = 0;
  struct AUDIO *a = (struct AUDIO *) AllocMem(sizeof(struct AUDIO), MEMF_PUBLIC | MEMF_CLEAR);
  if (! a)
  {
    return a;
  }
  a->device = 1;
  a->task = FindTask(NULL);
  a->oldpri = SetTaskPri(a->task, 21);
  a->clock = isPAL ? 3546895L : 3579545L;
  a->speed = a->clock / samplerate;
  a->size = blocksize * 2;
  a->data = (UBYTE *) AllocMem(a->size, MEMF_CHIP | MEMF_CLEAR);
  a->io[0] = (struct IOAudio *) AllocMem(sizeof(struct IOAudio), MEMF_PUBLIC | MEMF_CLEAR);
  a->io[1] = (struct IOAudio *) AllocMem(sizeof(struct IOAudio), MEMF_PUBLIC | MEMF_CLEAR);
  a->port[0] = CreatePort(0, 0);
  a->port[1] = CreatePort(0, 0);
  a->chan[0] = 1;
  a->chan[1] = 2;
  a->chan[2] = 4;
  a->chan[3] = 8;
  a->chans[0] = &a->chan[0];
  a->chans[1] = &a->chan[1];
  a->chans[2] = &a->chan[2];
  a->chans[3] = &a->chan[3];
  if (a->data && a->io[0] && a->port[0] && a->io[1] && a->port[1])
  {
    while (a->device && c < 4)
    {
      a->io[0]->ioa_Request.io_Message.mn_ReplyPort = a->port[0];
      a->io[0]->ioa_Request.io_Message.mn_Node.ln_Pri = 127;
      a->io[0]->ioa_AllocKey = 0;
      a->io[0]->ioa_Data = a->chans[c++];
      a->io[0]->ioa_Length = 1;
      a->device = OpenDevice(AUDIONAME, 0L, (struct IORequest *) a->io[0], 0L);
    }
    a->io[0]->ioa_Request.io_Command = CMD_WRITE;
    a->io[0]->ioa_Request.io_Flags = ADIOF_PERVOL;
    a->io[0]->ioa_Volume = 64;
    a->io[0]->ioa_Period = a->speed;
    a->io[0]->ioa_Cycles = 1;
    a->io[0]->ioa_Length = blocksize;
    *(a->io[1]) = *(a->io[0]);
    a->io[1]->ioa_Request.io_Message.mn_ReplyPort = a->port[1];
    a->io[0]->ioa_Data = a->data;
    a->io[1]->ioa_Data = a->data + blocksize;
  }
  if (a->device)
  {
    AUDIO_delete(a);
    a = 0;
  }
  return a;
}

#ifdef DISCO_ADAPTIVE

struct TIMER
{
  struct timerequest *io;
  struct MsgPort *port;
  ULONG device;
};

void TIMER_delete(struct TIMER *t)
{
  TimerBase = 0;
  if (! t->device) CloseDevice((struct IORequest *) t->io);
  if (t->port) DeletePort(t->port);
  if (t->io) FreeMem(t->io, sizeof(struct timerequest));
  FreeMem(t, sizeof(struct TIMER));
}

struct TIMER *TIMER_new(void)
{
  struct TIMER *t = (struct TIMER *) AllocMem(sizeof(struct TIMER), MEMF_PUBLIC | MEMF_CLEAR);
  if (! t)
  {
    return t;
  }
  t->device = 1;
  t->io = (struct timerequest *) AllocMem(sizeof(struct timerequest), MEMF_PUBLIC | MEMF_CLEAR);
  t->port = CreatePort(0, 0);
  if (t->io && t->port)
  {
    t->io->tr_node.io_Message.mn_ReplyPort = t->port;
    t->device = OpenDevice(TIMERNAME, UNIT_MICROHZ, (struct IORequest *) t->io, 0L);
    TimerBase = t->io->tr_node.io_Device;
  }
  if (t->device)
  {
    TIMER_delete(t);
    t = 0;
  }
  return t;
}

struct timeval TIMER_time(struct TIMER *t)
{
  t->io->tr_node.io_Command = TR_GETSYSTIME;
  t->io->tr_node.io_Flags = IOF_QUICK;
  DoIO((struct IORequest *) t->io);
  return t->io->tr_time;
}

#endif

struct DISCO0 disco;

int main1(BPTR out, char *filename, ULONG ctrlc)
{
  UBYTE w;
  ULONG isPAL;
  ULONG samplerate = 88 << 8;
  const UBYTE haptic = 11;
  const ULONG blocksize = samplerate / haptic; /* block size MUST be multiple of 256 */
#ifdef DISCO_ADAPTIVE
  UBYTE s[8];
  ULONG u, n, factor;
  const ULONG BIGGER = 260;
  const ULONG SMALLER = 252;
  struct timeval end, start;
  ULONG speed;
  ULONG slow_down_limit = 950000L / haptic;
  ULONG speed_up_limit  = 850000L / haptic;
  struct TIMER *timer;
#endif
  struct AUDIO *audio;
  struct PRESET93_v1 *preset;
  LONG bytes;
  int retval = 20;
  if ((preset = PRESET93_new()))
  {
    if (0 == PRESET93_load(preset, filename))
    {
      if ((bytes = DISCO_init(&disco, preset, time(0))))
      {
#ifdef DISCO_ADAPTIVE
        if ((timer = TIMER_new()))
        {
#endif
          if ((GfxBase = (struct GfxBase *) OpenLibrary("graphics.library", 0L)))
          {
            isPAL = !!(GfxBase->DisplayFlags & PAL);
            if (isPAL)
            {
              Write(out, "PAL\n", 4);
            }
            else
            {
              Write(out, "NTSC\n", 5);
            }
            if ((audio = AUDIO_new(isPAL, samplerate, blocksize)))
            {
              int filter = AUDIO_filter(FILTER_SET, 0);
#ifdef DISCO_ADAPTIVE
              speed = audio->speed;
#endif
              DISCO_bytes(&disco, audio->io[0]->ioa_Data, blocksize);
              DISCO_bytes(&disco, audio->io[1]->ioa_Data, blocksize);
              audio->io[0]->ioa_Length = blocksize;
              BeginIO((struct IORequest *) audio->io[0]);
              w = 0;
              while (1)
              {
                audio->io[! w]->ioa_Length = blocksize;
                BeginIO((struct IORequest *) audio->io[! w]);
                if (ctrlc & Wait(ctrlc | (1 << audio->port[w]->mp_SigBit))) { break; }
                while (! GetMsg(audio->port[w])) { }
#ifdef DISCO_ADAPTIVE
                start = TIMER_time(timer);
#endif
                DISCO_bytes(&disco, audio->io[w]->ioa_Data, blocksize);
#ifdef DISCO_ADAPTIVE
                end = TIMER_time(timer);
                SubTime(&end, &start);
                u = end.tv_sec * 1000000L + end.tv_usec;
                factor = 0;
                if (u > slow_down_limit)
                {
                  factor = BIGGER;
                }
                else if (u < speed_up_limit)
                {
                  factor = SMALLER;
                }
                if (factor)
                {
                  speed *= factor;
                  speed >>= 8;
                  slow_down_limit *= factor;
                  slow_down_limit >>= 8;
                  speed_up_limit *= factor;
                  speed_up_limit >>= 8;
                  audio->io[0]->ioa_Period = speed;
                  audio->io[1]->ioa_Period = speed;
                  n = audio->clock / speed;
                  s[7] = '\n';
                  s[6] =      '0' + (n % 10);        n /= 10;
                  s[5] = n ? ('0' + (n % 10)) : ' '; n /= 10;
                  s[4] = n ? ('0' + (n % 10)) : ' '; n /= 10;
                  s[3] = n ? ('0' + (n % 10)) : ' '; n /= 10;
                  s[2] = n ? ('0' + (n % 10)) : ' '; n /= 10;
                  s[1] = n ? '*' : ' ';
                  s[0] = factor == BIGGER ? '-' : '+';
                  Write(out, s, 8);
                }
#endif
                w = ! w;
              }
              retval = 0;
              AUDIO_filter(FILTER_SET, filter);
              AUDIO_delete(audio);
            }
            else
            {
              Write(out, "audio error\n", 12);
            }
            CloseLibrary((struct Library *)GfxBase);
          }
          else
          {
            Write(out, "graphics error\n", 15);
          }
#ifdef DISCO_ADAPTIVE
          TIMER_delete(timer);
        }
        else
        {
          Write(out, "timer error\n", 12);
        }
#endif
        DISCO_deinit(bytes);
      }
      else
      {
        Write(out, "init error\n", 11);
      }
    }
    else
    {
      Write(out, "load error\n", 11);
    }
    PRESET93_delete(preset);
  }
  else
  {
    Write(out, "alloc error\n", 12);
  }
  return retval;
}

int main(int argc, char **argv)
{
  int retval = 20;
  if (argc == 0)
  {
    struct WBStartup *wb;
    if ((wb = (struct WBStartup *) argv))
    {
      if (wb->sm_NumArgs == 2)
      {
        BPTR olddir, out;
        olddir = CurrentDir(wb->sm_ArgList[1].wa_Lock);
        if ((out = Open("CON:", MODE_NEWFILE)))
        {
          retval = main1(out, wb->sm_ArgList[1].wa_Name, SIGBREAKF_CTRL_C);
          Close(out);
        }
        else
        {
          /* what do? */
        }
        CurrentDir(olddir);
      }
      else
      {
        BPTR out;
        if ((out = Open("CON:", MODE_NEWFILE)))
        {
          Write(out, "usage error\n", 12);
          Close(out);
        }
        else
        {
          /* what do? */
        }
      }
    }
    else
    {
      BPTR out;
      if ((out = Open("CON:", MODE_NEWFILE)))
      {
        Write(out, "workbench error\n", 16);
        Close(out);
      }
    }
  }
  else
  {
    if (argc == 2)
    {
      retval = main1(Output(), argv[1], SIGBREAKF_CTRL_C);
    }
    else if (argc == 3 && 0 == strcmp("NOQUIT", argv[2]))
    {
      retval = main1(Output(), argv[1], 0);
    }
    else
    {
      Write(Output(), "usage error\n", 12);
    }
  }
  return retval;
}

__entry const char version[] = "\0$VER: DeepDisco 1.1 (16.9.2021) Smoltech";
