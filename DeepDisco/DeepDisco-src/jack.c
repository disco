#include <stdio.h>
#include <unistd.h>

#include <jack/jack.h>

#include "disco.h"

jack_client_t *client;
jack_port_t *port;
struct DISCO0 disco;

int MIN(int a, int b)
{
  return a < b ? a : b;
}

int processcb(jack_nframes_t nframes, void *arg)
{
  int i, n = nframes;
  jack_default_audio_sample_t *out = (jack_default_audio_sample_t *) jack_port_get_buffer(port, nframes);
  while (n > 0)
  {
    BYTE data[256];
    /* this will be wrong when nframes is odd */
    DISCO_bytes(&disco, data, MIN((n + 1) / 2, 256));
    for (i = 0; i < MIN(n, 512); ++i)
    {
      out[i] = data[i/2] / 128.0;
    }
    n -= 512;
  }
  return 0;
  (void) arg;
}

int main(int argc, char **argv)
{
  struct PRESET93_v1 *preset;
  if (! (argc > 1))
  {
    fprintf(stderr, "ERROR: usage: %s preset.deepdisco\n", argv[0]);
    return 1;
  }
  preset = PRESET93_new();
  if (! preset)
  {
    fprintf(stderr, "ERROR: alloc failed\n");
    return 1;
  }
  if (0 != PRESET93_load(preset, argv[1]))
  {
    fprintf(stderr, "ERROR: could not load '%s'\n", argv[1]);
    PRESET93_delete(preset);
    return 1;
  }
  DISCO_init(&disco, preset, time(0));
  client = jack_client_open("deepdisco", JackNoStartServer, 0);
  jack_set_process_callback(client, processcb, 0);
  port = jack_port_register(client, "output_1", JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);
  jack_activate(client);
  jack_connect(client, "deepdisco:output_1", "system:playback_1");
  jack_connect(client, "deepdisco:output_1", "system:playback_2");
  while (1)
  {
    sleep(1);
  }
  return 0;
}
