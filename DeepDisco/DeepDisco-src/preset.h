#ifndef DISCO_preset_h
#define DISCO_preset_h

#include "types.h"

/* big-endian data on disk, native-endian in memory */

struct PRESET93_v0
{
  ULONG magic;    /* 0xDee9D15c */
  ULONG bytes;    /* sizeof(struct PRESET93_v0) */
  UBYTE version;  /* 0 */
  UBYTE octaves;  /* 9 */
  UBYTE levels;   /* 3 */
  UBYTE channels; /* 1 */
  ULONG gain[3];
  UBYTE db[9][9][9];
  UBYTE pad[3];
};

struct PRESET93_v1
{
  ULONG magic;    /* 0xDee9D15c */
  ULONG bytes;    /* sizeof(struct PRESET93_v1) */
  UBYTE version;  /* 1 */
  UBYTE octaves;  /* 9 */
  UBYTE levels;   /* 3 */
  UBYTE channels; /* 1 */
  ULONG gain[3];
  UBYTE db[9][9][9];
  UBYTE pad1[3];
  UBYTE correlate0[9][9];
  UBYTE pad2[3];
  UBYTE correlate1[9][9][9];
  UBYTE pad3[3];
};

struct PRESET93_v1 *PRESET93_new();
void PRESET93_delete(struct PRESET93_v1 *preset);
ULONG PRESET93_load(struct PRESET93_v1 *preset, const char *filename);
ULONG PRESET93_save(struct PRESET93_v1 *preset, const char *filename); /* 0 == success */

#endif
