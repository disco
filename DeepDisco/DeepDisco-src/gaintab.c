#include "gaintab.h"

#ifdef AMIGA
#include <exec/memory.h>
#include <proto/exec.h>
#include <proto/dos.h>
#else
#include <stdio.h>
#include <stdlib.h>
#endif

const UBYTE dbtorms[256] = { 4,4,4,4,4,4,4,5,5,5,5,5,5,5,5,5,5,5,5,6,6,6,6,6,6,6,6,6,6,6,7,7,7,7,7,7,7,7,7,8,8,8,8,8,8,8,9,9,9,9,9,9,9,10,10,10,10,10,10,11,11,11,11,11,11,12,12,12,12,12,13,13,13,13,13,14,14,14,14,15,15,15,15,16,16,16,16,17,17,17,17,18,18,18,19,19,19,20,20,20,21,21,21,22,22,22,23,23,23,24,24,25,25,25,26,26,27,27,28,28,28,29,29,30,30,31,31,32,32,33,33,34,35,35,36,36,37,37,38,39,39,40,41,41,42,43,43,44,45,46,46,47,48,49,49,50,51,52,53,54,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,71,72,73,74,75,77,78,79,80,82,83,84,86,87,89,90,92,93,95,96,98,99,101,103,104,106,108,110,111,113,115,117,119,121,123,125,127,129,131,133,135,138,140,142,144,147,149,152,154,157,159,162,164,167,170,173,175,178,181,184,187,190,193,197,200,203,206,210,213,217,220,224,228,231,235,239,243,247,251,255 };

WORD **gaintab0;
WORD **gaintab1;
WORD **gaintab2;

LONG gaintab_new(const struct PRESET93_v1 *preset)
{
  UBYTE used[256];
  BYTE s[8];
  LONG bytes, count, n;
  WORD i, j, k;
  for (i = 0; i < 256; ++i)
  {
    used[i] = 0;
  }
  for (i = 0; i < 256; ++i)
  {
    used[dbtorms[i]] = 1;
  }
  count = 0;
  for (i = 0; i < 256; ++i)
  {
    count += used[i];
  }
  count *= 2;
  for (i = 0; i < 256; ++i)
  {
    used[i] = 0;
  }
  for (i = 0; i < 9; ++i)
  {
    for (j = 0; j < 9; ++j)
    {
      for (k = 0; k < 9; ++k)
      {
        used[preset->db[i][j][k]] = 1;
      }
    }
  }
  for (i = 0; i < 256; ++i)
  {
    count += used[i];
  }
  bytes = 3 * 256 * sizeof(WORD *) + count * 256 * sizeof(WORD);
  n = bytes;
  s[7] = '\n';
  s[6] = '0' + (n % 10); n /= 10;
  s[5] = '0' + (n % 10); n /= 10;
  s[4] = '0' + (n % 10); n /= 10;
  s[3] = '0' + (n % 10); n /= 10;
  s[2] = '0' + (n % 10); n /= 10;
  s[1] = '0' + (n % 10); n /= 10;
  s[0] = '0' + (n % 10);
#ifdef AMIGA
  Write(Output(), s, 8);
  if ((gaintab0 = (WORD **) AllocMem(bytes, MEMF_CLEAR)))
#else
  fwrite(s, 8, 1, stderr);
  if ((gaintab0 = (WORD **) calloc(1, bytes)))
#endif
  {
    LONG last;
    WORD lasti;
    WORD *gt;
    LONG peak0 = preset->gain[0];
    LONG peak1 = preset->gain[1];
    LONG peak2 = preset->gain[2];
    gaintab1 = gaintab0 + 256;
    gaintab2 = gaintab1 + 256;
    gt = (WORD *) (gaintab2 + 256);
    last = -1;
    for (i = 0; i < 256; ++i)
    {
      LONG g = dbtorms[i];
      if (g == last)
      {
        gaintab0[i] = gaintab0[i - 1];
      }
      else
      {
        LONG g0 = (peak0 * g + 0x7F) >> 8;
        for (j = -128; j < 128; ++j)
        {
          gt[(UBYTE) j] = (g0 * j + 0x7F) >> 8;
        }
        gaintab0[i] = gt;
        gt += 256;
      }
      last = g;
    }
    last = -1;
    for (i = 0; i < 256; ++i)
    {
      LONG g = dbtorms[i];
      if (g == last)
      {
        gaintab1[i] = gaintab1[i - 1];
      }
      else
      {
        LONG g1 = (peak1 * g + 0x7F) >> 8;
        for (j = -128; j < 128; ++j)
        {
          gt[(UBYTE) j] = (g1 * j + 0x7F) >> 8;
        }
        gaintab1[i] = gt;
        gt += 256;
      }
      last = g;
    }
    last = -1;
    lasti = -1;
    for (i = 0; i < 256; ++i)
    {
      if (used[i])
      {
        LONG g = dbtorms[i];
        if (g == last)
        {
          gaintab2[i] = gaintab2[lasti];
        }
        else
        {
          LONG g2 = (peak2 * g + 0x7F) >> 8;
          for (j = -128; j < 128; ++j)
          {
            gt[(UBYTE) j] = (g2 * j + 0x7F) >> 8;
          }
          gaintab2[i] = gt;
          gt += 256;
        }
        last = g;
        lasti = i;
      }
    }
    return bytes;
  }
  else
  {
    return 0;
  }
}

void gaintab_free(LONG bytes)
{
  if (bytes && gaintab0)
  {
#ifdef AMIGA
    FreeMem(gaintab0, bytes);
#else
    free(gaintab0);
#endif
    gaintab0 = 0;
    gaintab1 = 0;
    gaintab2 = 0;
  }
}
