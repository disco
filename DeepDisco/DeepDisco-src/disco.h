#ifndef DISCO_disco_h
#define DISCO_disco_h

#include "preset.h"

#ifdef DISCO_DEBUG
typedef LONG SAMPLE;
#else
typedef WORD SAMPLE;
#endif

#define COUNT 64

struct DISCO2
{
  SAMPLE accum;
  SAMPLE octave[9];
  WORD *gain[9];
  UBYTE ix;
};

struct DISCO1
{
  SAMPLE accum;
  SAMPLE octave[9];
  WORD *gain[9];
  struct DISCO2 noise[9];
  WORD *correlate[9][9];
  UBYTE uncorrelated[9];
  UBYTE ox[9];
  UBYTE ix;
};

struct DISCO0
{
  SAMPLE accum;
  SAMPLE octave[9];
  WORD *gain[9];
  struct DISCO1 noise[9];
  WORD *correlate[9][9];
  UBYTE uncorrelated[9];
  UBYTE ox[9];
  UBYTE ix;
};

LONG DISCO_init(struct DISCO0 *d, struct PRESET93_v1 *preset, ULONG seed);
void DISCO_deinit(LONG bytes);
void DISCO_bytes(struct DISCO0 *d, BYTE *out, LONG length
#ifdef DISCO_DEBUG
, ULONG *histogram, ULONG *overflow, ULONG pass
#endif
);

#endif
