#include "random.h"

#ifdef DISCO_RANDOM_MUSL

const RANDOM rndmul = 6364136223846793005ULL; /*5851 f42d 4c95 7f2d*/
const RANDOM rndadd = 1ULL;

ULONG get(RANDOM seed)
{
  /*
        s3 s2 s1 s0
        m3 m2 m1 m0
              s0*m0
           s0*m1
           s1*m0
        s0*m2
        s1*m1
        s2*m0
     s0*m3
     s1*m2
     s2*m1
     s3*m0
     // 10 mulu.w = 700 cycles, plus all the rest
  */
  return (ULONG) (seed >> 32);
}

RANDOM rnd(RANDOM seed)
{
  return seed * rndmul + rndadd;
}

#endif

#ifdef DISCO_RANDOM_BAD

const RANDOM rndmul = 196314165UL;
const RANDOM rndadd = 907633515UL;

ULONG get(RANDOM seed)
{
  return seed;
}

RANDOM rnd(RANDOM seed)
{
  return seed * rndmul + rndadd;
  /*
    D0 := seed    //  ?
    movem.l D1D2D3,-(SP)
                  // 32  // 8+8*3
    move.l #rndmul
              ,D3 // 12
    move.l D3,D1  //  4
    mulu.w D0,D1  // 70  lo * lo
    swap D0       //  4
    mulu.w D0,D2  // 70  hi * lo
    swap D2       //  4
    moveq.w 0,D2  //  4
    swap D0       //  4
    swap D3       //  4
    mulu.w D0,D3  // 70  lo * hi
    swap D3       //  4
    add.w D2,D3   //  4
    swap D3       //  4
    moveq.w 0,D3  //  4
    add.l D3,D0   //  8
    move.l D3,D0  //  4
    movem.l (SP)+,D1D2D3
                  // 36   // 12+8*3
    TOTAL           342
  */
}

#endif

#ifdef DISCO_RANDOM_LFSR

const RANDOM rndxor = 0x80200003L;

ULONG get(RANDOM seed)
{
  return seed;
}

/*
x^32 + x^22 + x^2 + x^1 + 1
x^32 + x^22 + x^21 + x^20 + x^18 + x^17 + x^15 + x^13 + x^12 + x^10 + x^8 + x^6 + x^4 + x^1 + 1
x^32 + x^23 + x^17 + x^16 + x^14 + x^10 + x^8 + x^7 + x^6 + x^5 + x^3 + 1
x^32 + x^26 + x^23 + x^22 + x^16 + x^12 + x^11 + x^10 + x^8 + x^7 + x^5 + x^4 + x^2 + x^1 + 1
x^32 + x^27 + x^26 + x^25 + x^24 + x^23 + x^22 + x^17 + x^13 + x^11 + x^10+x^9+x^8+x^7+x^2+x^1 + 1
x^32 + x^28 + x^19 + x^18 + x^16 + x^14 + x^11 + x^10 + x^9 + x^6 + x^5 + x^1 + 1
*/

RANDOM rnd(RANDOM seed)
{
  /*
    D0 := seed
    D1 := rndxor   // 12

    D0 += D0       // 8
    bcc skip       // 10/8  (taken/not taken)                                                                   |
    D0 ^= D1       // 8
skip:              // average 25 cycles

    // total ~812 cycles for 32 bits
  */
  ULONG b;
  for (b = 0; b < 32; ++b)
  {
    ULONG msb = ((LONG) seed) < 0;
    seed <<= 1;
    if (msb)
    {
      seed ^= rndxor;
    }
  }
  return seed;
}

#endif

#ifdef DISCO_RANDOM_XORSHIFT32

ULONG get(RANDOM seed)
{
  return seed;
}

RANDOM rnd(RANDOM x)
{
  /*
  move.l D0,D1   // 4
  asl.l #13,D1   // 8+2*13
  eor.l D1,D0    // 8
  move.l D0,D1   // 4
  lsr.l #17,D1   // 8+2*17
  eor.l D1,D0    // 8
  move.l D0,D1   // 4
  asl.l #5,D1    // 8+2*5
  eor.l D1,D0    // 8
  TOTAL          // 130
  */
  x ^= x << 13;
  x ^= x >> 17;
  x ^= x << 5;
  return x;
}

#endif

RANDOM prng = 0xDee9D15cUL;
U4 prng_data;
UBYTE prng_count = 3;
#ifdef DISCO_DEBUG
ULONG u8rands = 0;
#endif
UBYTE u8rand(void)
{
#ifdef DISCO_DEBUG
  u8rands++;
#endif
  if (++prng_count == 4)
  {
    prng_count = 0;
    prng_data.l = get(prng = rnd(prng));
  }
  return prng_data.b[prng_count];
}
