#ifndef DISCO_types_h
#define DISCO_types_h

#ifdef AMIGA

#include <exec/types.h>

#else

#include <stdint.h>
typedef int8_t BYTE;
typedef uint8_t UBYTE;
typedef int16_t WORD;
typedef uint16_t UWORD;
typedef int32_t LONG;
typedef uint32_t ULONG;

#endif

#endif
