#include <stdio.h>

#include "random.h"

int main(int argc, char **argv)
{
  RANDOM prng = 22222;
  while (1)
  {
    uint32_t r = get(prng = rnd(prng));
    fwrite(&r, sizeof(r), 1, stdout);
  }
  return 0;
  (void) argc;
  (void) argv;
}
