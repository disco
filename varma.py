# usage:
#   python3 varma.py x.wav y.wav x-input.wav y-output.wav
# single-channel x is transformed to FFT bins
# multi-channel x is mapped to RMS of each channel
# single-channel y is transformed to FFT bins
# x-input channels must match x channels
# y-output is single-channel

import math
import numpy
import scipy
from scipy.sparse.linalg import eigs
import soundfile
import sys

# constants

SR = 44100
FFT = 256
IOVERLAP = 4
IHOP = FFT // IOVERLAP
OOVERLAP = 4
OHOP = FFT // OOVERLAP
I = FFT // 2
O = FFT // 2
P = 0
Q = 1
M = P + Q + 1

t = numpy.array(range(FFT))
window = 0.5 - 0.5 * numpy.cos(2.0 * math.pi * t / FFT)

# analyze example

T = 0
X = []
Y = []
x = []
y = [ numpy.zeros((O)) for _ in range(Q) ]
for xblock, yblock in zip(soundfile.blocks(sys.argv[1], blocksize=FFT, overlap=FFT-IHOP),
                          soundfile.blocks(sys.argv[2], blocksize=FFT, overlap=FFT-IHOP)):
  if xblock.shape[0] is FFT and yblock.shape[0] is FFT and len(yblock.shape) is 1:
    if len(xblock.shape) > 1:
      I = xblock.shape[1]
      x0 = numpy.array([ numpy.sqrt(numpy.sum((numpy.ravel(xblock[:,i]) * window) ** 2)) for i in range(I) ])
    else:
      x0 = numpy.fft.fft((numpy.ravel(xblock)) * window)[1:I+1]
    if len(x) is 0:
      x = [ numpy.zeros((I)) for _ in range(P+1) ]
    x.insert(0, x0)
    x = x[:P+1]
    y0 = numpy.fft.fft((numpy.ravel(yblock)) * window)[1:O+1]
    T += 1
    X.append(numpy.concatenate([ numpy.ravel(xx) for xx in x ] + [ numpy.ravel(yy) for yy in y ]))
    Y.append(numpy.ravel(y0))
    y.insert(0, y0)
    y = y[:Q]

print (T)

X = numpy.reshape(numpy.vstack(X), (T, (P + 1) * I + Q * O))
Y = numpy.reshape(numpy.vstack(Y), (T, O))
beta = numpy.linalg.inv(X.conj().T @ X) @ X.conj().T @ Y
A = numpy.reshape(numpy.ravel(beta)[:(P + 1) * I * O], (P + 1, I, O))
A = [ numpy.reshape(A[i,:,:], (I, O)) for i in range(P+1) ]
B = numpy.reshape(numpy.ravel(beta)[(P + 1) * I * O:], (Q, O, O))
B = [ numpy.reshape(B[i,:,:], (O, O)) for i in range(Q) ]

# check feedback stability

def spectral_radius(m):
  return numpy.abs(eigs(m, k=1)[0])

r = numpy.amax([ spectral_radius(b) for b in B ])
print (r)

# process sound

with soundfile.SoundFile(sys.argv[4], 'w', SR, 1, subtype='FLOAT') as out:
  future = numpy.zeros((FFT))
  x = [ numpy.zeros((I)) for _ in range(P+1) ]
  y = [ numpy.zeros((O)) for _ in range(Q) ]
  for xblock in soundfile.blocks(sys.argv[3], blocksize=FFT, overlap=FFT-IHOP):
    if xblock.shape[0] is FFT:
      if len(xblock.shape) is 1:
        x0 = numpy.fft.fft((numpy.ravel(xblock)) * window)[1:I+1]
      else:
        x0 = numpy.array([ numpy.sqrt(numpy.sum((numpy.ravel(xblock[:,i]) * window) ** 2)) for i in range(I) ])
      x.insert(0, x0)
      x = x[:P+1]
      y0 = sum([ A[i].T @ x[i] for i in range(P + 1) ]) + sum([ B[i].T @ y[i] for i in range(Q) ])
      y.insert(0, y0)
      y = y[:Q]
      y0 = numpy.concatenate([numpy.zeros((1)), y0, numpy.zeros((FFT - O - 1))])
      y0 = y0 + numpy.flip(numpy.concatenate([y0, numpy.zeros((1))])).conj()[:FFT]
      future = numpy.concatenate([future[OHOP:], numpy.zeros((OHOP))])
      future += numpy.real(numpy.fft.ifft(y0)) * window
      out.write(numpy.reshape(future[:OHOP], (OHOP, 1)))
