// note: uses stack allocated arrays, may need `ulimit -s unlimited`

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sndfile.h>

// FIXME these defines must match rhythm-analysis.c

#define OCTAVES 11
#define OCTAVESIZE (1 << (OCTAVES - 1))

#define RHYTHMS 11
#define RHYTHMSIZE (1 << (RHYTHMS - 1))

#define OVERLAP 4

#define OHOP (OCTAVESIZE / OVERLAP)
#define RHOP (RHYTHMSIZE / OVERLAP)

// FIXME should match analyzed source audio
#define SR 44100

int main(int argc, char **argv)
{
  if (argc != 3)
  {
    fprintf
      ( stderr
      , "usage:\n"
        "  %s in.dat out.wav\n"
      , argv[0]
      );
    return 1;
  }
  const int normalize_control = 0;
  const int normalize_carrier = 1;

  float R[OCTAVES][RHYTHMS];
  memset(R, 0, sizeof(R));
  fprintf(stderr, "reading %s\n", argv[1]);
  FILE *ifile = fopen(argv[1], "rb");
  double sr = 0;
  for (int i = 0; i < OCTAVES; ++i)
  {
    for (int j = 0; j < RHYTHMS; ++j)
    {
      double r = 0;
      fscanf(ifile, "%lf ", &r);
      R[i][j] = r;
      if (j > 0)
        sr += r * r;
    }
  }
  sr /= OCTAVES * (RHYTHMS - 1);
  sr = sqrt(sr);
  if (normalize_control)
  for (int i = 0; i < OCTAVES; ++i)
  {
    for (int j = 1; j < RHYTHMS; ++j)
    {
      R[i][j] /= sr;
    }
  }
  fclose(ifile);

  fprintf(stderr, "writing %s\n", argv[2]);
  const int CHANNELS = 2;
  SF_INFO outfo =
    { 0
    , SR
    , CHANNELS
    , SF_FORMAT_WAV | SF_FORMAT_FLOAT
    , 0
    , 0
    };
  SNDFILE *ofile = sf_open(argv[2], SFM_WRITE, &outfo);
  if (! ofile) abort();

  float nbuffer[OCTAVESIZE][CHANNELS];
  memset(nbuffer, 0, sizeof(nbuffer));

  float obuffer[OCTAVESIZE][CHANNELS];
  memset(obuffer, 0, sizeof(obuffer));

  float owindow[OCTAVESIZE];
  memset(owindow, 0, sizeof(owindow));
  for (int i = 0; i < OCTAVESIZE; ++i)
  {
    owindow[i] = 0.5 - 0.5 * cos(i * 6.283185307179586 / OCTAVESIZE);
  }

  float rwindow[RHYTHMSIZE];
  memset(rwindow, 0, sizeof(rwindow));
  for (int i = 0; i < RHYTHMSIZE; ++i)
  {
    rwindow[i] = 0.5 - 0.5 * cos(i * 6.283185307179586 / RHYTHMSIZE);
  }

  int eindex = 0;
  float ebuffer[RHYTHMSIZE][OCTAVES];
  memset(ebuffer, 0, sizeof(ebuffer));

  int rindex = 0;
  float rbuffer[RHYTHMSIZE][OCTAVES];
  memset(rbuffer, 0, sizeof(rbuffer));

  int aindex = 0;
  float abuffer[OHOP][CHANNELS];
  memset(abuffer, 0, sizeof(abuffer));

  // FIXME 10mins should be enough for testing?
  for (int duration = 0; duration < 10 * 60 * SR; ++duration)
  {
    // generate noise
    for (int channel = 0; channel < CHANNELS; ++channel)
    {
      abuffer[aindex][channel] = rand() / (double) RAND_MAX - 0.5;
    }
    aindex++;
    if (aindex == OHOP)
    {
      aindex = 0;
      for (int i = 0; i < OCTAVESIZE - OHOP; ++i)
      {
        for (int channel = 0; channel < CHANNELS; ++channel)
        {
          nbuffer[i][channel] = nbuffer[i + OHOP][channel];
        }
      }
      for (int i = OCTAVESIZE - OHOP; i < OCTAVESIZE; ++i)
      {
        for (int channel = 0; channel < CHANNELS; ++channel)
        {
          nbuffer[i][channel] = abuffer[i - (OCTAVESIZE - OHOP)][channel];
        }
      }

      // window
      float haar[2][OCTAVESIZE][CHANNELS];
      memset(haar, 0, sizeof(haar));
      int src = 0;
      int dst = 1;
      for (int i = 0; i < OCTAVESIZE; ++i)
      {
        for (int channel = 0; channel < CHANNELS; ++channel)
        {
          haar[src][i][channel] = nbuffer[i][channel] * owindow[i];
        }
      }

      // compute Haar wavelet transform
      for (int length = OCTAVESIZE >> 1; length > 0; length >>= 1)
      {
        for (int i = 0; i < length; ++i)
        {
          for (int channel = 0; channel < CHANNELS; ++channel)
          {
            float a = haar[src][2 * i + 0][channel];
            float b = haar[src][2 * i + 1][channel];
            float s = (a + b) / 2;
            float d = (a - b) / 2;
            haar[dst][         i][channel] = s;
            haar[dst][length + i][channel] = d;
          }
        }
        for (int i = 0; i < OCTAVESIZE; ++i)
        {
          for (int channel = 0; channel < CHANNELS; ++channel)
          {
            haar[src][i][channel] = haar[dst][i][channel];
          }
        }
      }

      // compute energy per octave
      double E[OCTAVES];
      memset(E, 0, sizeof(E));
      for (int channel = 0; channel < CHANNELS; ++channel)
      {
        E[0] += fabsf(haar[src][0][channel]); // DC
      }
      for ( int octave = 1, length = 1
          ; length <= OCTAVESIZE >> 1
          ; octave += 1, length <<= 1
          )
      {
        double rms = 0;
        for (int channel = 0; channel < CHANNELS; ++channel)
        {
          for (int i = 0; i < length; ++i)
          {
            double a = haar[src][length + i][channel];
            rms += a * a;
          }
        }
        rms /= length * CHANNELS;
        rms = sqrt(fmax(rms, 0));
        E[octave] = rms;
      }

      // copy to ebuffer
      for (int octave = 0; octave < OCTAVES; ++octave)
      {
        ebuffer[eindex][octave] = E[octave];
      }
      eindex++;

      if (eindex == RHYTHMSIZE)
      {
        for (int octave = 0; octave < OCTAVES; ++octave)
        {

          float rhaar[2][RHYTHMSIZE];
          memset(rhaar, 0, sizeof(rhaar));

          // window
          for (int i = 0; i < RHYTHMSIZE; ++i)
          {
            rhaar[src][i] = ebuffer[i][octave] * 1;//rwindow[i];
          }

          // compute Haar wavelet transform
          for (int length = RHYTHMSIZE >> 1; length > 0; length >>= 1)
          {
            for (int i = 0; i < length; ++i)
            {
                float a = rhaar[src][2 * i + 0];
                float b = rhaar[src][2 * i + 1];
                float s = (a + b) / 2;
                float d = (a - b) / 2;
                rhaar[dst][         i] = s;
                rhaar[dst][length + i] = d;
            }
            for (int i = 0; i < RHYTHMSIZE; ++i)
            {
                rhaar[src][i] = rhaar[dst][i];
            }
          }

          // normalize carrier
          if (normalize_carrier)
          {
            rhaar[src][0] = 1;
            for ( int rhythm = 1, length = 1
                ; length <= RHYTHMSIZE >> 1
                ; rhythm += 1, length <<= 1
                )
            {
              double rms = 0;
              for (int i = 0; i < length; ++i)
              {
                double d = rhaar[src][length + i];
                rms += d * d;
              }
              rms /= length;
              rms = sqrt(rms);
              for (int i = 0; i < length; ++i)
              {
                rhaar[src][length + i] /= rms;
              }
            }
          }

          // amplify by control data
          rhaar[src][0] *= R[octave][0];
          for ( int rhythm = 1, length = 1
              ; length <= RHYTHMSIZE >> 1
              ; rhythm += 1, length <<= 1
              )
          {
            for (int i = 0; i < length; ++i)
            {
              rhaar[src][length + i] *= R[octave][rhythm];
            }
          }

          // compute inverse Haar wavelet transform
          for (int length = 1; length <= RHYTHMSIZE >> 1; length <<= 1)
          {
            for (int i = 0; i < RHYTHMSIZE; ++i)
            {
                rhaar[dst][i] = rhaar[src][i];
            }
            for (int i = 0; i < length; ++i)
            {
                float s = rhaar[dst][         i];
                float d = rhaar[dst][length + i];
                float a = s + d;
                float b = s - d;
                rhaar[src][2 * i + 0] = a;
                rhaar[src][2 * i + 1] = b;
            }
          }
      
          // window
          for (int i = 0; i < RHYTHMSIZE; ++i)
          {
              rhaar[src][i] *= rwindow[i];
          }
      
          // overlap-add
          for (int i = 0; i < RHYTHMSIZE; ++i)
          {
              rbuffer[i][octave]
                = i + RHOP < RHYTHMSIZE
                ? rbuffer[i + RHOP][octave]
                : 0;
          }
          for (int i = 0; i < RHYTHMSIZE; ++i)
          {
              rbuffer[i][octave] += rhaar[src][i];
          }
      
        } // for octave

        // shunt
        for (int i = 0; i < RHYTHMSIZE - RHOP; ++i)
        {
          for (int octave = 0; octave < OCTAVES; ++octave)
          {
            ebuffer[i][octave] = ebuffer[i + RHOP][octave];
          }
        }
        for (int i = RHYTHMSIZE - RHOP; i < RHYTHMSIZE; ++i)
        {
          for (int octave = 0; octave < OCTAVES; ++octave)
          {
            ebuffer[i][octave] = 0;
          }
        }
        
        eindex -= RHOP;
        rindex = 0;
      } // if eindex == RHYTHMSIZE

      // copy
      for (int octave = 0; octave < OCTAVES; ++octave)
      {
        E[octave] = rbuffer[rindex][octave];
      }
      rindex++;

      // amplify octaves
      for (int channel = 0; channel < CHANNELS; ++channel)
      {
        haar[src][0][channel] *= E[0]; // DC
      }
      for ( int octave = 1, length = 1
          ; length <= OCTAVESIZE >> 1
          ; octave += 1, length <<= 1
          )
      {
        for (int channel = 0; channel < CHANNELS; ++channel)
        {
          for (int i = 0; i < length; ++i)
          {
            haar[src][length + i][channel] *= E[octave];
          }
        }
      }

      // compute inverse Haar wavelet transform
      for (int length = 1; length <= OCTAVESIZE >> 1; length <<= 1)
      {
        for (int i = 0; i < OCTAVESIZE; ++i)
        {
          for (int channel = 0; channel < CHANNELS; ++channel)
          {
            haar[dst][i][channel] = haar[src][i][channel];
          }
        }
        for (int i = 0; i < length; ++i)
        {
          for (int channel = 0; channel < CHANNELS; ++channel)
          {
            float s = haar[dst][         i][channel];
            float d = haar[dst][length + i][channel];
            float a = s + d;
            float b = s - d;
            haar[src][2 * i + 0][channel] = a;
            haar[src][2 * i + 1][channel] = b;
          }
        }
      }
  
      // window
      for (int i = 0; i < OCTAVESIZE; ++i)
      {
        for (int channel = 0; channel < CHANNELS; ++channel)
        {
          haar[src][i][channel] *= owindow[i];
        }
      }
  
      // overlap-add
      for (int i = 0; i < OCTAVESIZE; ++i)
      {
        for (int channel = 0; channel < CHANNELS; ++channel)
        {
          obuffer[i][channel]
            = i + OHOP < OCTAVESIZE
            ? obuffer[i + OHOP][channel]
            : 0;
        }
      }
      for (int i = 0; i < OCTAVESIZE; ++i)
      {
        for (int channel = 0; channel < CHANNELS; ++channel)
        {
          obuffer[i][channel] += haar[src][i][channel];
        }
      }
  
      // output
      sf_writef_float(ofile, &obuffer[0][0], OHOP);

    } // if aindex == OHOP
  } // for duration

  sf_close(ofile);
  return 0;
}
