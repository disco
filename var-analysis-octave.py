import pandas as pd
import numpy as np
from statsmodels.tsa.api import VAR

input_data = pd.read_csv("input.csv", delim_whitespace=True, header=None, index_col=False, parse_dates=False)
r = input_data.values
d = pd.DataFrame(data=r, index=[i for i in range(r.shape[0])], columns=['f'+str(i) for i in range(r.shape[1])])
d.index = pd.to_datetime(d.index, unit='ms')
model = VAR(d)
result = model.fit(32, trend='c', ic=None)
np.savetxt("params.csv", result.params, delimiter=" ")
np.savetxt("sigmau.csv", result.sigma_u, delimiter=" ")
