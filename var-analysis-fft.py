import pandas as pd
import numpy as np
from statsmodels.tsa.api import VAR

input_data = pd.read_csv("input.csv", delim_whitespace=True, header=None, index_col=False, parse_dates=False)
r = input_data.values
c = 1j * np.hstack([np.zeros((r.shape[0],1)), r[:,129:], np.zeros((r.shape[0],1))]) + r[:,0:129]
d = pd.DataFrame(data=c, index=[i for i in range(c.shape[0])], columns=['f'+str(i) for i in range(c.shape[1])])
d.index = pd.to_datetime(d.index, unit='ms')
model = VAR(d)
result = model.fit(8, trend='c', ic=None)
np.savetxt("params.csv", result.params, delimiter=" ")
np.savetxt("sigmau.csv", result.sigma_u, delimiter=" ")
