// note: uses stack allocated arrays, may need `ulimit -s unlimited`

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sndfile.h>

// FIXME these defines must match markov-analysis.c

#define OCTAVES 11
#define OCTAVESIZE (1 << (OCTAVES - 1))

#define OVERLAP 16

#define OHOP (OCTAVESIZE / OVERLAP)

#define SOMSIZE 16

// FIXME should match analyzed source audio
#define SR 44100

int main(int argc, char **argv)
{
  if (argc != 3)
  {
    fprintf
      ( stderr
      , "usage:\n"
        "  %s in.dat out.wav\n"
      , argv[0]
      );
    return 1;
  }

  fprintf(stderr, "reading %s\n", argv[1]);
  FILE *dfile = fopen(argv[1], "rb");
  double imap[SOMSIZE][SOMSIZE][OCTAVES];
  int lasti = 0;
  int lastj = 0;
  int mins = 1.0 / 0.0;
  for (int i = 0; i < SOMSIZE; ++i)
  for (int j = 0; j < SOMSIZE; ++j)
  {
    double s = 0;
    for (int k = 0; k < OCTAVES; ++k)
    {
      double r = 0;
      fscanf(dfile, "%lf ", &r);
      imap[i][j][k] = r;
      s += r * r;
    }
    if (s < mins)
    {
      mins = s;
      lasti = i;
      lastj = j;
    }
  }
  double chain[SOMSIZE][SOMSIZE][SOMSIZE][SOMSIZE];
  for (int i = 0; i < SOMSIZE; ++i)
  for (int j = 0; j < SOMSIZE; ++j)
  for (int ii = 0; ii < SOMSIZE; ++ii)
  for (int jj = 0; jj < SOMSIZE; ++jj)
  {
    double r = 0;
    fscanf(dfile, "%lf ", &r);
    chain[i][j][ii][jj] = r;
  }
  fclose(dfile);

  const int CHANNELS = 2;
  fprintf(stderr, "writing %s\n", argv[2]);
  SF_INFO outfo =
    { 0
    , SR
    , CHANNELS
    , SF_FORMAT_WAV | SF_FORMAT_FLOAT
    , 0
    , 0
    };
  SNDFILE *ofile = sf_open(argv[2], SFM_WRITE, &outfo);
  if (! ofile) abort();

  float nbuffer[OCTAVESIZE][CHANNELS];
  memset(nbuffer, 0, sizeof(nbuffer));

  float obuffer[OCTAVESIZE][CHANNELS];
  memset(obuffer, 0, sizeof(obuffer));

  float owindow[OCTAVESIZE];
  memset(owindow, 0, sizeof(owindow));
  for (int i = 0; i < OCTAVESIZE; ++i)
  {
    owindow[i] = 0.5 - 0.5 * cos(i * 6.283185307179586 / OCTAVESIZE);
  }

  float abuffer[OHOP][CHANNELS];
  memset(abuffer, 0, sizeof(abuffer));

  // FIXME 10mins should be enough for testing?
  for (int duration = 0; duration < 10 * 60 * SR; duration += OHOP)
  {
    // generate noise
    for (int i = 0; i < OCTAVESIZE - OHOP; ++i)
    {
      for (int channel = 0; channel < CHANNELS; ++channel)
      {
        nbuffer[i][channel] = nbuffer[i + OHOP][channel];
      }
    }
    for (int i = OCTAVESIZE - OHOP; i < OCTAVESIZE; ++i)
    {
      for (int channel = 0; channel < CHANNELS; ++channel)
      {
        nbuffer[i][channel] = rand() / (double) RAND_MAX - 0.5;
      }
    }

    // window
    float haar[2][OCTAVESIZE][CHANNELS];
    memset(haar, 0, sizeof(haar));
    int src = 0;
    int dst = 1;
    for (int i = 0; i < OCTAVESIZE; ++i)
    {
      for (int channel = 0; channel < CHANNELS; ++channel)
      {
        haar[src][i][channel] = nbuffer[i][channel] * owindow[i];
      }
    }

    // compute Haar wavelet transform
    for (int length = OCTAVESIZE >> 1; length > 0; length >>= 1)
    {
      for (int i = 0; i < length; ++i)
      {
        for (int channel = 0; channel < CHANNELS; ++channel)
        {
          float a = haar[src][2 * i + 0][channel];
          float b = haar[src][2 * i + 1][channel];
          float s = (a + b) / 2;
          float d = (a - b) / 2;
          haar[dst][         i][channel] = s;
          haar[dst][length + i][channel] = d;
        }
      }
      for (int i = 0; i < OCTAVESIZE; ++i)
      {
        for (int channel = 0; channel < CHANNELS; ++channel)
        {
          haar[src][i][channel] = haar[dst][i][channel];
        }
      }
    }

    // Markov chain
    double E[OCTAVES];
    int nexti = lasti;
    int nextj = lastj;
    double t = rand() / (double) RAND_MAX;
    for (int i = 0; i < SOMSIZE; ++i)
    {
      for (int j = 0; j < SOMSIZE; ++j)
      {
        t -= chain[lasti][lastj][i][j];
        if (t <= 0)
        {
          nexti = i;
          nextj = j;
          break;
        }
      }
      if (t <= 0)
        break;
    }
    for (int k = 0; k < OCTAVES; ++k)
    {
      E[k] = imap[nexti][nextj][k];
    }
    lasti = nexti;
    lastj = nextj;

    // amplify octaves
    for (int channel = 0; channel < CHANNELS; ++channel)
    {
      haar[src][0][channel] *= E[0]; // DC
    }
    for ( int octave = 1, length = 1
        ; length <= OCTAVESIZE >> 1
        ; octave += 1, length <<= 1
        )
    {
      for (int channel = 0; channel < CHANNELS; ++channel)
      {
        for (int i = 0; i < length; ++i)
        {
          haar[src][length + i][channel] *= E[octave];
        }
      }
    }

    // compute inverse Haar wavelet transform
    for (int length = 1; length <= OCTAVESIZE >> 1; length <<= 1)
    {
      for (int i = 0; i < OCTAVESIZE; ++i)
      {
        for (int channel = 0; channel < CHANNELS; ++channel)
        {
          haar[dst][i][channel] = haar[src][i][channel];
        }
      }
      for (int i = 0; i < length; ++i)
      {
        for (int channel = 0; channel < CHANNELS; ++channel)
        {
          float s = haar[dst][         i][channel];
          float d = haar[dst][length + i][channel];
          float a = s + d;
          float b = s - d;
          haar[src][2 * i + 0][channel] = a;
          haar[src][2 * i + 1][channel] = b;
        }
      }
    }

    // window
    for (int i = 0; i < OCTAVESIZE; ++i)
    {
      for (int channel = 0; channel < CHANNELS; ++channel)
      {
        haar[src][i][channel] *= owindow[i];
      }
    }

    // overlap-add
    for (int i = 0; i < OCTAVESIZE; ++i)
    {
      for (int channel = 0; channel < CHANNELS; ++channel)
      {
        obuffer[i][channel] = i + OHOP < OCTAVESIZE ? obuffer[i + OHOP][channel] : 0;
      }
    }
    for (int i = 0; i < OCTAVESIZE; ++i)
    {
      for (int channel = 0; channel < CHANNELS; ++channel)
      {
        obuffer[i][channel] += haar[src][i][channel];
      }
    }

    // output
    sf_writef_float(ofile, &obuffer[0][0], OHOP);
  } // for duration

  sf_close(ofile);
  return 0;
}
