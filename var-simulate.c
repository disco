// note: uses stack allocated arrays, may need `ulimit -s unlimited`

#include <complex.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <gsl/gsl_linalg.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

#undef NOISE

#ifdef NOISE
bool positive_definite(const gsl_matrix_complex *A)
{
  const size_t N = A->size1;
  for (size_t j = 0; j < N; ++j)
  {
    gsl_complex z = gsl_matrix_complex_get(A, j, j);
    double ajj = GSL_REAL(z);
    if (ajj <= 0.0)
    {
      fprintf(stderr, "error: ajj <= 0 : j = %llu : ajj = %g\n", j, ajj);
    }
    if (j > 0)
    {
      gsl_vector_complex_const_view aj =
        gsl_matrix_complex_const_subrow(A, j, 0, j);
      gsl_blas_zdotc(&aj.vector, &aj.vector, &z);
      ajj -= GSL_REAL(z);
    }
    if (ajj <= 0.0)
    {
      return false;
    }
  }
  return true;
}

void make_positive_definite(gsl_matrix_complex *A)
{
  const size_t N = A->size1;
  const double l = 0.9;
  double f = 1;
  while (! positive_definite(A))
  {
    f *= l;
    fprintf(stderr, ".");
    for (size_t i = 0; i < N; ++i)
    {
      for (size_t j = 0; j < N; ++j)
      {
        gsl_complex z = gsl_matrix_complex_get(A, i, j);
        if (i == j)
        {
          GSL_SET_COMPLEX(&z, l * GSL_REAL(z) + (1.0 - l), l * GSL_IMAG(z));
        }
        else
        {
          GSL_SET_COMPLEX(&z, l * GSL_REAL(z), l * GSL_IMAG(z));
        }
        gsl_matrix_complex_set(A, i, j, z);
      }
    }
  }
  fprintf(stderr, "\nshrunk by %g\n", f);
}
#endif

int main(int argc, char **argv)
{
  if (argc != 5)
  {
    fprintf
      ( stderr
      , "usage:\n"
        "  %s channels history params.csv sigmau.csv < input.csv > output.csv\n"
      , argv[0]
      );
    return 1;
  }
  const int OCTAVES = atoi(argv[1]);
  const int HISTORY = atoi(argv[2]);

  // read params
  fprintf(stderr, "reading %s\n", argv[3]);
  FILE *ifile = fopen(argv[3], "rb");
  double _Complex cA[OCTAVES * HISTORY + 1][OCTAVES];
  for (int i = 0; i < OCTAVES * HISTORY + 1; ++i)
  {
    for (int j = 0; j < OCTAVES; ++j)
    {
      double re = 0, im = 0;
      fscanf(ifile, " (%lf%lfj) ", &re, &im);
      cA[i][j] = re + I * im;
    }
  }
  fclose(ifile);

#ifdef NOISE
  // read sigma_u
  fprintf(stderr, "reading %s\n", argv[4]);
  ifile = fopen(argv[4], "rb");
  double _Complex Ecov[OCTAVES][OCTAVES];
  for (int i = 0; i < OCTAVES; ++i)
  {
    for (int j = 0; j < OCTAVES; ++j)
    {
      double re = 0, im = 0;
      fscanf(ifile, " (%lf%lfj) ", &re, &im);
      Ecov[i][j] = re + (i == j ? 0 : I * im);
      if (i == j)
      {
        fprintf(stderr, "%g ", re);
        Ecov[i][j] = fabs(re);
      }
    }
  }
  fprintf(stderr, "\n");
  fclose(ifile);
#endif

#ifdef NOISE
  // compute sigma and decompose matrix
  double sigma[OCTAVES];
  for (int i = 0; i < OCTAVES; ++i)
  {
    sigma[i] = sqrt(creal(Ecov[i][i]));
    fprintf(stderr, "%g ", sigma[i]);
  }
  fprintf(stderr, "\n");
  gsl_matrix_complex_view cov =
    gsl_matrix_complex_view_array(&Ecov[0][0], OCTAVES, OCTAVES);
  make_positive_definite(&cov.matrix);
  gsl_linalg_complex_cholesky_decomp(&cov.matrix);
  gsl_rng_env_setup();
  gsl_rng *PRNG = gsl_rng_alloc(gsl_rng_default);
#endif

  // buffer
  double _Complex Y[HISTORY][OCTAVES];
  memset(Y, 0, sizeof(Y));

  while (true)
  {
    // input
    double X[OCTAVES][2];
    for (int i = 0; i < OCTAVES; ++i)
    {
      fscanf(stdin, "%lfe ", &X[i][0]);
    }
    X[0][1] = 0;
    for (int i = 1; i < OCTAVES - 1; ++i)
    {
      fscanf(stdin, "%lf ", &X[i][1]);
    }
    X[OCTAVES - 1][1] = 0;
    if (feof(stdin))
    {
      break;
    }

#ifdef NOISE
    // generate correlated noise source
    double _Complex noise[OCTAVES];
    // generate independent Gaussian vectors
    for (int k = 0; k < OCTAVES; ++k)
    {
      noise[k] = gsl_ran_gaussian(PRNG, sigma[k])
           + I * gsl_ran_gaussian(PRNG, sigma[k]);
    }
    gsl_vector_complex_view x =
      gsl_vector_complex_view_array(noise, OCTAVES);
    // correlate them to the correct covariance matrix
    gsl_blas_ztrmv
      (CblasLower, CblasNoTrans, CblasNonUnit, &cov.matrix, &x.vector);
#if 0 // impulse response only
    int target = (duration / 0x100) % OCTAVES;
    for (int k = 0; k < OCTAVES; ++k)
    {
      noise[k] = (duration & 0x7F) == 0 && k == target;
    }
#endif
#endif

    // step vector autoregression
    double _Complex Yn[OCTAVES];
    for (int octave = 0; octave < OCTAVES; ++octave)
    {
      int k = 0;
      Yn[octave] = 0;
#if 0 // constant trend
      Yn[octave] += cA[k++][octave];
#else
      k++;
#endif
#ifdef NOISE
      Yn[octave] += noise[octave];
#else
      Yn[octave] += X[octave][0] + I * X[octave][1];
#endif
      for (int p = 0; p < HISTORY; ++p)
      {
        for (int o = 0; o < OCTAVES; ++o)
        {
          Yn[octave] += cA[k++][octave] * Y[p][o];
        }
      }
    }
    for (int octave = 0; octave < OCTAVES; ++octave)
    {
      for (int p = HISTORY - 1; p > 0; --p)
      {
        Y[p][octave] = Y[p - 1][octave];
      }
      Y[0][octave] = Yn[octave];
    }

#if 0
    // normalize
    // FIXME normalize is the "wrong" thing to do, but makes it stable
    double s = 0;
    for (int i = 0; i < HISTORY; ++i)
    {
      for (int j = 0; j < OCTAVES; ++j)
      {
        s += Y[i][j] * Y[i][j];
      }
    }
    s /= HISTORY;
    s /= OCTAVES;
    s = sqrt(s);
    s = 1/s;
    if (isnan(s) || isinf(s)) s = 0;
    for (int i = 0; i < HISTORY; ++i)
    {
      for (int j = 0; j < OCTAVES; ++j)
      {
        Y[i][j] *= s;
      }
    }
#endif

    // output
    for (int i = 0; i < OCTAVES; ++i)
    {
      fprintf(stdout, "%.16e ", creal(Y[0][i]));
    }
    for (int i = 1; i < OCTAVES - 1; ++i)
    {
      fprintf(stdout, "%.16e ", cimag(Y[0][i]));
    }
    fprintf(stdout, "\n");
  } // for duration

  return 0;
}
