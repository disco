#!/bin/bash
set -e
rm -i input.csv || true
./octave-analysis "$1" > input.csv
rm -i params.csv || true
rm -i sigmau.csv || true
python3 ./var-analysis-octave.py
rm input.csv
./var-synthesis params.csv sigmau.csv "$1-u.wav"
rm params.csv sigmau.csv
./normalize "$1-u.wav" "$1-n.wav"
