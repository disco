#include <stdio.h>
#include <stdlib.h>

#include "haar3d.h"
#include "haar2d.h"

#define SIZE 4

float input[7][SIZE][7];
float tile[1 << SIZE][1 << SIZE][1 << SIZE];
float control[1 << SIZE][1 << SIZE][1 << SIZE];
float temporary[1 << SIZE][1 << SIZE][1 << SIZE];
float gain[1 << SIZE][1 << SIZE][1 << SIZE][SIZE][7];

float slice[2][1 << SIZE][1 << (SIZE << 1)][1 << (SIZE << 1)];
unsigned char pgm[1 << (SIZE << 1)][1 << (SIZE << 1)];

int main(int argc, char **argv)
{
  int seed = 1;
  if (argc > 1)
  {
    seed = atoi(argv[1]);
  }
  srand(seed);
  for (int direction = 0; direction < 7; ++direction)
  {
    for (int size = 0; size < SIZE; ++size)
    {
      for (int direction2 = 0; direction2 < 7; ++direction2)
      {
        float x = rand() / (float) RAND_MAX * 2 - 1;
        float g = (x * x * x * x * x * x * x) * 2 / SIZE;
        input[direction][size][direction2] = g;
      }
    }
  }
  for (int size = 0; size < SIZE; ++size)
  {
    for (int direction = 0; direction < 7; ++direction)
    {
      noise3d(SIZE, &tile[0][0][0]);
      tile[0][0][0] = 0;
      amplify3d(SIZE, &tile[0][0][0], &input[direction][0][0]);
      ihaar3d(SIZE, &control[0][0][0], &tile[0][0][0], &temporary[0][0][0]);
      for (int z = 0; z < 1 << SIZE; ++z)
      {
        for (int y = 0; y < 1 << SIZE; ++y)
        {
          for (int x = 0; x < 1 << SIZE; ++x)
          {
            gain[z][y][x][size][direction] = control[z][y][x];
          }
        }
      }
    }
  }
  int play = 0;
  int record = 1;
  int recorded = 0;
  for (int z = 0; z < 1 << SIZE; ++z)
  {
    for (int y = 0; y < 1 << SIZE; ++y)
    {
      for (int x = 0; x < 1 << SIZE; ++x)
      {
        noise3d(SIZE, &tile[0][0][0]);
        amplify3d(SIZE, &tile[0][0][0], &gain[z][y][x][0][0]);
        tile[0][0][0] = 0.5;
        ihaar3d(SIZE, &control[0][0][0], &tile[0][0][0], &temporary[0][0][0]);
        blit3d(SIZE << 1, SIZE, &slice[record][0][y << SIZE][x << SIZE], &control[0][0][0]);
      }
      if (recorded)
      {
        int frame = y;
        floatpgm(SIZE << 1, &pgm[0][0], &slice[play][frame][0][0]);
        printf("P5\n%d %d\n255\n", 1 << (SIZE << 1), 1 << (SIZE << 1));
        fwrite(pgm, sizeof(pgm), 1, stdout);
      }
    }
    int tmp = play;
    play = record;
    record = tmp;
    recorded = 1;
  }
  for (int frame = 0; frame < 1 << SIZE; ++frame)
  {
    floatpgm(SIZE << 1, &pgm[0][0], &slice[play][frame][0][0]);
    printf("P5\n%d %d\n255\n", 1 << (SIZE << 1), 1 << (SIZE << 1));
    fwrite(pgm, sizeof(pgm), 1, stdout);
  }
  return 0;
}
