#include <SDL2/SDL.h>
#include <SDL2/SDL_audio.h>
#include <SDL2/SDL_opengl.h>

#ifdef __EMSCRIPTEN__
#include <emscripten.h>
#endif

#include <cassert>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <algorithm>

#define SIZE 10

float input[SIZE][7];

extern "C"
void set(int size, int dir, double value)
{
  assert(0 <= size);
  assert(size < SIZE);
  assert(0 <= dir);
  assert(dir < 7);
  input[size][dir] = value;
}

#define CHANNELS 8
int spread = 8;
int audio_x[CHANNELS], audio_y[CHANNELS];
int audio_x0[CHANNELS+1], audio_x1[CHANNELS+1], audio_xy[CHANNELS], audio_xy_dir[CHANNELS];
int audio_y0[CHANNELS+1], audio_y1[CHANNELS+1], audio_yx[CHANNELS], audio_yx_dir[CHANNELS];

float wav[1 << SIZE][CHANNELS][2];

float old_x = 0.5 * CHANNELS, old_y = 0.5 * CHANNELS;
void audio(void *userdata, Uint8 *stream, int len)
{
  (void) userdata;
  float *b = (float *) stream;
  int m = len / sizeof(float) / 2;
  int k = 0;
  for (int i = 0; i < m; ++i)
  {
    float x = 0, y = 0;
    for (int j = 0; j < CHANNELS; ++j)
    {
      float u = wav[audio_x[j]][j][0];
      float v = wav[audio_y[j]][j][1];
      if (j & 1)
      {
        std::swap(u, v);
      }
      x += u;
      y += v;
      ++audio_x[j];
      if (audio_x[j] > audio_x1[j])
      {
        audio_x0[j] = audio_x0[CHANNELS] + (rand() % (2 * spread + 1)) - spread;
        audio_x1[j] = audio_x1[CHANNELS] + (rand() % (2 * spread + 1)) - spread;
        audio_x0[j] = std::min(std::max(audio_x0[j], 0), (1 << SIZE) - 1);
        audio_x1[j] = std::min(std::max(audio_x1[j], 0), (1 << SIZE) - 1);
        if (audio_x0[j] > audio_x1[j]) std::swap(audio_x0[j], audio_x1[j]);
        audio_x[j] = audio_x0[j];
        audio_yx[j] += audio_yx_dir[j];
        if (audio_yx_dir[j] > 0 && audio_yx[j] >= audio_x1[j])
        {
          audio_yx_dir[j] = -audio_yx_dir[j];
        }
        else if (audio_yx_dir[j] < 0 && audio_yx[j] <= audio_x0[j])
        {
          audio_yx_dir[j] = -audio_yx_dir[j];
        }
      }
      ++audio_y[j];
      if (audio_y[j] > audio_y1[j])
      {
        audio_y0[j] = audio_y0[CHANNELS] + (rand() % (2 * spread + 1)) - spread;
        audio_y1[j] = audio_y1[CHANNELS] + (rand() % (2 * spread + 1)) - spread;
        audio_y0[j] = std::min(std::max(audio_y0[j], 0), (1 << SIZE) - 1);
        audio_y1[j] = std::min(std::max(audio_y1[j], 0), (1 << SIZE) - 1);
        if (audio_y0[j] > audio_y1[j]) std::swap(audio_y0[j], audio_y1[j]);
        audio_y[j] = audio_y0[j];
        audio_xy[j] += audio_xy_dir[j];
        if (audio_xy_dir[j] > 0 && audio_xy[j] >= audio_y1[j])
        {
          audio_xy_dir[j] = -audio_xy_dir[j];
        }
        else if (audio_xy_dir[j] < 0 && audio_xy[j] <= audio_y0[j])
        {
          audio_xy_dir[j] = -audio_xy_dir[j];
        }
      }
    }
    // high pass filter
    old_x = 0.99 * old_x + 0.01 * x;
    old_y = 0.99 * old_y + 0.01 * y;
    x -= old_x;
    y -= old_y;
    b[k++] = std::tanh(x / CHANNELS);
    b[k++] = std::tanh(y / CHANNELS);
  }
}

unsigned char pgm[1 << SIZE][1 << SIZE];

template <unsigned int n>
struct level
{
  float random[7];
  level<n - 1> next[2][2];
};

template<>
struct level<0>
{
};

level<SIZE> control;

template <unsigned int n>
void render(const level<n> &control, float value, int x, int y, int frame)
{
  int z = frame >> (n - 1);
  int k = ((z & 1) < 1) * 2 - 1;
  int j = ((y & 1) < 1) * 2 - 1;
  int i = ((x & 1) < 1) * 2 - 1;
  float x0 = value;
  float x1 = control.random[0] * input[n - 1][0];
  float x2 = control.random[1] * input[n - 1][1];
  float x3 = control.random[2] * input[n - 1][2];
  float x4 = control.random[3] * input[n - 1][3];
  float x5 = control.random[4] * input[n - 1][4];
  float x6 = control.random[5] * input[n - 1][5];
  float x7 = control.random[6] * input[n - 1][6];
  float v[2][2][2];
  v[0][0][0] = ((x0 + i * x1) + j * (x2 + i * x3)) + k * ((x4 + i * x5) + j * (x6 + i * x7));
  v[0][0][1] = ((x0 + i * x1) + j * (x2 + i * x3)) - k * ((x4 + i * x5) + j * (x6 + i * x7));
  v[0][1][0] = ((x0 + i * x1) - j * (x2 + i * x3)) + k * ((x4 + i * x5) - j * (x6 + i * x7));
  v[0][1][1] = ((x0 + i * x1) - j * (x2 + i * x3)) - k * ((x4 + i * x5) - j * (x6 + i * x7));
  v[1][0][0] = ((x0 - i * x1) + j * (x2 - i * x3)) + k * ((x4 - i * x5) + j * (x6 - i * x7));
  v[1][0][1] = ((x0 - i * x1) + j * (x2 - i * x3)) - k * ((x4 - i * x5) + j * (x6 - i * x7));
  v[1][1][0] = ((x0 - i * x1) - j * (x2 - i * x3)) + k * ((x4 - i * x5) - j * (x6 - i * x7));
  v[1][1][1] = ((x0 - i * x1) - j * (x2 - i * x3)) - k * ((x4 - i * x5) - j * (x6 - i * x7));
  for (int dy = 0; dy < 2; ++dy)
  {
    for (int dx = 0; dx < 2; ++dx)
    {
      render(control.next[dy][dx], v[dx][dy][z & 1], (x << 1) + dx, (y << 1) + dy, frame);
    }
  }
}

template <>
void render<0>(const level<0> &control, float value, int x, int y, int frame)
{
  (void) control;
  (void) frame;
  for (int j = 0; j < CHANNELS; ++j)
  {
    if (y == audio_xy[j])
    {
      wav[x][j][0] = value;
    }
    if (x == audio_yx[j])
    {
      wav[y][j][1] = value;
    }
  }
  pgm[y][x] = std::fmin(std::fmax(255.0f * value, 0.0f), 255.0f);
}

void render(int frame)
{
  render(control, 0.5, 0, 0, frame);
}

template <unsigned int n>
void update(level<n> &control, int frame)
{
  if ((frame & ((1 << n) - 1)) == 0)
  {
    for (int i = 0; i < 7; ++i)
    {
      control.random[i] = rand() / (float) RAND_MAX * 2 - 1;
    }
  }
  for (int dy = 0; dy < 2; ++dy)
  {
    for (int dx = 0; dx < 2; ++dx)
    {
      update(control.next[dy][dx], frame);
    }
  }
}

template <>
void update<0>(level<0> &control, int frame)
{
  (void) control;
  (void) frame;
}

void update(int frame)
{
  update(control, frame);
}

void draw(void)
{
  glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, 1 << SIZE, 1 << SIZE, GL_LUMINANCE, GL_UNSIGNED_BYTE, &pgm[0][0]);
  glEnable(GL_TEXTURE_2D);
  glBegin(GL_TRIANGLE_STRIP);
  glColor3f(1, 1, 1); glTexCoord2f(0, 0); glVertex2f(-1, -1);
  glColor3f(1, 1, 1); glTexCoord2f(0, 1); glVertex2f(-1,  1);
  glColor3f(1, 1, 1); glTexCoord2f(1, 0); glVertex2f( 1, -1);
  glColor3f(1, 1, 1); glTexCoord2f(1, 1); glVertex2f( 1,  1);
  glEnd();
  glDisable(GL_TEXTURE_2D);
  glBegin(GL_LINES);
  for (int j = 0; j < CHANNELS; ++j)
  {
    float x0 = (audio_x0[j]) / (float)(1 << SIZE) * 2 - 1;
    float x1 = (audio_x1[j]) / (float)(1 << SIZE) * 2 - 1;
    float y  = (audio_xy[j]) / (float)(1 << SIZE) * 2 - 1;
    glColor3f(1, 0, 0); glVertex2f(x0, -y);
    glColor3f(1, 0, 0); glVertex2f(x1, -y);
    float y0 = (audio_y0[j]) / (float)(1 << SIZE) * 2 - 1;
    float y1 = (audio_y1[j]) / (float)(1 << SIZE) * 2 - 1;
    float x  = (audio_yx[j]) / (float)(1 << SIZE) * 2 - 1;
    glColor3f(1, 0, 0); glVertex2f(x, -y0);
    glColor3f(1, 0, 0); glVertex2f(x, -y1);
  }
  glEnd();
}

volatile int running = 1;
bool old_button = false;
void interact(void)
{
  SDL_Event event;
  while (SDL_PollEvent(&event) != 0)
  {
    switch (event.type)
    {
      case SDL_QUIT:
      {
        running = 0;
      }
    }
  }
  int x = -1, y = -1;
  int buttons = SDL_GetMouseState(&x, &y);
  bool new_button = buttons & SDL_BUTTON(SDL_BUTTON_LEFT);
  if (new_button)
  {
    if (old_button)
    {
      audio_x0[CHANNELS] = std::min(audio_x0[CHANNELS], x);
      audio_y0[CHANNELS] = std::min(audio_y0[CHANNELS], y);
      audio_x1[CHANNELS] = std::max(audio_x1[CHANNELS], x);
      audio_y1[CHANNELS] = std::max(audio_y1[CHANNELS], y);
    }
    else
    {
      audio_x0[CHANNELS] = x;
      audio_y0[CHANNELS] = y;
      audio_x1[CHANNELS] = x;
      audio_y1[CHANNELS] = y;
    }
  }
  old_button = new_button;
  audio_x0[CHANNELS] = std::min(std::max(audio_x0[CHANNELS], 0), (1 << SIZE) - 1);
  audio_x1[CHANNELS] = std::min(std::max(audio_x1[CHANNELS], 0), (1 << SIZE) - 1);
  audio_y0[CHANNELS] = std::min(std::max(audio_y0[CHANNELS], 0), (1 << SIZE) - 1);
  audio_y1[CHANNELS] = std::min(std::max(audio_y1[CHANNELS], 0), (1 << SIZE) - 1);
  if (audio_x0[CHANNELS] > audio_x1[CHANNELS]) std::swap(audio_x0[CHANNELS], audio_x1[CHANNELS]);
  if (audio_y0[CHANNELS] > audio_y1[CHANNELS]) std::swap(audio_y0[CHANNELS], audio_y1[CHANNELS]);
}

SDL_Window* window;
SDL_GLContext context;
int frame = 0;
time_t start, now;
void main1(void)
{
  if (frame == 0)
  {
    start = time(0);
  }
  interact();
  update(frame);
  render(frame);
  ++frame;
  frame &= (1 << SIZE) - 1;
  draw();
  SDL_GL_SwapWindow(window);
  if (frame == 0)
  {
    now = time(0);
    float fps = (1 << SIZE) / (float) (now - start);
    std::fprintf(stderr, "fps: %g\n", fps);
  }
}

extern
int main(int argc, char **argv)
{
  (void) argc;
  (void) argv;
  srand(time(0));

  SDL_Init(SDL_INIT_AUDIO | SDL_INIT_VIDEO);

  // initialize audio
  SDL_AudioSpec want, have;
  want.freq = 44100;
  want.format = AUDIO_F32;
  want.channels = 2;
  want.samples = 1024;
  want.callback = audio;
  SDL_AudioDeviceID dev = SDL_OpenAudioDevice(NULL, 0, &want, &have, SDL_AUDIO_ALLOW_FREQUENCY_CHANGE | SDL_AUDIO_ALLOW_SAMPLES_CHANGE);
  assert(have.format == AUDIO_F32);
  assert(have.channels == 2);
  audio_x0[CHANNELS] = 1 << (SIZE - 2);
  audio_x1[CHANNELS] = 3 << (SIZE - 2);
  audio_y0[CHANNELS] = 1 << (SIZE - 2);
  audio_y1[CHANNELS] = 3 << (SIZE - 2);
  for (int j = 0; j < CHANNELS; ++j)
  {
    audio_xy[j] = 2 << (SIZE - 2);
    audio_yx[j] = 2 << (SIZE - 2);
    audio_xy_dir[j] = 2 * (j & 1) - 1;
    audio_yx_dir[j] = 2 * (j & 1) - 1;
  }

  // initialize video
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);
  window = SDL_CreateWindow("disco/imager", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 1 << SIZE, 1 << SIZE, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
  if (! window)
  {
    fprintf(stderr, "error: couldn't create SDL2 window\n");
    return 1;
  }
  context = SDL_GL_CreateContext(window);
  if (! context)
  {
    fprintf(stderr, "error: couldn't create OpenGL context\n");
    return 1;
  }
  SDL_GL_SetSwapInterval(1);

  // create texture
  GLuint texture;
  glGenTextures(1, &texture);
  glBindTexture(GL_TEXTURE_2D, texture);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_LUMINANCE, 1 << SIZE, 1 << SIZE, 0, GL_LUMINANCE, GL_UNSIGNED_BYTE, 0);
  glDisable(GL_DEPTH_TEST);

  // start audio processing
  SDL_PauseAudioDevice(dev, 0);

  // main loop
#ifdef __EMSCRIPTEN__
  emscripten_set_main_loop(main1, 30, 0);
#else
  while (running)
  {
    main1();
  }
#endif

  // unreachable
  return 0;
}
