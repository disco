#pragma once

#include <math.h>
#include <stdlib.h>

// 3D Haar inverse wavelet transformation
void ihaar3d
  ( int stride                          // arrays are [1 << (stride * 3)]
  , float * restrict output             // output is overwritten
  , const float * restrict input        // input is preserved
  , float * restrict next               // temporary space is overwritten
  );

// adjust gain of different parts
void amplify3d
  ( int stride                          // transform is [1 << (stride * 3)]
  , float * restrict transform          // modified in place
  , const float * restrict gain         // gain for each octave/dir, [7 << stride]
  );

// generate noise
void noise3d
  ( int stride                          // output is [1 << (3 * stride)]
  , float * restrict output             // output is overwritten
  );

// blit a tile into a larger one
void blit3d
  ( int ostride                         // output is [1 << (3 * ostride)]
  , int istride                         // input is [1 << (3 * istride)]
  , float * restrict output             // output is overwritten
  , const float * restrict input        // input is preserved
  );
