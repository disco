#include <math.h>
#include <stdlib.h>

#include "haar2d.h"

// 2D Haar wavelet transformation
void haar2d
  ( int stride                          // arrays are [1 << (stride << 1)]
  , float * restrict output             // output is overwritten
  , float * restrict input              // input is overwritten
  , float * restrict next               // temporary space is overwritten
  )
{
  for (int size = stride; size >= 1; --size)
  {
    int d = 1 << (size - 1);
    for (int y = 0; y < 1 << size; y += 2)
    {
      for (int x = 0; x < 1 << size; x += 2)
      {
        float x0 = input[((y + 0) << stride) + (x + 0)];
        float x1 = input[((y + 0) << stride) + (x + 1)];
        float x2 = input[((y + 1) << stride) + (x + 0)];
        float x3 = input[((y + 1) << stride) + (x + 1)];
        float y0 = ((x0 + x1) + (x2 + x3)) / 4;
        float y1 = ((x0 + x1) - (x2 + x3)) / 4;
        float y2 = ((x0 - x1) + (x2 - x3)) / 4;
        float y3 = ((x0 - x1) - (x2 - x3)) / 4;
        next  [(((y >> 1) + 0) << stride) + ((x >> 1) + 0)] = y0;
        output[(((y >> 1) + 0) << stride) + ((x >> 1) + d)] = y1;
        output[(((y >> 1) + d) << stride) + ((x >> 1) + 0)] = y2;
        output[(((y >> 1) + d) << stride) + ((x >> 1) + d)] = y3;
      }
    }
    float * restrict tmp = input;
    input = next;
    next = tmp;
  }
  output[0] = input[0];
}

// 2D Haar inverse wavelet transformation
void ihaar2d
  ( int stride                          // arrays are [1 << (stride << 1)]
  , float * restrict output             // output is overwritten
  , const float * restrict input        // input is preserved
  , float * restrict next               // temporary space is overwritten
  )
{
  if (stride & 1)
  {
    float * restrict tmp = output;
    output = next;
    next = tmp;
  }
  output[0] = input[0];
  for (int size = 1; size <= stride; ++size)
  {
    float * restrict tmp = output;
    output = next;
    next = tmp;
    int d = 1 << (size - 1);
    for (int y = 0; y < 1 << size; y += 2)
    {
      for (int x = 0; x < 1 << size; x += 2)
      {
        float x0 = next [(((y >> 1) + 0) << stride) + ((x >> 1) + 0)];
        float x1 = input[(((y >> 1) + 0) << stride) + ((x >> 1) + d)];
        float x2 = input[(((y >> 1) + d) << stride) + ((x >> 1) + 0)];
        float x3 = input[(((y >> 1) + d) << stride) + ((x >> 1) + d)];
        float y0 = (x0 + x1) + (x2 + x3);
        float y1 = (x0 + x1) - (x2 + x3);
        float y2 = (x0 - x1) + (x2 - x3);
        float y3 = (x0 - x1) - (x2 - x3);
        output[((y + 0) << stride) + (x + 0)] = y0;
        output[((y + 0) << stride) + (x + 1)] = y1;
        output[((y + 1) << stride) + (x + 0)] = y2;
        output[((y + 1) << stride) + (x + 1)] = y3;
      }
    }
  }
}

// convert to gray8 with offset
void haar2dpgm
  ( int stride                          // arrays are [1 << (stride << 1)]
  , unsigned char * restrict output     // output is overwritten
  , const float * restrict input        // input is preserved
  )
{
  for (int y = 0; y < 1 << stride; ++y)
  {
    for (int x = 0; x < 1 << stride; ++x)
    {
      float x0 = input[(y << stride) + x];
      output[(y << stride) + x] = fminf(fmaxf(255 * (x0 + 0.5), 0), 255);
    }
  }
}

// convert to gray8
void floatpgm
  ( int stride                          // arrays are [1 << (stride << 1)]
  , unsigned char * restrict output     // output is overwritten
  , const float * restrict input        // input is preserved
  )
{
  for (int y = 0; y < 1 << stride; ++y)
  {
    for (int x = 0; x < 1 << stride; ++x)
    {
      float x0 = input[(y << stride) + x];
      output[(y << stride) + x] = fminf(fmaxf(255 * x0, 0), 255);
    }
  }
}

// convert from gray8
void pgmfloat
  ( int stride                          // arrays are [1 << (stride << 1)]
  , float * restrict output             // output is overwritten
  , const unsigned char * restrict input// input is preserved
  )
{
  for (int y = 0; y < 1 << stride; ++y)
  {
    for (int x = 0; x < 1 << stride; ++x)
    {
      unsigned char x0 = input[(y << stride) + x];
      output[(y << stride) + x] = x0 / 255.0f;
    }
  }
}

// interleaved rgbrgbrgb to planar rrrgggbbb
void deinterleave3
  ( int stride                          // arrays are [3 << (stride << 1)]
  , unsigned char * restrict output     // output is overwritten
  , const unsigned char * restrict input// input is preserved
  )
{
  for (int y = 0; y < 1 << stride; ++y)
  {
    for (int x = 0; x < 1 << stride; ++x)
    {
      for (int channel = 0; channel < 3; ++channel)
      {
        output[(((channel << stride) + y) << stride) + x]
          = input[((y << stride) + x) * 3 + channel];
      }
    }
  }
}

// planar rrrgggbbb to interleaved rgbrgbrgb
void interleave3
  ( int stride                          // arrays are [3 << (stride << 1)]
  , unsigned char * restrict output     // output is overwritten
  , const unsigned char * restrict input// input is preserved
  )
{
  for (int y = 0; y < 1 << stride; ++y)
  {
    for (int x = 0; x < 1 << stride; ++x)
    {
      for (int channel = 0; channel < 3; ++channel)
      {
        output[((y << stride) + x) * 3 + channel]
          = input[(((channel << stride) + y) << stride) + x];
      }
    }
  }
}

// adjust gain of different parts
void amplify
  ( int stride                          // transform is [1 << (stride << 1)]
  , float * restrict transform          // modified in place
  , const float * restrict gain         // gain for each octave/dir, [3 << stride]
  )
{
  for (int size = 1; size <= stride; ++size)
  {
    int d = 1 << (size - 1);
    for (int y = 0; y < 1 << size; y += 2)
    {
      for (int x = 0; x < 1 << size; x += 2)
      {
        int k1 = (((y >> 1) + 0) << stride) + ((x >> 1) + d);
        int k2 = (((y >> 1) + d) << stride) + ((x >> 1) + 0);
        int k3 = (((y >> 1) + d) << stride) + ((x >> 1) + d);
        transform[k1] *= gain[3 * (size - 1) + 0];
        transform[k2] *= gain[3 * (size - 1) + 1];
        transform[k3] *= gain[3 * (size - 1) + 2];
      }
    }
  }
}

// generate noise
void noise
  ( int stride                          // output is [1 << (stride << 1)]
  , float * restrict output             // output is overwritten
  )
{
  for (int y = 0; y < 1 << stride; ++y)
  {
    for (int x = 0; x < 1 << stride; ++x)
    {
      output[(y << stride) + x] = rand() / (float) RAND_MAX - 0.5;
    }
  }
}

// blit a tile into a larger one
void blit
  ( int ostride                         // output is [1 << (ostride << 1)]
  , int istride                         // input is [1 << (istride << 1)]
  , float * restrict output             // output is overwritten
  , const float * restrict input        // input is preserved
  )
{
  for (int y = 0; y < 1 << istride; ++y)
  {
    for (int x = 0; x < 1 << istride; ++x)
    {
      output[(y << ostride) + x] = input[(y << istride) + x];
    }
  }
}
