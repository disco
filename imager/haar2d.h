#pragma once

// 2D Haar wavelet transformation
void haar2d
  ( int stride                          // arrays are [1 << (stride << 1)]
  , float * restrict output             // output is overwritten
  , float * restrict input              // input is overwritten
  , float * restrict next               // temporary space is overwritten
  );

// 2D Haar inverse wavelet transformation
void ihaar2d
  ( int stride                          // arrays are [1 << (stride << 1)]
  , float * restrict output             // output is overwritten
  , const float * restrict input        // input is preserved
  , float * restrict next               // temporary space is overwritten
  );

// convert to gray8 with offset
void haar2dpgm
  ( int stride                          // arrays are [1 << (stride << 1)]
  , unsigned char * restrict output     // output is overwritten
  , const float * restrict input        // input is preserved
  );

// convert to gray8
void floatpgm
  ( int stride                          // arrays are [1 << (stride << 1)]
  , unsigned char * restrict output     // output is overwritten
  , const float * restrict input        // input is preserved
  );

// convert from gray8
void pgmfloat
  ( int stride                          // arrays are [1 << (stride << 1)]
  , float * restrict output             // output is overwritten
  , const unsigned char * restrict input// input is preserved
  );

// interleaved rgbrgbrgb to planar rrrgggbbb
void deinterleave3
  ( int stride                          // arrays are [3 << (stride << 1)]
  , unsigned char * restrict output     // output is overwritten
  , const unsigned char * restrict input// input is preserved
  );

// planar rrrgggbbb to interleaved rgbrgbrgb
void interleave3
  ( int stride                          // arrays are [3 << (stride << 1)]
  , unsigned char * restrict output     // output is overwritten
  , const unsigned char * restrict input// input is preserved
  );

// adjust gain of different parts
void amplify
  ( int stride                          // transform is [1 << (stride << 1)]
  , float * restrict transform          // modified in place
  , const float * restrict gain         // gain for each octave/dir, [3 << stride]
  );

// generate noise
void noise
  ( int stride                          // output is [1 << (stride << 1)]
  , float * restrict output             // output is overwritten
  );

// blit a tile into a larger one
void blit
  ( int ostride                         // output is [1 << (ostride << 1)]
  , int istride                         // input is [1 << (istride << 1)]
  , float * restrict output             // output is overwritten
  , const float * restrict input        // input is preserved
  );
