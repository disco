#include <stdio.h>
#include <stdlib.h>

#include "haar2d.h"

#define SIZE 6

float input[3][SIZE][3];
float tile[1 << SIZE][1 << SIZE];
float control[1 << SIZE][1 << SIZE];
float temporary[1 << SIZE][1 << SIZE];
float gain[1 << SIZE][1 << SIZE][SIZE][3];

float image[1 << (SIZE << 1)][1 << (SIZE << 1)];
unsigned char pgm[1 << (SIZE << 1)][1 << (SIZE << 1)];

int main(int argc, char **argv)
{
  int seed = 1;
  if (argc > 1)
  {
    seed = atoi(argv[1]);
  }
  srand(seed);
  for (int direction = 0; direction < 3; ++direction)
  {
    for (int size = 0; size < SIZE; ++size)
    {
      for (int direction2 = 0; direction2 < 3; ++direction2)
      {
        float x = rand() / (float) RAND_MAX * 2 - 1;
        float g = (x * x * x * x * x * x * x) * 2 / SIZE;
        input[direction][size][direction2] = g;
      }
    }
  }
  for (int size = 0; size < SIZE; ++size)
  {
    for (int direction = 0; direction < 3; ++direction)
    {
      noise(SIZE, &tile[0][0]);
      tile[0][0] = 0;
      amplify(SIZE, &tile[0][0], &input[direction][0][0]);
      ihaar2d(SIZE, &control[0][0], &tile[0][0], &temporary[0][0]);
      for (int y = 0; y < 1 << SIZE; ++y)
       {
        for (int x = 0; x < 1 << SIZE; ++x)
        {
          gain[y][x][size][direction] = control[y][x];
        }
      }
    }
  }
  for (int y = 0; y < 1 << SIZE; ++y)
  {
    for (int x = 0; x < 1 << SIZE; ++x)
    {
      noise(SIZE, &tile[0][0]);
      amplify(SIZE, &tile[0][0], &gain[y][x][0][0]);
      tile[0][0] = 0.5;
      ihaar2d(SIZE, &control[0][0], &tile[0][0], &temporary[0][0]);
      blit(SIZE << 1, SIZE, &image[y << SIZE][x << SIZE], &control[0][0]);
    }
  }
  floatpgm(SIZE << 1, &pgm[0][0], &image[0][0]);
  printf("P5\n%d %d\n255\n", 1 << (SIZE << 1), 1 << (SIZE << 1));
  fwrite(pgm, sizeof(pgm), 1, stdout);
  return 0;
}
