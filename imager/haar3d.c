#include <math.h>
#include <stdlib.h>

#include "haar3d.h"

// 3D Haar inverse wavelet transformation
void ihaar3d
  ( int stride                          // arrays are [1 << (stride * 3)]
  , float * restrict output             // output is overwritten
  , const float * restrict input        // input is preserved
  , float * restrict next               // temporary space is overwritten
  )
{
  if (stride & 1)
  {
    float * restrict tmp = output;
    output = next;
    next = tmp;
  }
  output[0] = input[0];
  for (int size = 1; size <= stride; ++size)
  {
    float * restrict tmp = output;
    output = next;
    next = tmp;
    int d = 1 << (size - 1);
    for (int z = 0; z < 1 << size; z += 2)
    for (int y = 0; y < 1 << size; y += 2)
    for (int x = 0; x < 1 << size; x += 2)
    {
        float x0 = next [(((((z >> 1) + 0) << stride) + ((y >> 1) + 0)) << stride) + ((x >> 1) + 0)];
        float x1 = input[(((((z >> 1) + 0) << stride) + ((y >> 1) + 0)) << stride) + ((x >> 1) + d)];
        float x2 = input[(((((z >> 1) + 0) << stride) + ((y >> 1) + d)) << stride) + ((x >> 1) + 0)];
        float x3 = input[(((((z >> 1) + 0) << stride) + ((y >> 1) + d)) << stride) + ((x >> 1) + d)];
        float x4 = input[(((((z >> 1) + d) << stride) + ((y >> 1) + 0)) << stride) + ((x >> 1) + 0)];
        float x5 = input[(((((z >> 1) + d) << stride) + ((y >> 1) + 0)) << stride) + ((x >> 1) + d)];
        float x6 = input[(((((z >> 1) + d) << stride) + ((y >> 1) + d)) << stride) + ((x >> 1) + 0)];
        float x7 = input[(((((z >> 1) + d) << stride) + ((y >> 1) + d)) << stride) + ((x >> 1) + d)];
        float y0 = ((x0 + x1) + (x2 + x3)) + ((x4 + x5) + (x6 + x7));
        float y1 = ((x0 + x1) + (x2 + x3)) - ((x4 + x5) + (x6 + x7));
        float y2 = ((x0 + x1) - (x2 + x3)) + ((x4 + x5) - (x6 + x7));
        float y3 = ((x0 + x1) - (x2 + x3)) - ((x4 + x5) - (x6 + x7));
        float y4 = ((x0 - x1) + (x2 - x3)) + ((x4 - x5) + (x6 - x7));
        float y5 = ((x0 - x1) + (x2 - x3)) - ((x4 - x5) + (x6 - x7));
        float y6 = ((x0 - x1) - (x2 - x3)) + ((x4 - x5) - (x6 - x7));
        float y7 = ((x0 - x1) - (x2 - x3)) - ((x4 - x5) - (x6 - x7));
        output[((((z + 0) << stride) + (y + 0)) << stride) + (x + 0)] = y0;
        output[((((z + 0) << stride) + (y + 0)) << stride) + (x + 1)] = y1;
        output[((((z + 0) << stride) + (y + 1)) << stride) + (x + 0)] = y2;
        output[((((z + 0) << stride) + (y + 1)) << stride) + (x + 1)] = y3;
        output[((((z + 1) << stride) + (y + 0)) << stride) + (x + 0)] = y4;
        output[((((z + 1) << stride) + (y + 0)) << stride) + (x + 1)] = y5;
        output[((((z + 1) << stride) + (y + 1)) << stride) + (x + 0)] = y6;
        output[((((z + 1) << stride) + (y + 1)) << stride) + (x + 1)] = y7;
    }
  }
}

// adjust gain of different parts
void amplify3d
  ( int stride                          // transform is [1 << (stride * 3)]
  , float * restrict transform          // modified in place
  , const float * restrict gain         // gain for each octave/dir, [7 << stride]
  )
{
  for (int size = 1; size <= stride; ++size)
  {
    int d = 1 << (size - 1);
    for (int z = 0; z < 1 << size; z += 2)
    for (int y = 0; y < 1 << size; y += 2)
    for (int x = 0; x < 1 << size; x += 2)
    {
      int k1 = (((((z >> 1) + 0) << stride) + ((y >> 1) + 0)) << stride) + ((x >> 1) + d);
      int k2 = (((((z >> 1) + 0) << stride) + ((y >> 1) + d)) << stride) + ((x >> 1) + 0);
      int k3 = (((((z >> 1) + 0) << stride) + ((y >> 1) + d)) << stride) + ((x >> 1) + d);
      int k4 = (((((z >> 1) + d) << stride) + ((y >> 1) + 0)) << stride) + ((x >> 1) + 0);
      int k5 = (((((z >> 1) + d) << stride) + ((y >> 1) + 0)) << stride) + ((x >> 1) + d);
      int k6 = (((((z >> 1) + d) << stride) + ((y >> 1) + d)) << stride) + ((x >> 1) + 0);
      int k7 = (((((z >> 1) + d) << stride) + ((y >> 1) + d)) << stride) + ((x >> 1) + d);
      transform[k1] *= gain[7 * (size - 1) + 0];
      transform[k2] *= gain[7 * (size - 1) + 1];
      transform[k3] *= gain[7 * (size - 1) + 2];
      transform[k4] *= gain[7 * (size - 1) + 3];
      transform[k5] *= gain[7 * (size - 1) + 4];
      transform[k6] *= gain[7 * (size - 1) + 5];
      transform[k7] *= gain[7 * (size - 1) + 6];
    }
  }
}

// generate noise
void noise3d
  ( int stride                          // output is [1 << (3 * stride)]
  , float * restrict output             // output is overwritten
  )
{
  for (int k = 0; k < 1 << (3 * stride); ++k)
  {
    output[k] = rand() / (float) RAND_MAX - 0.5;
  }
}

// blit a tile into a larger one
void blit3d
  ( int ostride                         // output is [1 << (3 * ostride)]
  , int istride                         // input is [1 << (3 * istride)]
  , float * restrict output             // output is overwritten
  , const float * restrict input        // input is preserved
  )
{
  for (int z = 0; z < 1 << istride; ++z)
  for (int y = 0; y < 1 << istride; ++y)
  for (int x = 0; x < 1 << istride; ++x)
  {
    output[(((z << ostride) + y) << ostride) + x]
      = input[(((z << istride) + y) << istride) + x];
  }
}
