// note: uses stack allocated arrays, may need `ulimit -s unlimited`

#include <complex.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sndfile.h>

#include <fftw3.h>

#include <gsl/gsl_linalg.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

#define FFTSIZE 256
#define FFTBINS (FFTSIZE/2 + 1)
#define OVERLAP 16
#define OHOP (FFTSIZE / OVERLAP)
#define SOMSIZE 16

int main(int argc, char **argv)
{
  if (argc != 3)
  {
    fprintf
      ( stderr
      , "usage:\n"
        "  %s in.wav out.dat\n"
      , argv[0]
      );
    return 1;
  }
  SF_INFO info;
  memset(&info, 0, sizeof(info));
  fprintf(stderr, "reading %s\n", argv[1]);
  SNDFILE *ifile = sf_open(argv[1], SFM_READ, &info);
  if (! ifile) abort();
  const int CHANNELS = info.channels;

  float window[FFTSIZE];
  memset(window, 0, sizeof(window));
  for (int i = 0; i < FFTSIZE; ++i)
  {
    window[i] = 0.5 - 0.5 * cos(i * 6.283185307179586 / FFTSIZE);
  }

  double _Complex *fft_in = fftw_malloc(FFTSIZE * sizeof(*fft_in));
  double _Complex *fft_out = fftw_malloc(FFTSIZE * sizeof(*fft_out));
  fftw_plan forward = fftw_plan_dft_1d(FFTSIZE, fft_in, fft_out, FFTW_FORWARD, FFTW_PATIENT | FFTW_DESTROY_INPUT);

  int Ecount = 0;
  double Emean[FFTBINS];
  memset(Emean, 0, sizeof(Emean));
  double Ecov[FFTBINS][FFTBINS];
  memset(Ecov, 0, sizeof(Ecov));

  double imap[SOMSIZE][SOMSIZE][FFTBINS];
  memset(imap, 0, sizeof(imap));

  gsl_rng_env_setup();
  gsl_rng *PRNG = gsl_rng_alloc(gsl_rng_default);
  int step = 0;
  int lasti = 0, lastj = 0;
  double chain[SOMSIZE][SOMSIZE][SOMSIZE][SOMSIZE];
  memset(chain, 0, sizeof(chain));
  double total[SOMSIZE][SOMSIZE];
  memset(total, 0, sizeof(total));

  for (int pass = 0; pass < 4; ++pass)
  {
    fprintf(stderr, "pass %d/4\n", pass + 1);
    sf_seek(ifile, 0, SEEK_SET);
    float ibuffer[FFTSIZE][CHANNELS];
    memset(ibuffer, 0, sizeof(ibuffer));

    if (pass == 2)
    {
      // Cholesky decomposition of covariance matrix
      for (int i = 0; i < FFTBINS; ++i)
      {
        for (int j = 0; j < FFTBINS; ++j)
        {
          Ecov[i][j] /= Ecount;
//          fprintf(stderr, "%g ", Ecov[i][j]);
        }
//        fprintf(stderr, "\n");
      }
      gsl_matrix_view cov = gsl_matrix_view_array(&Ecov[0][0], FFTBINS, FFTBINS);
      gsl_linalg_cholesky_decomp1(&cov.matrix);
      // randomize SOM
      for (int i = 0; i < SOMSIZE; ++i)
      {
        for (int j = 0; j < SOMSIZE; ++j)
        {
          // generate independent Gaussian vectors
          double d[FFTBINS];
          for (int k = 0; k < FFTBINS; ++k)
          {
            d[k] = gsl_ran_gaussian(PRNG, 1);
          }
          gsl_vector_view x = gsl_vector_view_array(d, FFTBINS);
          // correlate them to the correct covariance matrix
          gsl_blas_dtrmv(CblasLower, CblasNoTrans, CblasNonUnit, &cov.matrix, &x.vector);
          for (int k = 0; k < FFTBINS; ++k)
          {
            imap[i][j][k] = Emean[k] / Ecount + d[k];
          }
        }
      }
      step = 0;
    }

    int reading = OVERLAP;
    int ix = 0;
    while (reading > 0)
    {
      if (pass > 0)
      {
        int percent0 = 100 * ix / (double) Ecount;
        ix++;
        int percent1 = 100 * ix / (double) Ecount;
        if (percent0 != percent1)
        {
          fprintf(stderr, "%4d%%\r", percent1);
        }
      }

      // read input
      for (int i = 0; i < FFTSIZE - OHOP; ++i)
      {
        for (int channel = 0; channel < CHANNELS; ++channel)
        {
          ibuffer[i][channel] = ibuffer[i + OHOP][channel];
        }
      }
      for (int i = FFTSIZE - OHOP; i < FFTSIZE; ++i)
      {
        for (int channel = 0; channel < CHANNELS; ++channel)
        {
          ibuffer[i][channel] = 0;
        }
      }
      if (reading == OVERLAP)
      {
        int r = sf_readf_float(ifile, &ibuffer[FFTSIZE - OHOP][0], OHOP);
        if (r != OHOP)
        {
          reading--;
        }
      }
      else
      {
        reading--;
      }
  
      // clear spectrum
      double E[FFTBINS];
      for (int i = 0; i < FFTBINS; ++i)
      {
        E[i] = 0;
      }
      for (int channel = 0; channel < CHANNELS; ++channel)
      {
        // window input
        for (int i = 0; i < FFTSIZE; ++i)
        {
          fft_in[i] = ibuffer[i][channel] * window[i];
        }
        // FFT
        fftw_execute(forward);
        // accumulate magnitude
        for (int i = 0; i < FFTBINS; ++i)
        {
          E[i] += cabs(fft_out[i]);
        }
      }

      if (pass == 0)
      {
        // compute mean
        for (int octave = 0; octave < FFTBINS; ++octave)
        {
          Emean[octave] += E[octave];
        }
        Ecount += 1;
      }
      else if (pass == 1)
      {
        // compute covariance
        for (int octave1 = 0; octave1 < FFTBINS; ++octave1)
        {
          for (int octave2 = 0; octave2 < FFTBINS; ++octave2)
          {
            Ecov[octave1][octave2]
              += (E[octave1] - Emean[octave1] / Ecount)
               * (E[octave2] - Emean[octave2] / Ecount);
          }
        }
      }
      else if (pass == 2)
      {
        // compute SOM
        
        // find best match
        int mini = 0;
        int minj = 0;
        double mins = 1.0 / 0.0;
        //#pragma omp parallel for collapse(2)
        for (int i = 0; i < SOMSIZE; ++i)
        {
          for (int j = 0; j < SOMSIZE; ++j)
          {
            double s = 0;
            for (int k = 0; k < FFTBINS; ++k)
            {
              double d = E[k] - imap[i][j][k];
              s += d * d;
            }
            //#pragma omp critical
            if (s < mins)
            {
              mins = s;
              mini = i;
              minj = j;
            }
          }
        }
        
        // update weights
        //#pragma omp parallel for collapse(2)
        for (int i = 0; i < SOMSIZE; ++i)
        {
          for (int j = 0; j < SOMSIZE; ++j)
          {
            double di = (i - mini) * 1.0 / SOMSIZE;
            double dj = (j - minj) * 1.0 / SOMSIZE;
            // FIXME magic numbers to control learning rate
            double kernel = 0.1 * exp(- (di * di + dj * dj) * (step + 1.0) / 100000);
            for (int k = 0; k < FFTBINS; ++k)
            {                
              imap[i][j][k] += kernel * (E[k] - imap[i][j][k]);
            }
          }
        }
        step++;
      }
      else if (pass == 3)
      {
        // compute Markov chain
        
        // find best match
        int mini = 0;
        int minj = 0;
        double mins = 1.0 / 0.0;
        //#pragma omp parallel for collapse(2)
        for (int i = 0; i < SOMSIZE; ++i)
        {
          for (int j = 0; j < SOMSIZE; ++j)
          {
            double s = 0;
            for (int k = 0; k < FFTBINS; ++k)
            {
              double d = E[k] - imap[i][j][k];
              s += d * d;
            }
            //#pragma omp critical
            if (s < mins)
            {
              mins = s;
              mini = i;
              minj = j;
            }
          }
        }
        chain[lasti][lastj][mini][minj] += 1;
        total[lasti][lastj] += 1;
        lasti = mini;
        lastj = minj;
      }
    } // while reading
  } // for pass
  sf_close(ifile);

  // output
  fprintf(stderr, "writing %s\n", argv[2]);
  FILE *ofile = fopen(argv[2], "wb");
  for (int i = 0; i < SOMSIZE; ++i)
  for (int j = 0; j < SOMSIZE; ++j)
  for (int k = 0; k < FFTBINS; ++k)
    fprintf(ofile, "%.18e\n", imap[i][j][k]);
  for (int i = 0; i < SOMSIZE; ++i)
  for (int j = 0; j < SOMSIZE; ++j)
  for (int ii = 0; ii < SOMSIZE; ++ii)
  for (int jj = 0; jj < SOMSIZE; ++jj)
    fprintf(ofile, "%.18e\n", chain[i][j][ii][jj] / total[i][j]);
  fclose(ofile);
  fprintf(stderr, "total frames %d\n", Ecount);

  return 0;
}
