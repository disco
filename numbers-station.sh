#!/bin/bash
set -e
voice=slt
flite -voice awb -t "corona" -o input.wav
sox input.wav tmp.wav rate 44100
rubberband --duration 20 tmp.wav input.wav
./fft-analysis.sh
mv "output.txt" "input.csv"
python3 ./var-analysis.py 2>/dev/null
#flite -voice awb -t "stretch" -o input.wav
#sox input.wav tmp.wav rate 44100
#rubberband --duration 300 tmp.wav input.wav
cp 2018-09-20-09-32-23--2-3-4-5-2-2-2.wav input.wav
./fft-analysis.sh
./var-simulate 129 8 "params.csv" "sigmau.csv" < "output.txt" > "output.csv" 2>/dev/null
mv "output.csv" "output.txt"
./fft-synthesis.sh
./normalize output.wav number-station.wav
