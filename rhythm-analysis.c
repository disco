// note: uses stack allocated arrays, may need `ulimit -s unlimited`

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sndfile.h>

#define OCTAVES 13
#define OCTAVESIZE (1 << (OCTAVES - 1))

#define RHYTHMS 11
#define RHYTHMSIZE (1 << (RHYTHMS - 1))

#define OVERLAP 4

#define OHOP (OCTAVESIZE / OVERLAP)
#define RHOP (RHYTHMSIZE / OVERLAP)

int main(int argc, char **argv)
{
  if (argc != 3)
  {
    fprintf
      ( stderr
      , "usage:\n"
        "  %s in.wav out.dat\n"
      , argv[0]
      );
    return 1;
  }
  SF_INFO info;
  memset(&info, 0, sizeof(info));
  fprintf(stderr, "reading %s\n", argv[1]);
  SNDFILE *ifile = sf_open(argv[1], SFM_READ, &info);
  if (! ifile) abort();
  const int CHANNELS = info.channels;

  float ibuffer[OCTAVESIZE][CHANNELS];
  memset(ibuffer, 0, sizeof(ibuffer));

  float window[OCTAVESIZE];
  memset(window, 0, sizeof(window));
  for (int i = 0; i < OCTAVESIZE; ++i)
  {
    window[i] = 0.5 - 0.5 * cos(i * 6.283185307179586 / OCTAVESIZE);
  }

  float rwindow[RHYTHMSIZE];
  memset(rwindow, 0, sizeof(rwindow));
  for (int i = 0; i < RHYTHMSIZE; ++i)
  {
#if 0
    // FIXME needs two passes (for DC removal)
    rwindow[i] = 0.5 - 0.5 * cos(i * 6.283185307179586 / RHYTHMSIZE);
#else
    // FIXME use one pass without windowing for now
    rwindow[i] = 1;
#endif
  }

  int eindex = 0;
  float ebuffer[RHYTHMSIZE][OCTAVES];
  memset(ebuffer, 0, sizeof(ebuffer));

  float R[OCTAVES][RHYTHMS];
  memset(R, 0, sizeof(R));
  int R_count = 0;

  int reading = OVERLAP;
  while (reading > 0)
  {

    // read input
    for (int i = 0; i < OCTAVESIZE - OHOP; ++i)
    {
      for (int channel = 0; channel < CHANNELS; ++channel)
      {
        ibuffer[i][channel] = ibuffer[i + OHOP][channel];
      }
    }
    for (int i = OCTAVESIZE - OHOP; i < OCTAVESIZE; ++i)
    {
      for (int channel = 0; channel < CHANNELS; ++channel)
      {
        ibuffer[i][channel] = 0;
      }
    }
    if (reading == OVERLAP)
    {
      int r = sf_readf_float(ifile, &ibuffer[OCTAVESIZE - OHOP][0], OHOP);
      if (r != OHOP)
      {
        reading--;
      }
    }  
    else
    {
      reading--;
    }

    // window
    float haar[2][OCTAVESIZE][CHANNELS];
    memset(haar, 0, sizeof(haar));
    int src = 0;
    int dst = 1;
    for (int i = 0; i < OCTAVESIZE; ++i)
    {
      for (int channel = 0; channel < CHANNELS; ++channel)
      {
        haar[src][i][channel] = ibuffer[i][channel] * window[i];
      }
    }
  
    // compute Haar wavelet transform
    for (int length = OCTAVESIZE >> 1; length > 0; length >>= 1)
    {
      for (int i = 0; i < length; ++i)
      {
        for (int channel = 0; channel < CHANNELS; ++channel)
        {
          float a = haar[src][2 * i + 0][channel];
          float b = haar[src][2 * i + 1][channel];
          float s = (a + b) / 2;
          float d = (a - b) / 2;
          haar[dst][         i][channel] = s;
          haar[dst][length + i][channel] = d;
        }
      }
      for (int i = 0; i < OCTAVESIZE; ++i)
      {
        for (int channel = 0; channel < CHANNELS; ++channel)
        {
          haar[src][i][channel] = haar[dst][i][channel];
        }
      }
    }
  
    // compute energy per octave
    double E[OCTAVES];
    memset(E, 0, sizeof(E));
    for (int channel = 0; channel < CHANNELS; ++channel)
    {
      E[0] += fabsf(haar[src][0][channel]); // DC
    }
    for ( int octave = 1, length = 1
        ; length <= OCTAVESIZE >> 1
        ; octave += 1, length <<= 1
        )
    {
      double rms = 0;
      for (int channel = 0; channel < CHANNELS; ++channel)
      {
        for (int i = 0; i < length; ++i)
        {
          double a = haar[src][length + i][channel];
          rms += a * a;
        }
      }
      rms /= length * CHANNELS;
      rms = sqrt(fmax(rms, 0));
      E[octave] = rms;
    }
    
    // copy to ebuffer
    for (int octave = 0; octave < OCTAVES; ++octave)
    {
      ebuffer[eindex][octave] = E[octave];
    }
    eindex++;
  
    if (eindex == RHYTHMSIZE)
    {
      
      for (int octave = 0; octave < OCTAVES; ++octave)
      {
      
        float rhaar[2][RHYTHMSIZE][CHANNELS];
        memset(rhaar, 0, sizeof(rhaar));
  
        // window
        for (int i = 0; i < RHYTHMSIZE; ++i)
        {
          rhaar[src][i][0] = ebuffer[i][octave] * rwindow[i];
        }
  
        // compute Haar wavelet transform
        for (int length = RHYTHMSIZE >> 1; length > 0; length >>= 1)
        {
          for (int i = 0; i < length; ++i)
          {
              float a = rhaar[src][2 * i + 0][0];
              float b = rhaar[src][2 * i + 1][0];
              float s = (a + b) / 2;
              float d = (a - b) / 2;
              rhaar[dst][         i][0] = s;
              rhaar[dst][length + i][0] = d;
          }
          for (int i = 0; i < RHYTHMSIZE; ++i)
          {
              rhaar[src][i][0] = rhaar[dst][i][0];
          }
        }
        
        // compute energy per octave
        R[octave][0] += fabsf(rhaar[src][0][0]); // DC
        for ( int rhythm = 1, length = 1
            ; length <= RHYTHMSIZE >> 1
            ; rhythm += 1, length <<= 1
            )
        {
          double rms = 0;
          for (int i = 0; i < length; ++i)
          {
            double a = rhaar[src][length + i][0];
            rms += a * a;
          }
          rms /= length;
          rms = sqrt(fmax(rms, 0));
          R[octave][rhythm] += rms;
        }
        
      } // for octave
      R_count++;
      eindex -= RHOP;
  
      // shunt
      for (int i = 0; i < RHYTHMSIZE - RHOP; ++i)
      {
        for (int octave = 0; octave < OCTAVES; ++octave)
        {
          ebuffer[i][octave] = ebuffer[i + RHOP][octave];
        }
      }
      for (int i = RHYTHMSIZE - RHOP; i < RHYTHMSIZE; ++i)
      {
        for (int octave = 0; octave < OCTAVES; ++octave)
        {
          ebuffer[i][octave] = 0;
        }
      }
              
    } // if (eindex == RHYTHMSIZE)
  } // while (reading > 0)
  
  sf_close(ifile);

  // output fingerprint
  fprintf(stderr, "writing %s\n", argv[2]);
  FILE *ofile = fopen(argv[2], "wb");
  for (int i = 0; i < OCTAVES; ++i)
  {
    for (int j = 0; j < RHYTHMS; ++j)
    {
      fprintf(ofile, "%.18e\n", R[i][j] / R_count);
    }
    fprintf(ofile, "\n");
  }
  fclose(ofile);
  fprintf(stderr, "total frames %d\n", R_count);

  return 0;
}
