#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <vector>
#include <glm/glm.hpp>

const double pi = 3.141592653589793;

double f(const glm::dvec3 &x, const glm::dvec3 &mu, const glm::dmat3 &sigma)
{
  return std::exp(-0.5 * glm::dot((x - mu) * glm::inverse(sigma), (x - mu)))
    / std::sqrt(std::pow(2 * pi, 3) * glm::determinant(sigma));
}

glm::dvec3 x(const unsigned char *xs, int i)
{
  return glm::dvec3
    ( xs[3 * i + 0]
    , xs[3 * i + 1]
    , xs[3 * i + 2]
    );
}

int main1(unsigned char *xs, int samples)
{
  double min_BIC = 1.0 / 0.0;
  std::vector<double> best_tau;
  std::vector<glm::dvec3> best_mu;
  std::vector<glm::dmat3> best_sigma;
  for (int clusters = 1; clusters <= 16; ++clusters)
  {
    std::vector<double> cur_tau;
    std::vector<glm::dvec3> cur_mu;
    std::vector<glm::dmat3> cur_sigma;
    double min_err = 1.0 / 0.0;
    for (int attempt = 0; attempt < 10; ++attempt)
    {
      std::fprintf(stderr, "\r\t%d\t%2d/10\t%2ld\t%g \t", clusters, attempt + 1, best_mu.size(), min_BIC);
      // randomize Gaussian mixture
      std::vector<double> tau(clusters);
      std::vector<glm::dvec3> mu(clusters);
      std::vector<glm::dmat3> sigma(clusters);
      for (int j = 0; j < clusters; ++j)
      {
        tau[j] = 1.0 / clusters;
        mu[j] = 255.0 * glm::dvec3
          ( rand() * 1.0 / RAND_MAX
          , rand() * 1.0 / RAND_MAX
          , rand() * 1.0 / RAND_MAX
          );
        sigma[j] = 255.0 * glm::dmat3
          ( 1.0, 0.0, 0.0
          , 0.0, 1.0, 0.0
          , 0.0, 0.0, 1.0
          );
      }
      double err = 0, last_err, eps = 1e-8;
      do
      {
        last_err = err;
        // compute membership probabilities
        double T[clusters][samples];
        for (int i = 0; i < samples; ++i)
        {
          double sum_Tj = 0;
          for (int j = 0; j < clusters; ++j)
          {
            T[j][i] = tau[j] * f(x(xs, i), mu[j], sigma[j]);
            sum_Tj += T[j][i];
          }
          for (int j = 0; j < clusters; ++j)
          {
            T[j][i] /= sum_Tj;
          }
        }
        // sum Tji
        double sum_Ti[clusters];
        for (int j = 0; j < clusters; ++j)
        {
          sum_Ti[j] = 0;
          for (int i = 0; i < samples; ++i)
          {
            sum_Ti[j] += T[j][i];
          }
        }
        // update tau
        for (int j = 0; j < clusters; ++j)
        {
          tau[j] = sum_Ti[j] / samples;
        }
        // update mu
        for (int j = 0; j < clusters; ++j)
        {
          glm::dvec3 sum_Tji_xi = glm::dvec3(0.0, 0.0, 0.0);
          for (int i = 0; i < samples; ++i)
          {
            sum_Tji_xi += T[j][i] * x(xs, i);
          }
          mu[j] = sum_Tji_xi / sum_Ti[j];
        }
        // update sigma (using new mu)
        for (int j = 0; j < clusters; ++j)
        {
          glm::dmat3 sum_Tji_sigma = glm::dmat3
            ( 0.0, 0.0, 0.0
            , 0.0, 0.0, 0.0
            , 0.0, 0.0, 0.0
            );
          for (int i = 0; i < samples; ++i)
          {
            glm::dvec3 d = x(xs, i) - mu[j];
            sum_Tji_sigma += T[j][i] * glm::outerProduct(d, d);
          }
          sigma[j] = sum_Tji_sigma / sum_Ti[j];
        }
        // compute error
        err = 0;
        for (int i = 0; i < samples; ++i)
        {
          double m = 1.0 / 0.0;
          for (int j = 0; j < clusters; ++j)
          {
            glm::dvec3 d = x(xs, i) - mu[j];
            double e = glm::dot(d * glm::inverse(sigma[j]), d);
            m = std::min(m, e);
          }
          err += m;
        }
        err /= 3;
//        std::fprintf(stderr, "\t%g  %+g\n", err, err - last_err);
      } while (err > last_err + eps);
      if (err < min_err)
      {
        min_err = err;
        cur_tau = tau;
        cur_mu = mu;
        cur_sigma = sigma;
      }
    }
    double BIC = samples * log(min_err / samples) + clusters * log(samples);
    std::fprintf(stderr, "%g\r", BIC);
    if (std::isinf(BIC) || std::isnan(BIC))
    {
      break;
    }
    if (BIC < min_BIC)
    {
      min_BIC = BIC;
      best_tau = cur_tau;
      best_mu = cur_mu;
      best_sigma = cur_sigma;
    }
  }
  // compute palette
  int clusters = best_mu.size();
  std::fprintf(stderr, "\noptimal cluster count %d\n", clusters);
  unsigned char palette[clusters][3];
  for (int j = 0; j < clusters; ++j)
  {
    palette[j][0] = std::min(std::max(std::round(best_mu[j][0]), 0.0), 255.0);
    palette[j][1] = std::min(std::max(std::round(best_mu[j][1]), 0.0), 255.0);
    palette[j][2] = std::min(std::max(std::round(best_mu[j][2]), 0.0), 255.0);
  }
  // remap image
  for (int i = 0; i < samples; ++i)
  {    
    double m = 1.0 / 0.0;
    int mj = 0;
    for (int j = 0; j < clusters; ++j)
    {
      glm::dvec3 d = x(xs, i) - best_mu[j];
      double e = glm::dot(d * glm::inverse(best_sigma[j]), d);
      if (e < m)
      {
        m = e;
        mj = j;
      }
    }
    xs[3 * i + 0] = palette[mj][0];
    xs[3 * i + 1] = palette[mj][1];
    xs[3 * i + 2] = palette[mj][2];
  }
  return clusters;
}

int main(int argc, char **argv)
{
  std::srand(std::time(0));
  for (int arg = 1; arg < argc; ++arg)
  {
    std::FILE *ppm = std::fopen(argv[arg], "rb");
    if (ppm)
    {
      if ('P' == std::fgetc(ppm))
      {
        if ('6' == std::fgetc(ppm))
        {
          if ('\n' == std::fgetc(ppm))
          {
            if ('#' == std::fgetc(ppm))
            {
              while ('\n' != std::fgetc(ppm))
              {
                if (std::feof(ppm))
                {
                  break;
                }
              }
            }
            else
            {
              std::fprintf(stderr, "# expected\n");
            }
            int width = 0, height = 0;
            if (2 == std::fscanf(ppm, "%d %d\n255", &width, &height))
            {
              if ('\n' == std::fgetc(ppm))
              {
                int bytes = width * height * 3;
                unsigned char *rgb = (unsigned char *)std::malloc(bytes);
                if (rgb)
                {
                  if (1 == std::fread(rgb, bytes, 1, ppm))
                  {
                    int colours = main1(rgb, width * height);
                    std::fprintf(stdout, "P6\n# %d\n%d %d\n255\n", colours, width, height);
                    std::fwrite(rgb, bytes, 1, stdout);
                    std::fflush(stdout);
                  }
                  else
                  {
                    std::fprintf(stderr, "couldn't read %d bytes\n", bytes);
                  }
                  std::free(rgb);
                }
                else
                {
                  std::fprintf(stderr, "couldn't allocate %d bytes\n", bytes);
                }
              }
              else
              {
                std::fprintf(stderr, "newline expected after dims\n");
              }
            }
            else
            {
              std::fprintf(stderr, "dims expected\n");
            }
          }
          else
          {
            std::fprintf(stderr, "newline expected\n");
          }
        }
        else
        {
          std::fprintf(stderr, "6 expected\n");
        }
      }
      else
      {
        std::fprintf(stderr, "P expected\n");
      }
      std::fclose(ppm);
    }
  }
  return 0;
}
