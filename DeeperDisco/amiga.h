#include <stdint.h>

#ifdef VBCC

#define __builtin_expect(x,y) (x)

static inline uint8_t __builtin_ctz(uint8_t i)
{
  if (i)
  {
    uint8_t r = 0;
    while (! (i & 1))
    {
      i >>= 1;
      r += 1;
    }
    return r;
  }
  else
  {
    return 8;
  }
}

#endif

static inline int strcmp(const char *a, const char *b)
{
  while (*a && *b && *a == *b)
  {
    a++;
    b++;
  }
  if (*a < *b) return -1;
  if (*a > *b) return 1;
  return 0;
}

