#include <stddef.h>
#include <syscall.h>
#include <sys/types.h>

static inline int openat(int dirfd_, const char *name_, int flags_)
{
    register int dirfd __asm__("r0") = dirfd_;
    register const char *name __asm__("r1") = name_;
    register int flags __asm__("r2") = flags_;
    register int sys __asm__("r7") = SYS_openat;
    register int ret __asm__("r0");
    __asm__ __volatile__
    (
        "swi 0"
        : "=r"(ret)
        : "r"(sys), "r"(dirfd), "r"(name), "r"(flags)
        : "memory"
    );
    return ret;
}


static inline int close(int fd_)
{
    register int fd __asm__("r0") = fd_;
    register int sys __asm__("r7") = SYS_close;
    register int ret __asm__("r0");
    __asm__ __volatile__
    (
        "swi 0"
        : "=r"(ret)
        : "r"(sys), "r"(fd)
        : "memory"
    );
    return ret;
}

static inline int ioctl(int fd_, unsigned int cmd_, void *arg_)
{
    register int fd __asm__("r0") = fd_;
    register unsigned int cmd __asm__("r1") = cmd_;
    register void * arg __asm__("r2") = arg_;
    register int sys __asm__("r7") = SYS_ioctl;
    register int ret __asm__("r0");
    __asm__ __volatile__
    (
        "swi 0"
        : "=r"(ret)
        : "r"(sys), "r"(fd), "r"(cmd), "r"(arg)
        : "memory"
    );
    return ret;
}

static inline ssize_t write(int fd_, const void *buf_, size_t size_)
{
    register int fd __asm__("r0") = fd_;
    register const void *buf __asm__("r1") = buf_;
    register size_t size __asm__("r2") = size_;
    register int sys __asm__("r7") = SYS_write;
    register ssize_t ret __asm__("r0");
    __asm__ __volatile__
    (
        "swi 0"
        : "=r"(ret)
        : "r"(sys), "r"(fd), "r"(buf), "r"(size)
        : "memory"
    );
    return ret;
}

static inline ssize_t read(int fd_, void *buf_, size_t size_)
{
    register int fd __asm__("r0") = fd_;
    register void *buf __asm__("r1") = buf_;
    register size_t size __asm__("r2") = size_;
    register int sys __asm__("r7") = SYS_read;
    register ssize_t ret __asm__("r0");
    __asm__ __volatile__
    (
        "swi 0"
        : "=r"(ret)
        : "r"(sys), "r"(fd), "r"(buf), "r"(size)
        : "memory"
    );
    return ret;
}

static inline void exit(int code_)
{
    register int code __asm__("r0") = code_;
    register int sys __asm__("r7") = SYS_exit;
    __asm__ __volatile__
    (
        "swi 0"
        :
        : "r"(sys), "r"(code)
        :
    );
    __builtin_unreachable();
}

#define ALIGNED
