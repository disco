#include <stddef.h>
#include <syscall.h>
#include <sys/types.h>

static inline int openat(int dirfd, const char *name, int flags)
{
    int ret;
    __asm__ __volatile__
    (
        "int $0x80"
        : "=a"(ret)
        : "0"(SYS_openat), "b"(dirfd), "c"(name), "d"(flags)
        : "memory"
    );
    return ret;
}

static inline int close(int fd)
{
    int ret;
    __asm__ __volatile__
    (
        "int $0x80"
        : "=a"(ret)
        : "0"(SYS_close), "b"(fd)
        : "memory"
    );
    return ret;
}

static inline int ioctl(int fd, unsigned int cmd , void *arg)
{
    int ret;
    __asm__ __volatile__
    (
        "int $0x80"
        : "=a" (ret)
        : "0"(SYS_ioctl), "b"(fd), "c"(cmd), "d"(arg)
        : "memory"    // the kernel dereferences pointer args
    );
    return ret;
}

// i386 Linux
static inline ssize_t write(int fd, const void *buf, size_t size)
{
    ssize_t ret;
    __asm__ __volatile__
    (
        "int $0x80"
        : "=a" (ret)
        : "0"(SYS_write), "b"(fd), "c"(buf), "d"(size)
        : "memory"    // the kernel dereferences pointer args
    );
    return ret;
}

// returns negative value for error (for example, if error is EINVAL, then -EINVAL is returned)
static inline ssize_t read(int fd, void *buf, size_t size)
{
    ssize_t ret;
    __asm__ __volatile__
    (
        "int $0x80"
        : "=a" (ret)
        : "0"(SYS_read), "b"(fd), "c"(buf), "d"(size)
        : "memory"    // the kernel dereferences pointer args
    );
    return ret;
}

static inline void exit(int code)
{
    __asm__ __volatile__
    (
        "int $0x80"
        :
        : "a"(SYS_exit), "b"(code)
        :
    );
    __builtin_unreachable();
}

#define ALIGNED __attribute__((force_align_arg_pointer))
