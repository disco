#ifdef X86_64
#include "linux-x86_64.h"
#else
#ifdef I686
#include "linux-i686.h"
#else
#ifdef AARCH64
#include "linux-aarch64.h"
#else
#ifdef ARMV7
#include "linux-armv7.h"
#else
#error unsupported architecture
#endif
#endif
#endif
#endif

#define O_RDONLY 0
#define O_WRONLY 1
#define AT_FDCWD (-100)
