#include <stdint.h>
#define deeperdisco_blocksize (4 << 8)
#if defined(LINUX) || defined(WINDOWS)
#define deeperdisco_samplerate 48000
#define deeperdisco_octaves 10
#define deeperdisco_sequence_bits 5
#define deeperdisco_gain deeperdisco_octaves
#define deeperdisco_min (-200000000)
#define deeperdisco_max ( 200000000)
typedef int16_t sample8_t;
typedef int32_t sample16_t;
typedef int32_t sample32_t;
#define sample8(x) (((x) >> 16) & 0xFFFF)
#define deeperdisco_offset 0
void deeperdisco_audio(sample8_t *out1, uint16_t blocksize);
#endif
#ifdef AMIGA
#ifdef M68000
//#define deeperdisco_samplerate (36 << 8)
#define deeperdisco_pal 384
#define deeperdisco_ntsc 388
#define deeperdisco_octaves 7
#define deeperdisco_sequence_bits 2
#define deeperdisco_min (-4680)
#define deeperdisco_max ( 4680)
#endif
#ifdef M68020
//#define deeperdisco_samplerate (108 << 8)
#define deeperdisco_pal 128
#define deeperdisco_ntsc 129
#define deeperdisco_octaves 9
#define deeperdisco_sequence_bits 3
#define deeperdisco_gain ((int32_t) (deeperdisco_octaves) << 16)
#define deeperdisco_min (-3640)
#define deeperdisco_max ( 3640)
#endif
typedef int8_t sample8_t;
typedef int16_t sample16_t;
typedef int32_t sample32_t;
#define sample8(x) (((x)>>8)&0xff)
#define deeperdisco_offset 0x80
void deeperdisco_audio(sample8_t *out1, sample8_t *out2, uint16_t blocksize);
#endif
#define deeperdisco_sequence_mask ((1 << deeperdisco_sequence_bits) - 1)
#define deeperdisco_octave_mask ((1 << (deeperdisco_octaves - 1)) - 1)
void deeperdisco_initialize(uint32_t seed);
void deeperdisco_sequence(void);
