#include <windows.h>
#include <mmsystem.h>
#include <stdbool.h>

#include "deeperdisco.h"

#define BUFFERS 32
#define CHANNELS 2

typedef struct {
    WORD chnl;
    DWORD smpl;
    WORD bits;
    int size;
} WAVE;

struct audio
{
  volatile bool running;
  HANDLE event, stopped, thread;

  WAVE wave;
  WAVEFORMATEX wfmt;

    HWAVEOUT    hWaveOut; 
    HGLOBAL     hWaveHdr; 
    LPWAVEHDR   lpWaveHdr; 
    HMMIO       hmmio; 
    UINT        wResult; 
    HANDLE      hFormat; 
    WAVEFORMAT  *pFormat; 
    DWORD       dwDataSize; 
    
  WAVEHDR whdr[BUFFERS];
  int16_t data[BUFFERS][deeperdisco_blocksize][CHANNELS];
};

DWORD APIENTRY audio_thread(LPVOID data)
{
  struct audio *a = (struct audio *) data;
  uint32_t seed = GetTickCount();
  seed += ! seed;
  deeperdisco_initialize(seed);
  long size = a->wave.chnl * a->wave.size * (a->wave.bits >> 3);
  for (int i = 0; i < BUFFERS; ++i)
  {
    a->whdr[i].dwBufferLength = size;
    a->whdr[i].dwFlags = 0;
    a->whdr[i].dwLoops = 0;
    a->whdr[i].lpData = (LPSTR) &a->data[i][0][0];
    waveOutPrepareHeader(a->hWaveOut, &a->whdr[i], sizeof(WAVEHDR));
    deeperdisco_sequence();
    deeperdisco_audio(&a->data[i][0][0], deeperdisco_blocksize);
  }
  ResetEvent(a->event);
  for (int i = 0; i < BUFFERS; ++i)
  {
    waveOutWrite(a->hWaveOut, &a->whdr[i], sizeof(WAVEHDR));
  }
  int i = 0;
  while (true)
  {
    WaitForSingleObject(a->event, INFINITE);
    ResetEvent(a->event);
    if (! a->running)
    {
      break;
    }
    deeperdisco_sequence();
    deeperdisco_audio(&a->data[i][0][0], deeperdisco_blocksize);
    waveOutWrite(a->hWaveOut, &a->whdr[i], sizeof(WAVEHDR));
    ++i;
    if (i == BUFFERS)
    {
      i = 0;
    }
  }
  waveOutReset(a->hWaveOut);
  for (int j = 0; j < BUFFERS; ++j)
  {
    waveOutUnprepareHeader(a->hWaveOut, &a->whdr[i], sizeof(WAVEHDR));
    ++i;
    if (i == BUFFERS)
    { 
      i = 0;
    }
  }
  SetEvent(a->stopped);
  return 0;
}

struct audio *audio_start(void)
{
  struct audio *a = (struct audio *) GlobalAlloc(GMEM_FIXED | GMEM_ZEROINIT, sizeof(*a));
  a->wave = (WAVE){2, deeperdisco_samplerate, 16, deeperdisco_blocksize};
  a->wfmt = (WAVEFORMATEX){WAVE_FORMAT_PCM, a->wave.chnl, a->wave.smpl,
                         ((a->wave.chnl * a->wave.bits) >> 3) * a->wave.smpl,
                          (WORD)((a->wave.chnl * a->wave.bits) >> 3), a->wave.bits, 0};
  a->event = CreateEvent(0, 1, 0, 0);
  a->stopped = CreateEvent(0, 1, 0, 0);
  if (waveOutOpen(&a->hWaveOut, WAVE_MAPPER, &a->wfmt, (intptr_t) a->event, (intptr_t) a, CALLBACK_EVENT))
  {
    CloseHandle(a->event);
    CloseHandle(a->stopped);
    GlobalFree(a);
    return 0;
  }
  a->running = true;
  SetThreadPriority(a->thread = CreateThread(0, 1024 * 1024 * 8, audio_thread, a, 0, 0), THREAD_PRIORITY_TIME_CRITICAL);
  return a;
}

void audio_stop(struct audio *a)
{
  a->running = false;
  WaitForSingleObject(a->stopped, INFINITE);
  waveOutClose(a->hWaveOut);
  CloseHandle(a->event);
  CloseHandle(a->stopped);
  CloseHandle(a->thread);
  GlobalFree(a);
}

#if 0
double audio_get_time(struct audio *a)
{
  MMTIME mmtime;
  mmtime.wType = TIME_SAMPLES;
  waveOutGetPosition(a->hWaveOut, &mmtime, sizeof(MMTIME));
  if (mmtime.wType == TIME_SAMPLES)
  {
    return mmtime.u.sample / (double) a->wave.smpl;
  }
  return 0;
}
#endif

int APIENTRY wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nCmdShow)
{
  (void) hInstance;
  (void) hPrevInstance;
  (void) lpCmdLine;
  (void) nCmdShow;
  struct audio *a;
  if ((a = audio_start()))
  {
    MessageBox(0, L"Click OK to exit.", L"DeeperDisco", MB_OK);
    audio_stop(a);
  }
  return 0;
}

__attribute((externally_visible)) 
extern int WinMainCRTStartup(void)
{
  int r = wWinMain(GetModuleHandle(0), 0, 0, SW_SHOWNORMAL);
  TerminateProcess(GetCurrentProcess(), r);
  return r;
}
