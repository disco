#include "deeperdisco.h"

#ifdef AMIGA
#include "amiga.h"
#endif

#define   likely(x) __builtin_expect((x), 1)
#define unlikely(x) __builtin_expect((x), 0)

int8_t ctztab[1 << (deeperdisco_octaves - 1)]
#ifdef AMIGA
=
{ 6,0,1,0, 2,0,1,0
, 3,0,1,0, 2,0,1,0
, 4,0,1,0, 2,0,1,0
, 3,0,1,0, 2,0,1,0
, 5,0,1,0, 2,0,1,0
, 3,0,1,0, 2,0,1,0
, 4,0,1,0, 2,0,1,0
, 3,0,1,0, 2,0,1,0
#ifdef M68020
, 7,0,1,0, 2,0,1,0
, 3,0,1,0, 2,0,1,0
, 4,0,1,0, 2,0,1,0
, 3,0,1,0, 2,0,1,0
, 5,0,1,0, 2,0,1,0
, 3,0,1,0, 2,0,1,0
, 4,0,1,0, 2,0,1,0
, 3,0,1,0, 2,0,1,0
, 8,0,1,0, 2,0,1,0
, 3,0,1,0, 2,0,1,0
, 4,0,1,0, 2,0,1,0
, 3,0,1,0, 2,0,1,0
, 5,0,1,0, 2,0,1,0
, 3,0,1,0, 2,0,1,0
, 4,0,1,0, 2,0,1,0
, 3,0,1,0, 2,0,1,0
, 7,0,1,0, 2,0,1,0
, 3,0,1,0, 2,0,1,0
, 4,0,1,0, 2,0,1,0
, 3,0,1,0, 2,0,1,0
, 5,0,1,0, 2,0,1,0
, 3,0,1,0, 2,0,1,0
, 4,0,1,0, 2,0,1,0
, 3,0,1,0, 2,0,1,0
#endif
}
#endif
;
sample16_t src[deeperdisco_octaves][256][2];

typedef struct
{
  uint16_t grain, increment, increment2, _pad;
  uint16_t i, j, k, o;
  sample16_t out[2];
} octave_t;

octave_t state[deeperdisco_octaves];
sample32_t acc[2];

uint32_t prng;
static inline uint32_t rand32(void)
{
  prng ^= prng << 13;
  prng ^= prng >> 17;
  prng ^= prng << 5;
  return prng;
}

#if defined(LINUX) || defined(WINDOWS)
void deeperdisco_audio(sample8_t *out0, uint16_t blocksize)
#endif
#ifdef AMIGA
void deeperdisco_audio(sample8_t *out0, sample8_t *out1, uint16_t blocksize)
#endif
{
  register sample32_t acc0 = acc[0], acc1 = acc[1];
  for (uint16_t ix = 0; ix < blocksize; ++ix)
  {
    int8_t octave = ctztab[ix & deeperdisco_octave_mask];
    register octave_t *p = &state[octave];
    uint16_t i = p->i += p->increment;
    uint16_t o = p->o + p->increment2;
    uint16_t old_j = p->j;
    uint16_t j = p->j = i >> 8;
    uint16_t k = p->k;
    uint16_t grain = p->grain;
    if (unlikely(j > old_j))
    {
      p->k = k += grain;
    }
    if (unlikely(o >= grain))
    {
      o -= grain;
    }
    p->o = o;
    uint8_t q = (((uint16_t)(o + k)) >> 8) & 0xff;
    acc0 -= p->out[0];
    acc1 -= p->out[1];
    p->out[0] = src[octave][q][0];
    p->out[1] = src[octave][q][1];
    acc0 += p->out[0];
    acc1 += p->out[1];
    *out0++ = sample8(acc0);
#if defined(LINUX) || defined(WINDOWS)
    *out0++ = sample8(acc1);
#endif
#ifdef AMIGA
    *out1++ = sample8(acc1);
#endif
  }
}

void deeperdisco_initialize(uint32_t seed)
{
  seed += ! seed;
  prng = seed;

#ifndef AMIGA
  ctztab[0] = deeperdisco_octaves - 1;
  for (int i = 1; i <= deeperdisco_octave_mask; ++i)
  {
    ctztab[i] = __builtin_ctz(i);
  }
#endif

  sample16_t *s = &src[0][0][0];
  const uint16_t count = deeperdisco_octaves * 256 * 2;
  for (uint16_t i = 0; i < count; ++i)
  {
#ifdef AMIGA
#ifdef M68000
    sample16_t t;
    do
    {
      t = ((sample16_t) (int32_t) rand32()) >> 3;
    } while (t < deeperdisco_min || deeperdisco_max < t);
    *s++ = t;
#endif
#ifdef M68020
    sample16_t t;
    do
    {
      t = ((sample32_t) (int32_t) rand32()) / ((sample32_t) deeperdisco_gain);
    } while (t < deeperdisco_min || deeperdisco_max < t);
    *s++ = t;
#endif
#endif
#if defined(LINUX) || defined(WINDOWS)
    sample16_t t;
    do
    {
#if defined(WINDOWS) && defined(ARMV7)
      t = ((sample32_t) (int32_t) rand32()) / ((float) deeperdisco_gain);
#else
      t = ((sample32_t) (int32_t) rand32()) / ((sample32_t) deeperdisco_gain);
#endif
    } while (t < deeperdisco_min || deeperdisco_max < t);
    *s++ = t;
#endif
  }

  for (uint8_t octave = 0; octave < deeperdisco_octaves; ++octave)
  {
    octave_t *p = &state[octave];
    p->grain = 0x200 + (rand32() & 0x1ff);
    p->increment = (rand32() & 0xF) + 1;
    p->increment2 = (rand32() & 0x3ff) + 0x10;
    p->i = 0;
    p->j = 0;
    p->k = 0;
    p->o = 0;
    p->out[0] = 0;
    p->out[1] = 0;
  }

  acc[0] = 0;
  acc[1] = 0;
}

void deeperdisco_sequence(void)
{
  uint32_t r = rand32();
  if ((r & deeperdisco_sequence_mask) == 0) // maximum 5 bits
  {
    r >>= deeperdisco_sequence_bits;
    uint8_t octave = r & 0xF; // 4
    if (octave < deeperdisco_octaves)
    {
      octave_t *p = &state[octave];
      r >>= 4;
      p->increment = (r & 0xF) + 1; // 4
      r >>= 4;
      p->increment2 = (r & 0x3ff) + 0x10; // 10
      r >>= 10;
      p->grain = 0x200 + (r & 0x1ff); // 9
//      r >>= 9;
    }
  }
}
