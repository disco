SOURCES = \
README \
Makefile \
deeperdisco.c \
deeperdisco.h \
amiga.c \
amiga.h \
amiga-ncrt0.S \
linux.c \
linux.h \
linux-aarch64.h \
linux-armv7.h \
linux-i686.h \
linux-x86_64.h \
windows.c \
S/startup-sequence \
S/startup-sequence.68000 \
S/startup-sequence.68020 \
DeeperDisco.68000.fs-uae \
DeeperDisco.68020.fs-uae \

BINARIES = \
DeeperDisco.html \
DeeperDisco.68000 \
DeeperDisco.68020 \
DeeperDisco.aarch64 \
DeeperDisco.armv7 \
DeeperDisco.i686 \
DeeperDisco.x86_64 \
DeeperDisco.aarch64.exe \
DeeperDisco.armv7.exe \
DeeperDisco.i686.exe \
DeeperDisco.x86_64.exe \

CFLAGS = -Oz -s -std=c99 -Wall -Wextra -pedantic

# change OSS to WAV to write to stdout instead of /dev/dsp
LINUX = \
-DLINUX \
-nostdlib \
-ffreestanding \
-DRANDOM \
-DOSS \

WINDOWS = \
-DWINDOWS \
-D__USE_MINGW_ANSI_STDIO=1 \
-DWINVER=0x501 \
-D_WIN32_WINNT=0x501 \
-mwindows \
-municode \
-mno-stack-arg-probe \
-Xlinker --stack=0x200000,0x200000 \
-lkernel32 \
-luser32 \
-lwinmm \
-flto \
-fno-builtin \
-nostartfiles \
-nostdlib \
-nodefaultlibs \


DeeperDisco.zip: $(SOURCES) $(BINARIES)
	cd .. && zip -9 DeeperDisco/$@ $(addprefix DeeperDisco/,$^)
	unzip -v DeeperDisco.zip


DeeperDisco.i686: $(SOURCES)
	i686-linux-gnu-gcc -o $@ deeperdisco.c linux.c $(CFLAGS) -m32 -DI686 $(LINUX)
	i686-linux-gnu-strip $@

DeeperDisco.x86_64: $(SOURCES)
	x86_64-linux-gnu-gcc -o $@ deeperdisco.c linux.c $(CFLAGS) -m64 -DX86_64 $(LINUX)
	x86_64-linux-gnu-strip $@

DeeperDisco.armv7: $(SOURCES)
	arm-linux-gnueabihf-gcc -o $@ deeperdisco.c linux.c $(CFLAGS) -DARMV7 $(LINUX)
	arm-linux-gnueabihf-strip $@

DeeperDisco.aarch64: $(SOURCES)
	aarch64-linux-gnu-gcc -o $@ deeperdisco.c linux.c $(CFLAGS) -DAARCH64 $(LINUX)
	aarch64-linux-gnu-strip $@


DeeperDisco.i686.exe: $(SOURCES)
	i686-w64-mingw32-gcc -o $@ deeperdisco.c windows.c $(CFLAGS) -DI686 $(WINDOWS) -fuse-linker-plugin
	i686-w64-mingw32-strip $@

DeeperDisco.x86_64.exe: $(SOURCES)
	x86_64-w64-mingw32-gcc -o $@ deeperdisco.c windows.c $(CFLAGS) -DX86_64 $(WINDOWS) -fuse-linker-plugin
	x86_64-w64-mingw32-strip $@

DeeperDisco.armv7.exe: $(SOURCES)
	armv7-w64-mingw32-gcc -o $@ deeperdisco.c windows.c $(CFLAGS) -DARMV7 $(WINDOWS) -Wno-unknown-attributes
	armv7-w64-mingw32-strip $@

DeeperDisco.aarch64.exe: $(SOURCES)
	aarch64-w64-mingw32-gcc -o $@ deeperdisco.c windows.c $(CFLAGS) -DAARCH64 $(WINDOWS) -Wno-unknown-attributes
	aarch64-w64-mingw32-strip $@


DeeperDisco.68000: $(SOURCES)
	m68k-amigaos-gcc -o $@ -Wno-pointer-sign -Os -nostdlib -s amiga-ncrt0.S deeperdisco.c amiga.c -std=c99 -Wall -Wextra -pedantic -m68000 -mcrt=none -DAMIGA -DM68000
	m68k-amigaos-strip $@

DeeperDisco.68020: $(SOURCES)
	m68k-amigaos-gcc -o $@ -Wno-pointer-sign -Os -nostdlib -s amiga-ncrt0.S deeperdisco.c amiga.c -std=c99 -Wall -Wextra -pedantic -m68020 -mcrt=none -DAMIGA -DM68020
	m68k-amigaos-strip $@
