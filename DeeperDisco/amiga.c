#include "deeperdisco.h"
#include "amiga.h"

#include <exec/types.h>
#include <exec/memory.h>
#include <dos/dos.h>
#include <graphics/gfxbase.h>
#include <workbench/startup.h>
#include <devices/audio.h>
#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/alib.h>

#ifdef NOQUIT
#define ctrlc 0
#else
#define ctrlc SIGBREAKF_CTRL_C
#endif

// http://www.amigadev.elowar.com/read/ADCD_2.1/Libraries_Manual_guide/node02EC.html
    struct MsgPort *myCreatePort(void)//CONST_STRPTR name, LONG pri)
    {
        LONG sigBit;
        struct MsgPort *mp;

        if ((sigBit = AllocSignal(-1L)) == -1) return(NULL);

        mp = (struct MsgPort *) AllocMem((ULONG)sizeof(struct MsgPort),
                 (ULONG)MEMF_PUBLIC | MEMF_CLEAR);
        if (!mp) {
            FreeSignal(sigBit);
            return(NULL);
        }
        mp->mp_Node.ln_Name = 0;//name;
        mp->mp_Node.ln_Pri  = 0;//pri;
        mp->mp_Node.ln_Type = NT_MSGPORT;
        mp->mp_Flags        = PA_SIGNAL;
        mp->mp_SigBit       = sigBit;
        mp->mp_SigTask      = (struct Task *)FindTask(0L);
                                                  /* Find THIS task.   */

        //if (name) AddPort(mp);
        //else
        NewList(&(mp->mp_MsgList));          /* init message list */

        return(mp);
    }

// http://www.amigadev.elowar.com/read/ADCD_2.1/Libraries_Manual_guide/node02ED.html
    void DeletePort(mp)
    struct MsgPort *mp;
    {
        //if ( mp->mp_Node.ln_Name ) RemPort(mp);  /* if it was public... */

//        mp->mp_SigTask         = (struct Task *) -1;
                                /* Make it difficult to re-use the port */
//        mp->mp_MsgList.lh_Head = (struct Node *) -1;

        FreeSignal( mp->mp_SigBit );
        FreeMem( mp, (ULONG)sizeof(struct MsgPort) );
    }


UBYTE *data[2][4];

#define AUDIO_PRIORITY 40
#define IDLE_PRIORITY -50

struct ExecBase *SysBase;
extern struct GfxBase *GfxBase;
#ifdef VERBOSE
struct DosLibrary *DOSBase;
#endif

#ifdef CHANGEFILTER

enum FILTER_ACTION
{
  FILTER_QUERY = 0,
  FILTER_SET = 1,
};

int AUDIO_filter(enum FILTER_ACTION action, int value)
{
  const UBYTE bit = 0x02;
  const UBYTE mask = ~bit;
  volatile UBYTE *CIA_A_PRA = (UBYTE *) 0xBFE001UL;
  UBYTE old = 0, new;
  switch (action)
  {
    case FILTER_QUERY:
      old = *CIA_A_PRA;
      break;
    case FILTER_SET:
      if (! value)
      {
        old = *CIA_A_PRA;
        new = old;
        new |= bit;
        *CIA_A_PRA = new;
      }
      else
      {
        old = *CIA_A_PRA;
        new = old;
        new &= mask;
        *CIA_A_PRA = new;
      }
  }
  return !!(old * bit);
}

#endif

struct AUDIO
{
  struct IOAudio *io[2];
  struct MsgPort *port[2];
  struct Message *msg;
  ULONG device;
  ULONG copied;
};

void AUDIO_delete(struct AUDIO *a)
{
  if (! a->device && ! a->copied) CloseDevice((struct IORequest *) a->io[0]);
  if (a->port[0]) DeletePort(a->port[0]);
  if (a->port[1]) DeletePort(a->port[1]);
  if (a->io[0]) FreeMem(a->io[0], sizeof(struct IOAudio));
  if (a->io[1]) FreeMem(a->io[1], sizeof(struct IOAudio));
  FreeMem(a, sizeof(struct AUDIO));
}

__chip UBYTE audio_data[2][2][deeperdisco_blocksize];

struct AUDIO *AUDIO_new(ULONG isPAL, struct AUDIO *copyFrom, BYTE channel, BYTE bufferchannel)
{
  BYTE c = 0;
  struct AUDIO *a = (struct AUDIO *) AllocMem(sizeof(struct AUDIO), MEMF_PUBLIC | MEMF_CLEAR);
  if (! a)
  {
    return a;
  }
  a->copied = !!copyFrom;
  a->device = 1;
  // ULONG clock = isPAL ? 3546895L : 3579545L;
  // ULONG speed = a->clock / samplerate;
  ULONG speed = isPAL ? deeperdisco_pal : deeperdisco_ntsc;
  a->io[0] = (struct IOAudio *) AllocMem(sizeof(struct IOAudio), MEMF_PUBLIC | MEMF_CLEAR);
  a->io[1] = (struct IOAudio *) AllocMem(sizeof(struct IOAudio), MEMF_PUBLIC | MEMF_CLEAR);
  a->port[0] = myCreatePort();
  a->port[1] = myCreatePort();
  UBYTE chan[4] = { 15, 15, 15, 15 };
  UBYTE *chans[4] = { &chan[0], &chan[1], &chan[2], &chan[3] };
  if (a->io[0] && a->port[0] && a->io[1] && a->port[1])
  {
    if (copyFrom)
    {
      *(a->io[0]) = *(copyFrom->io[0]);
      a->io[0]->ioa_Request.io_Message.mn_ReplyPort = a->port[0];
      a->io[0]->ioa_Request.io_Unit = (void *) (1 << channel);
    }
    else
    {
      while (a->device && c < 4)
      {
        a->io[0]->ioa_Request.io_Message.mn_ReplyPort = a->port[0];
        a->io[0]->ioa_Request.io_Message.mn_Node.ln_Pri = 127;
        a->io[0]->ioa_AllocKey = 0;
        a->io[0]->ioa_Data = chans[c++];
        a->io[0]->ioa_Length = 1;
        a->device = OpenDevice(AUDIONAME, 0L, (struct IORequest *) a->io[0], 0L);
      }
    }
    a->io[0]->ioa_Request.io_Command = CMD_WRITE;
    a->io[0]->ioa_Request.io_Flags = ADIOF_PERVOL;
    a->io[0]->ioa_Volume = 64;
    a->io[0]->ioa_Period = speed;
    a->io[0]->ioa_Cycles = 1;
    a->io[0]->ioa_Length = deeperdisco_blocksize;
    *(a->io[1]) = *(a->io[0]);
    a->io[1]->ioa_Request.io_Message.mn_ReplyPort = a->port[1];
//    a->io[0]->ioa_Data = a->data;
//    a->io[1]->ioa_Data = a->data + blocksize;
    a->io[0]->ioa_Data = &audio_data[0][bufferchannel][0];
    a->io[1]->ioa_Data = &audio_data[1][bufferchannel][0];
  }
  if (a->device && ! copyFrom)
  {
    AUDIO_delete(a);
    a = 0;
  }
  return a;
}

#ifdef IDLE

extern void IDLE_thread(void);

volatile ULONG IDLE_counter = 0;
void IDLE_thread(void)
{
  volatile ULONG *counterp = &IDLE_counter;
  __asm__ __volatile__
  ( "loop%=:\n\t"
    "add.l #1,(%[p])\n\t" // 12(1/2)
    "bra.b loop%=\n\t"    // 10(2/0)
  :
  : [p]"a"(counterp)
  : "memory"
  );
}

#endif

int main1(
#ifdef VERBOSE
BPTR out
#endif
)
{
  int retval = 20;
  struct AUDIO *audio[5] = {0, 0, 0, 0, 0};
  deeperdisco_initialize(0xD15c0);

#ifdef IDLE
  ULONG old_count = 0;
  struct Task *idle = CreateTask("deeperdisco/idle", IDLE_PRIORITY, (void *)IDLE_thread, 4000L);
  if (idle)
  {
#endif

  // open per task
  struct GfxBase *GfxBase = (struct GfxBase *) OpenLibrary("graphics.library", 0L);
  if (GfxBase)
  {
    UBYTE isPAL = !!(GfxBase->DisplayFlags & PAL);
#ifdef VERBOSE
    if (isPAL)
    {
      Write(out, "PAL\n", 4);
    }
    else
    {
      Write(out, "NTSC\n", 5);
    }
#endif

    if ((audio[4] = AUDIO_new(isPAL, 0, 0, 0)) &&
        (audio[0] = AUDIO_new(isPAL, audio[4], 0, 0)) &&
        (audio[1] = AUDIO_new(isPAL, audio[4], 1, 1)) &&
        (audio[2] = AUDIO_new(isPAL, audio[4], 2, 1)) &&
        (audio[3] = AUDIO_new(isPAL, audio[4], 3, 0)) )
    {
#ifdef CHANGEFILTER
      int filter = AUDIO_filter(FILTER_SET, 1);
#endif

      deeperdisco_sequence();
      deeperdisco_audio(audio[0]->io[0]->ioa_Data, audio[1]->io[0]->ioa_Data, deeperdisco_blocksize);
      deeperdisco_sequence();
      deeperdisco_audio(audio[0]->io[1]->ioa_Data, audio[1]->io[1]->ioa_Data, deeperdisco_blocksize);

      audio[4]->io[0]->ioa_Request.io_Command = CMD_STOP;
      audio[4]->io[0]->ioa_Request.io_Flags = IOF_QUICK;
      DoIO((struct IORequest *) audio[4]->io[0]);

      struct Task *task = FindTask(NULL);
      LONG oldpri = SetTaskPri(task, AUDIO_PRIORITY);

      BeginIO((struct IORequest *) audio[0]->io[0]);
      BeginIO((struct IORequest *) audio[1]->io[0]);
      BeginIO((struct IORequest *) audio[2]->io[0]);
      BeginIO((struct IORequest *) audio[3]->io[0]);

      UBYTE w = 0;
#ifdef IDLE
      ULONG old_count = IDLE_counter;
#endif
      UBYTE first = 1;
      for (ULONG block = 0; ; ++block)
      {
        for (BYTE q = 0; q < 4; ++q)
        {
          BeginIO((struct IORequest *) audio[q]->io[! w]);
        }

        if (first)
        {
          first = 0;
          audio[4]->io[1]->ioa_Request.io_Command = CMD_START;
          audio[4]->io[1]->ioa_Request.io_Flags = IOF_QUICK;
          DoIO((struct IORequest *) audio[4]->io[1]);
        }

        for (BYTE q = 0; q < 4; ++q)
        {
          WaitPort(audio[q]->port[w]);
          GetMsg(audio[q]->port[w]);
        }
        if (SetSignal(0, 0) & SIGBREAKF_CTRL_C)
        {
          for (BYTE q = 0; q < 4; ++q)
          {
            AbortIO((struct IORequest *) audio[q]->io[! w]);
            WaitPort(audio[q]->port[! w]);
            GetMsg(audio[q]->port[! w]);
          }
          break;
        }

        deeperdisco_sequence();
        deeperdisco_audio(audio[0]->io[w]->ioa_Data, audio[1]->io[w]->ioa_Data, deeperdisco_blocksize);

#ifdef VERBOSE
        if ((block & 0x3F) == 0)
        {
          char hexdigit[16] = {"0123456789abcdef"};
          char hexcount[9];
#ifdef IDLE
          ULONG new_count = IDLE_counter;
          ULONG diff = new_count - old_count;
          old_count = new_count;
          hexcount[8] = ' ';
          for (BYTE i = 7; i >= 0; --i)
          {
            hexcount[i] = hexdigit[diff & 0xf];
            diff >>= 4;
          }
          Write(out, hexcount, 9);
#endif
          Write(out, "\n", 1);
        }
#endif
        w = ! w;
      }

      SetTaskPri(task, oldpri);

#ifdef CHANGEFILTER
      AUDIO_filter(FILTER_SET, filter);
#endif

      retval = 0;
    }
#ifdef VERBOSE
    else
    {
      Write(out, "audio error\n", 12);
    }
#endif
    if (audio[0]) AUDIO_delete(audio[0]);
    if (audio[1]) AUDIO_delete(audio[1]);
    if (audio[2]) AUDIO_delete(audio[2]);
    if (audio[3]) AUDIO_delete(audio[3]);
    if (audio[4]) AUDIO_delete(audio[4]);

    CloseLibrary((struct Library *) GfxBase);
  }
#ifdef VERBOSE
  else
  {
    Write(out, "graphics error\n", 15);
  }
#endif

#ifdef IDLE
  }
  else
  {
    retval = 22;
  }
#endif

  return retval;
}

int main(void)
{
  int retval = 30;
#ifdef VERBOSE
  if (! (DOSBase = OpenLibrary("dos.library", 0L)))
  {
    return 21;
  }
  retval = main1(Output());
  CloseLibrary((struct Library *) DOSBase);
#else
  retval = main1();
#endif
  return retval;
}
