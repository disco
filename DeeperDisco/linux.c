#ifdef OSS
#include <linux/soundcard.h>
#endif

#include "deeperdisco.h"
#include "linux.h"

sample8_t out[deeperdisco_blocksize][2];

#ifdef WAV
uint8_t wav[44] =
  { 'R', 'I', 'F', 'F', 0x24, 0, 0, 0, 'W', 'A', 'V', 'E'
  , 'f', 'm', 't', ' ', 0x10, 0, 0, 0
  , 1, 0
  , 2, 0
  , 0x80, 0xbb, 0x00, 0x00
  , 0x00, 0xee, 0x02, 0x00
  , 4, 0
  , 0x10, 0
  , 'd', 'a', 't', 'a', 0, 0, 0, 0
  };
#endif

ALIGNED
void _start(void)
{
  uint32_t seed = 0xd15c0;
#ifdef RANDOM
  int random = openat(AT_FDCWD, "/dev/random", O_RDONLY);
  if (! (random < 0))
  {
    read(random, &seed, sizeof(seed));
    close(random);
  }
#endif
  deeperdisco_initialize(seed);

#ifdef OSS
int dsp = openat(AT_FDCWD, "/dev/dsp", O_WRONLY);
if (dsp < 0) exit(1);
int format = AFMT_S16_NE;
int ret = ioctl(dsp, SNDCTL_DSP_SETFMT, &format);
if (ret < 0) exit(1);
int channels = 2;
ret = ioctl(dsp, SNDCTL_DSP_CHANNELS, &channels);
if (ret < 0) exit(1);
int rate = 48000;
ret = ioctl(dsp, SNDCTL_DSP_SPEED, &rate);
if (ret < 0) exit(1);
#endif

#if 0
  uint32_t nframes = 0;// 1 << 25; // max 30 for uint8_t, 28 for float
  uint32_t samplerate = deeperdisco_samplerate;
  uint8_t format = 3; // 1 for int, 3 for float
  uint8_t channels = 2;
  uint8_t bitdepth = 8 * sizeof(sample8_t); // 32 for float
  uint16_t bytes_per_frame = channels * bitdepth / 8;
  uint16_t bytes_per_second = samplerate * channels;
  uint32_t size = nframes * bytes_per_frame * bitdepth / 8;
  uint32_t file_size = size + 0x24;
#define LE2(x) ((x) & 0xFF), (((x)>>8) & 0xFF)
#define LE4(x) ((x) & 0xFF), (((x)>>8) & 0xFF), (((x)>>16) & 0xFF), (((x)>>24) & 0xFF)
  uint8_t WAV[44] =
    { 'R', 'I', 'F', 'F', LE4(file_size), 'W', 'A', 'V', 'E'
    , 'f', 'm', 't', ' ', LE4(0x10)
    , LE2(format), LE2(channels), LE4(samplerate)
    , LE4(bytes_per_second), LE2(bytes_per_frame), LE2(bitdepth)
    , 'd', 'a', 't', 'a', LE4(size)
    };
#undef LE4
#undef LE4
#endif

#ifdef WAV
  const int dsp = 1;
  write(dsp, wav, sizeof(wav));
#endif

  while (1)
  {
    deeperdisco_sequence();
    deeperdisco_audio(&out[0][0], deeperdisco_blocksize);
    write(dsp, out, sizeof(out));
  }

//  close(dsp);
//  exit(0);
}
