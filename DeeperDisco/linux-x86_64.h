#include <stddef.h>
#include <syscall.h>
#include <sys/types.h>

static inline int openat(int dirfd, const char *name, int flags)
{
    int result;
    __asm__ __volatile__(
        "syscall"
        : "=a"(result)
        : "0"(SYS_openat), "D"(dirfd), "S"(name), "d"(flags)
        : "cc", "rcx", "r11", "memory");
    return result;
}

static inline int close(int fd)
{
    int result;
    __asm__ __volatile__(
        "syscall"
        : "=a"(result)
        : "0"(SYS_close), "D"(fd)
        : "cc", "rcx", "r11", "memory");
    return result;
}

static inline int ioctl(int fd, unsigned int cmd, void *arg)
{
    ssize_t result;
    __asm__ __volatile__(
        "syscall"
        : "=a"(result)
        : "0"(SYS_ioctl), "D"(fd), "S"(cmd), "d"(arg)
        : "cc", "rcx", "r11", "memory");
    return result;
}

static inline ssize_t write(int fd, const void *buf, size_t size)
{
    ssize_t result;
    __asm__ __volatile__(
        "syscall"
        : "=a"(result)
        : "0"(SYS_write), "D"(fd), "S"(buf), "d"(size)
        : "cc", "rcx", "r11", "memory");
    return result;
}

static inline ssize_t read(int fd, void *buf, size_t size)
{
    ssize_t result;
    __asm__ __volatile__(
        "syscall"
        : "=a"(result)
        : "0"(SYS_read), "D"(fd), "S"(buf), "d"(size)
        : "cc", "rcx", "r11", "memory");
    return result;
}

static inline void exit(int code)
{
    __asm__ __volatile__(
        "syscall"
        :
        : "a"(SYS_exit), "D"(code)
        : "cc", "rcx", "r11", "memory");
    __builtin_unreachable();
}

#define ALIGNED __attribute__((force_align_arg_pointer))
