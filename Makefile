EXES := timbre-stamp time-stretch rhythm-analysis rhythm-synthesis markov-analysis markov-synthesis octave-analysis octave-synthesis var-synthesis var-simulate normalize markov-analysis-fft markov-synthesis-fft disco-dreamer

all: $(EXES) moise~.pd_linux

clean:
	-rm $(EXES) moise~.pd_linux

%: %.c
	gcc -O3 -march=native -fopenmp $< -o $@ -Wall -Wextra -pedantic -g `pkg-config --cflags --libs gsl` -lsndfile -lfftw3 -lm 

%.pd_linux: %.c
	gcc -O3 -march=native $< -o $@ -Wall -Wextra -pedantic -g `pkg-config --cflags --libs gsl` -shared -fPIC

disco-designer: disco-designer.c
	gcc -O3 -march=native $< -o $@ -Wall -Wextra -pedantic -g `sdl2-config --cflags --libs` -lGL -lm

designer.html: disco-designer.c
	emcc -O3 -Os -o designer.html disco-designer.c --closure 1 -s USE_SDL=2 -s LEGACY_GL_EMULATION=1 -s GL_FFP_ONLY=1
	gzip -9 -k -f designer.js
	gzip -9 -k -f designer.wasm
