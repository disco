#include <SDL2/SDL.h>
#include <SDL2/SDL_audio.h>
#include <SDL2/SDL_opengl.h>

#ifdef __EMSCRIPTEN__
#include <emscripten.h>
#else
#include <unistd.h>
#endif

#include <limits.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <tgmath.h>
#include <time.h>

#define OCTAVES 11
#define OCTAVESIZE (1 << OCTAVES)

#define RHYTHMS 11
#define RHYTHMSIZE (1 << RHYTHMS)

#define HOPSIZE (1 << (RHYTHMS - 3))

#define CHANNELS 2

#define CELLSIZE 64
#define FPS 60

#define sqrt2 1.4142135623731f

// xorshift32
typedef uint32_t RANDOM;
RANDOM prng;
RANDOM rnd(RANDOM x)
{
  x ^= x << 13;
  x ^= x >> 17;
  x ^= x << 5;
  return x;
}

float noise(void)
{
  prng = rnd(prng);
  return prng / (float) (1u << 31) - 1.0f;
}

struct DISCO1
{
  float gain[RHYTHMS];
  float haar[2][RHYTHMSIZE];
  int ix;
  float output;
};

struct DISCO0
{
  struct DISCO1 rhythm[OCTAVES];
  float haar[2][OCTAVESIZE][CHANNELS];
  int ix;
  int jx;
  float output[CHANNELS];
};

void pull1(struct DISCO1 *disco)
{
  if (disco->ix == 0)
  {
    for (int i = 0; i < RHYTHMSIZE; ++i)
    {
      disco->haar[0][i] = noise();
    }
    // Haar wavelet transform
    for (int length = RHYTHMSIZE >> 1; length > 0; length >>= 1)
    {
      for (int i = 0; i < length; ++i)
      {
        float a = disco->haar[0][2 * i + 0];
        float b = disco->haar[0][2 * i + 1];
        float s = (a + b) / sqrt2;
        float d = (a - b) / sqrt2;
        disco->haar[1][         i] = s;
        disco->haar[1][length + i] = d;
      }
      for (int i = 0; i < RHYTHMSIZE; ++i)
      {
        disco->haar[0][i] = disco->haar[1][i];
      }
    }
  }
}

void eval1(struct DISCO1 *disco)
{
  disco->output = 0;
  int i = disco->ix;
  for (int length = RHYTHMSIZE >> 1, rhythm = RHYTHMS - 1; length > 0; length >>= 1, --rhythm)
  {
    float gain = disco->gain[rhythm];
    if (i & 1)
    {
      gain = -gain;
    }
    i >>= 1;
    disco->output += disco->haar[0][length + i] * gain;
  }
}

void step1(struct DISCO1 *disco)
{
  disco->ix++;
  disco->ix &= RHYTHMSIZE - 1;
}

void pull0(struct DISCO0 *disco)
{
  if (disco->jx == 0)
  {
    for (int i = 0; i < OCTAVES; ++i)
    {
      pull1(&disco->rhythm[i]);
    }
  }
  if (disco->ix == 0)
  {
    for (int i = 0; i < OCTAVESIZE; ++i)
    {
      for (int c = 0; c < CHANNELS; ++c)
      {
        disco->haar[0][i][c] = noise();
      }
    }
    // Haar wavelet transform
    for (int length = OCTAVESIZE >> 1; length > 0; length >>= 1)
    {
      for (int i = 0; i < length; ++i)
      {
        for (int c = 0; c < CHANNELS; ++c)
        {
          float a = disco->haar[0][2 * i + 0][c];
          float b = disco->haar[0][2 * i + 1][c];
          float s = (a + b) / sqrt2;
          float d = (a - b) / sqrt2;
          disco->haar[1][         i][c] = s;
          disco->haar[1][length + i][c] = d;
        }
      }
      for (int i = 0; i < OCTAVESIZE; ++i)
      {
        for (int c = 0; c < CHANNELS; ++c)
        {
          disco->haar[0][i][c] = disco->haar[1][i][c];
        }
      }
    }
  }
  for (int c = 0; c < CHANNELS; ++c)
  {
    disco->output[c] = 0;
  }
}

void eval0(struct DISCO0 *disco)
{
  int i = disco->ix;
  for (int length = OCTAVESIZE >> 1, octave = OCTAVES - 1; length > 0; length >>= 1, --octave)
  {
    eval1(&disco->rhythm[octave]);
    float gain = disco->rhythm[octave].output;
    if (i & 1)
    {
      gain = -gain;
    }
    i >>= 1;
    for (int c = 0; c < CHANNELS; ++ c)
    {
      disco->output[c] += disco->haar[0][length + i][c] * gain;
    }
  }
}

void step0(struct DISCO0 *disco)
{
  disco->ix++;
  disco->ix &= OCTAVESIZE - 1;
  if (disco->jx == 0)
  {
    for (int i = 0; i < OCTAVES; ++i)
    {
      step1(&disco->rhythm[i]);
    }
  }
  disco->jx++;
  disco->jx &= HOPSIZE - 1;
}

void disco0(struct DISCO0 *disco)
{
  pull0(disco);
  eval0(disco);
  step0(disco);
}

static struct
{
  // audio state
  struct DISCO0 disco;
  // video state
  SDL_Window* window;
  SDL_GLContext context;
  int target_octave;
  int target_rhythm;
  double target_delta;
} state;

static void audio(void *userdata, Uint8 *stream, int len)
{
  (void) userdata;
  float *b = (float *) stream;
  int m = len / sizeof(float) / CHANNELS;
  int k = 0;
  for (int i = 0; i < m; ++i)
  {
    disco0(&state.disco);
    for (int j = 0; j < CHANNELS; ++j)
    {
      b[k++] = state.disco.output[j];
    }
  }
}

static bool interact(void)
{
  bool running = true;
  SDL_Event event;
  while (SDL_PollEvent(&event) != 0)
  {
    switch (event.type)
    {
      case SDL_QUIT:
      {
        running = false;
        break;
      }
    }
  }
  int x = -1, y = -1;
  Uint32 b = SDL_GetMouseState(&x, &y);
  state.target_octave = 1 + x / CELLSIZE;
  state.target_rhythm = (RHYTHMS - 1) - y / CELLSIZE;
  state.target_delta = 2 * (0.5 - ((y % CELLSIZE) + 0.5) / CELLSIZE);
  if ( (b & SDL_BUTTON(SDL_BUTTON_LEFT)) &&
       1 <= state.target_octave && state.target_octave < OCTAVES &&
       1 <= state.target_rhythm && state.target_rhythm < RHYTHMS &&
      -1 <= state.target_delta  && state.target_delta <= 1
     )
  {
    double r = state.disco.rhythm[state.target_octave].gain[state.target_rhythm];
    r += state.target_delta / FPS;
    r = fmin(fmax(r, 0), 0.1);
    state.disco.rhythm[state.target_octave].gain[state.target_rhythm] = r;
  }
  return running;
}

static void video1(void)
{
  glClearColor(1, 1, 1, 1);
  glClear(GL_COLOR_BUFFER_BIT);
  glBegin(GL_QUADS);
  {
    for (int i = 1; i < OCTAVES; ++i)
    {
      for (int j = 1; j < RHYTHMS; ++j)
      {
        double r = state.disco.rhythm[i].gain[j];
        double radius = fmin(fmax(sqrt(r / 0.1) / 2.0, 3.0 / CELLSIZE), 0.5);
        const int sx[4] = { -1, -1, 1, 1 };
        const int sy[4] = { -1, 1, 1, -1 };
        for (int k = 0; k < 4; ++k)
        {
          glColor4f(0, 0, 0, 1);
          glVertex4f
            ( ((i - 0.5 + sx[k] * radius) / (OCTAVES - 1) - 0.5) * 2.0
            , ((j - 0.5 + sy[k] * radius) / (RHYTHMS - 1) - 0.5) * 2.0
            , 0
            , 1
            );
        }
        radius = 1.0 / CELLSIZE;
        for (int k = 0; k < 4; ++k)
        {
          glColor4f(1, 1, 1, 1);
          glVertex4f
            ( ((i - 0.5 + sx[k] * radius) / (OCTAVES - 1) - 0.5) * 2.0
            , ((j - 0.5 + sy[k] * radius) / (RHYTHMS - 1) - 0.5) * 2.0
            , 0
            , 1
            );
        }
      }
    }
  }
  glEnd();
  SDL_GL_SwapWindow(state.window);
}

void video(void)
{
  while (interact())
  {
    video1();
  }
}

void main1(void)
{
  if (interact())
  {
    video1();
  }
}

int main(int argc, char **argv)
{
  (void) argc;
  (void) argv;
  printf("disco/designer (Free Art License 1.3+) 2019,2022,2024 Claude Heiland-Allen\n");
  prng = time(0);
  prng += ! prng; // must not be 0
  memset(&state, 0, sizeof(state));
  // initialize SDL2
  SDL_Init(SDL_INIT_AUDIO | SDL_INIT_VIDEO);
  SDL_AudioSpec want, have;
  want.freq = 44100;
  want.format = AUDIO_F32;
  want.channels = CHANNELS;
  want.samples = 4096;
  want.callback = audio;
  SDL_AudioDeviceID dev = SDL_OpenAudioDevice(NULL, 0, &want, &have, SDL_AUDIO_ALLOW_ANY_CHANGE);
  if (have.freq > 192000 || have.format != AUDIO_F32 || have.channels != CHANNELS)
  {
    printf("want: %d %d %d %d\n", want.freq, want.format, want.channels, want.samples);
    printf("have: %d %d %d %d\n", have.freq, have.format, have.channels, have.samples);
    printf("error: bad audio parameters\n");
  }
  else
  {
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);
    state.window = SDL_CreateWindow("disco/designer", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, (OCTAVES - 1) * CELLSIZE, (RHYTHMS - 1) * CELLSIZE, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
    if (! state.window)
    {
      printf("error: couldn't create SDL2 window\n");
      return 1;
    }
    state.context = SDL_GL_CreateContext(state.window);
    if (! state.context)
    {
      printf("error: couldn't create OpenGL context\n");
      return 1;
    }
    // start audio processing
    SDL_PauseAudioDevice(dev, 0);
#ifdef __EMSCRIPTEN__
    emscripten_set_main_loop(main1, 0, 1);
#else
    video();
#endif
  }
  SDL_DestroyWindow(state.window);
  state.window = NULL;
  SDL_Quit();
  return 0;
}
