#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sndfile.h>

#define OCTAVES 11
#define BLOCKSIZE (1 << (OCTAVES - 1))
#define OVERLAP 4
#define HOP (BLOCKSIZE / OVERLAP)

int main(int argc, char **argv)
{
  if (argc != 3)
  {
    fprintf
      ( stderr
      , "usage:\n"
        "  %s in.wav out.wav\n"
      , argv[0]
      );
    return 1;
  }
  SF_INFO info;
  memset(&info, 0, sizeof(info));
  fprintf(stderr, "reading %s\n", argv[1]);
  SNDFILE *ifile = sf_open(argv[1], SFM_READ, &info);
  if (! ifile) abort();
  fprintf(stderr, "writing %s\n", argv[2]);
  SF_INFO outfo =
    { 0
    , info.samplerate
    , info.channels
    , SF_FORMAT_WAV | SF_FORMAT_FLOAT
    , 0
    , 0
    };
  SNDFILE *ofile = sf_open(argv[2], SFM_WRITE, &outfo);
  if (! ofile) abort();
  const int CHANNELS = info.channels;

  float ibuffer[BLOCKSIZE][CHANNELS];
  memset(ibuffer, 0, sizeof(ibuffer));
  float obuffer[BLOCKSIZE][CHANNELS];
  memset(obuffer, 0, sizeof(obuffer));
  float nbuffer[BLOCKSIZE][CHANNELS];
  memset(nbuffer, 0, sizeof(nbuffer));
  float window[BLOCKSIZE];
  memset(window, 0, sizeof(window));
  for (int i = 0; i < BLOCKSIZE; ++i)
  {
    window[i] = 0.5 - 0.5 * cos(i * 6.283185307179586 / BLOCKSIZE);
  }

  int reading = OVERLAP;
  while (reading > 0)
  {

    // read input
    for (int i = 0; i < BLOCKSIZE - HOP; ++i)
    {
      for (int channel = 0; channel < CHANNELS; ++channel)
      {
        ibuffer[i][channel] = ibuffer[i + HOP][channel];
        nbuffer[i][channel] = nbuffer[i + HOP][channel];
      }
    }
    for (int i = BLOCKSIZE - HOP; i < BLOCKSIZE; ++i)
    {
      for (int channel = 0; channel < CHANNELS; ++channel)
      {
        // pre-fill with zero in case of end of input
        ibuffer[i][channel] = 0;
        nbuffer[i][channel] = rand() / (double) RAND_MAX - 0.5;
      }
    }
    if (reading == OVERLAP)
    {
      int r = sf_readf_float(ifile, &ibuffer[BLOCKSIZE - HOP][0], HOP);
      if (r != HOP)
      {
        reading--;
      }
    }
    else
    {
      reading--;
    }

    // window
    float haar[2][BLOCKSIZE][CHANNELS];
    memset(haar, 0, sizeof(haar));
    int src = 0;
    int dst = 1;
    for (int i = 0; i < BLOCKSIZE; ++i)
    {
      for (int channel = 0; channel < CHANNELS; ++channel)
      {
        haar[src][i][channel] = ibuffer[i][channel] * window[i];
      }
    }

    // compute Haar wavelet transform
    for (int length = BLOCKSIZE >> 1; length > 0; length >>= 1)
    {
      for (int i = 0; i < length; ++i)
      {
        for (int channel = 0; channel < CHANNELS; ++channel)
        {
          float a = haar[src][2 * i + 0][channel];
          float b = haar[src][2 * i + 1][channel];
          float s = (a + b) / 2;
          float d = (a - b) / 2;
          haar[dst][         i][channel] = s;
          haar[dst][length + i][channel] = d;
        }
      }
      for (int i = 0; i < BLOCKSIZE; ++i)
      {
        for (int channel = 0; channel < CHANNELS; ++channel)
        {
          haar[src][i][channel] = haar[dst][i][channel];
        }
      }
    }

    // compute energy per octave
    double E[OCTAVES];
    memset(E, 0, sizeof(E));
    for (int channel = 0; channel < CHANNELS; ++channel)
    {
      E[0] += fabsf(haar[src][0][channel]); // DC
    }
    for ( int octave = 1, length = 1
        ; length <= BLOCKSIZE >> 1
        ; octave += 1, length <<= 1
        )
    {
      double rms = 0;
      for (int channel = 0; channel < CHANNELS; ++channel)
      {
        for (int i = 0; i < length; ++i)
        {
          double a = haar[src][length + i][channel];
          rms += a * a;
        }
      }
      rms /= length * CHANNELS;
      rms = sqrt(fmax(rms, 0));
      E[octave] = rms;
    }

    // window
    for (int i = 0; i < BLOCKSIZE; ++i)
    {
      for (int channel = 0; channel < CHANNELS; ++channel)
      {
        haar[src][i][channel] = nbuffer[i][channel] * window[i];
      }
    }

    // compute Haar wavelet transform
    for (int length = BLOCKSIZE >> 1; length > 0; length >>= 1)
    {
      for (int i = 0; i < length; ++i)
      {
        for (int channel = 0; channel < CHANNELS; ++channel)
        {
          float a = haar[src][2 * i + 0][channel];
          float b = haar[src][2 * i + 1][channel];
          float s = (a + b) / 2;
          float d = (a - b) / 2;
          haar[dst][         i][channel] = s;
          haar[dst][length + i][channel] = d;
        }
      }
      for (int i = 0; i < BLOCKSIZE; ++i)
      {
        for (int channel = 0; channel < CHANNELS; ++channel)
        {
          haar[src][i][channel] = haar[dst][i][channel];
        }
      }
    }

    // amplify octaves
    for (int channel = 0; channel < CHANNELS; ++channel)
    {
      haar[src][0][channel] *= E[0];
    }
    for ( int octave = 1, length = 1
        ; length <= BLOCKSIZE >> 1
        ; octave += 1, length <<= 1
        )
    {
      for (int channel = 0; channel < CHANNELS; ++channel)
      {
        for (int i = 0; i < length; ++i)
        {
          haar[src][length + i][channel] *= E[octave];
        }
      }
    }

    // compute inverse Haar wavelet transform
    for (int length = 1; length <= BLOCKSIZE >> 1; length <<= 1)
    {
      for (int i = 0; i < BLOCKSIZE; ++i)
      {
        for (int channel = 0; channel < CHANNELS; ++channel)
        {
          haar[dst][i][channel] = haar[src][i][channel];
        }
      }
      for (int i = 0; i < length; ++i)
      {
        for (int channel = 0; channel < CHANNELS; ++channel)
        {
          float s = haar[dst][         i][channel];
          float d = haar[dst][length + i][channel];
          float a = s + d;
          float b = s - d;
          haar[src][2 * i + 0][channel] = a;
          haar[src][2 * i + 1][channel] = b;
        }
      }
    }

    // window
    for (int i = 0; i < BLOCKSIZE; ++i)
    {
      for (int channel = 0; channel < CHANNELS; ++channel)
      {
        haar[src][i][channel] *= window[i];
      }
    }

    // overlap-add
    for (int i = 0; i < BLOCKSIZE; ++i)
    {
      for (int channel = 0; channel < CHANNELS; ++channel)
      {
        obuffer[i][channel]
          = i + HOP < BLOCKSIZE
          ? obuffer[i + HOP][channel]
          : 0;
      }
    }
    for (int i = 0; i < BLOCKSIZE; ++i)
    {
      for (int channel = 0; channel < CHANNELS; ++channel)
      {
        obuffer[i][channel] += haar[src][i][channel];
      }
    }

    // output
    sf_writef_float(ofile, &obuffer[0][0], HOP);

  }
  sf_close(ifile);
  sf_close(ofile);
  return 0;
}
